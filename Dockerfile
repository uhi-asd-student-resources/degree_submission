FROM ubuntu as basicos

RUN apt -y update
RUN DEBIAN_FRONTEND=noninteractive TZ="Europe/London" apt -y install python3 ffmpeg libmagic-dev libgtk-3-dev python3-venv python3-pip git openssh-client

# Takes environment variables so user can send their own
# docker build --build-arg UID=$(id -u) --build-arg GID=$(id -g) -f dockerfile -t degree_submission .
ARG UNAME=testuser
ARG UID=1000
ARG GID=1000

RUN groupadd -g $GID -o ${UNAME}
RUN useradd -m -u $UID -g $GID -o -s /bin/bash ${UNAME}

FROM basicos as copyhost2container
ARG UNAME=testuser
ARG UID=1000
ARG GID=1000
USER $UNAME
ENV PATH="/home/${UNAME}/.local/bin:${PATH}"
COPY --chown=${UID}:${GID} . /home/${UNAME}

FROM copyhost2container as pythoninstall
ARG UNAME=testuser
ARG UID=1000
ARG GID=1000
USER $UNAME
ENV PATH="/home/${UNAME}/.local/bin:${PATH}"
WORKDIR /home/${UNAME}
RUN sh INSTALL.sh

FROM pythoninstall as security
ARG UNAME=testuser
ARG UID=1000
ARG GID=1000
ARG SSHKEY="gitkey"
USER $UNAME
ENV PATH="/home/${UNAME}/.local/bin:${PATH}"
WORKDIR /home/${UNAME}
RUN mkdir -p /home/${UNAME}/.ssh
COPY --chown=${UID}:${GID} ${SSHKEY} /home/${UNAME}/.ssh/id_rsa
COPY --chown=${UID}:${GID} ${SSHKEY}.pub /home/${UNAME}/.ssh/id_rsa.pub

FROM security as portfoliobuild
ARG UNAME=testuser
ARG UID=1000
ARG GID=1000
USER $UNAME
WORKDIR /home/${UNAME}
ENV PATH="/home/${UNAME}/.local/bin:${PATH}"
COPY --chown=${UID}:${GID} mapping.csv /home/${UNAME}
COPY --chown=${UID}:${GID} repositories.txt /home/${UNAME}
COPY --chown=${UID}:${GID} submission_configuration.json /home/${UNAME}
# RUN eval $(ssh-agent -s) && ssh-add /home/${UNAME}/.ssh/id_rsa && ssh-add -l && sh build_submission.sh

# FROM portfoliobuild as zipfile
# ARG UNAME=testuser
# ARG UID=1000
# ARG GID=1000
# USER $UNAME
# WORKDIR /home/${UNAME}
# ENV PATH="/home/${UNAME}/.local/bin:${PATH}"
# COPY --chown=${UID}:${GID} ${STUDENTNUMBER} /home/${UNAME}
# RUN sh build_submission.sh
