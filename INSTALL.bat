@echo off

rem Inspired by this code:
rem https://stackoverflow.com/questions/52211917/batch-script-to-execute-script-with-python-3-if-available-else-python-2/52211993

set "$py=0"
call:construct

for /f "delims=" %%a in ('python #.py ^| findstr "3"') do set "$py=3"
del #.py
goto:%$py%

echo python 3 is not installed or python's path Path is not in the %%$path%% env. var
exit/b

:3
echo "Found python 3 installed"
echo "Installing python environment"
cmd /k "echo 'Updating pip' & pip install --upgrade pip & echo 'Installing required modules' & pip install -r requirements.txt & echo 'Installing libmagic' & pip install python-magic-bin"

:construct
echo import sys; print('{0[0]}.{0[1]}'.format(sys.version_info^)^) >#.py