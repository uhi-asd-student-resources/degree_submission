#!/bin/bash

PYTHON=$(which python3)
PIP=$(which pip3)
"$PYTHON" --version | grep "Python 3" > /dev/null
if [ $? -eq 0 ]; then
    echo "Python 3 detected."
else
    echo "Python 3 was not detected."
    exit 1
fi


"${PIP}" install --user wheel
chmod +x *.sh
"${PIP}" install --user -r requirements.txt

# echo
# echo "Now run the following 3 commands one after the other:"
# echo "   pip install --user wheel"
# echo "   pip install -r requirements.txt"
# echo


