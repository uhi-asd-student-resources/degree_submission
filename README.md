# Degree Submission

This repository contains scripts and instructions to help students submit their portfolios for the BSc Hons Applied Software Development degree at University of Highlands and Islands.

> ALWAYS HAVE A COPY OF ANY FILES THAT ARE IMPORTANT TO YOU.  ONLY PLACE **COPIES** OF FILES WITHIN THE DIRECTORIES THAT THIS TOOL WILL ACT UPON - JUST IN CASE!

## Getting started

Please start in the [docs/students](docs/students) directory in the docs file and select your OS.

There is also documentation for developers and markers in the docs directory.

## Dependencies

* Git/Git for Windows
* Python 3
* ffmpeg - used to compress audio and video assets
* GTK3

