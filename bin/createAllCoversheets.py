#!/bin/env python3
from DegreeModulesCollection import createDegreeModuleCollection
import os
import sys

if not os.path.isdir(sys.argv[1]):
    raise ValueError("first argument must be a valid directory")

moduleCollection = createDegreeModuleCollection()
moduleCollection.createAllCoversheets(sys.argv[1])

