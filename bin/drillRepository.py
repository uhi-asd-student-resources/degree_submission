#!/bin/env python3

import pydriller
import sys

file = None
repoPath = sys.argv[1]
if len(sys.argv) > 2:
    file = sys.argv[2]

repo = None
if file is None:
    repo = pydriller.Repository(repoPath)
else:
    # this only works with existing files not ones that have been deleted.
    print("Searching for file: {}".format(file))
    repo = pydriller.Repository(repoPath, filepath=file)

for commit in repo.traverse_commits():
    print("Extracting commit {} by {}".format(commit.hash, commit.author.name))
    for file in commit.modified_files:
        print("\tFile change type: {}".format(file.change_type))
        print("\tFile committed (old_path): {}".format(file.old_path))
        print("\tFile committed (new_path): {}".format(file.new_path))
        print("\tFile committed (filename): {}".format(file.filename))
        print("[{}] Source Code Before:".format(commit.hash))
        print("{}".format(file.source_code_before))
        print("[{}] Source Code:".format(commit.hash))
        print("{}".format(file.source_code))