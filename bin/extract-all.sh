#!/bin/bash

if [ "x${DEGREE_SUBMISSION_PATH}" == "x" ]; then
  echo "Please set DEGREE_SUBMISSION_PATH"
  exit 1
fi



# now we are going to extract the submission using a version of the following
# ${DEGREE_SUBMISSION_PATH}/extract_student_evidence.sh --zip-path ./123456789.zip --student-number 123456789
# && cd \"$D\" && python \"${DEGREE_SUBMISSION_PATH}/src/init.py\" --extract --portfolio-path \"$X\" --student-number \"$S\"
find . -mindepth 2 -maxdepth 2 -type f -iname "*.zip" -exec sh -c 'echo -e "Zip file found:\t\t{}" && D="`dirname \"{}\"`" && S="`basename \"{}\" | sed \"s/\.zip$//\"`" && X="$D/$S" && echo -e "Parent directory:\t$D" && echo -e "Student Number:\t\t$S" && echo -e "Portfolio Directory:\t$X" && cd "$D" && python "${DEGREE_SUBMISSION_PATH}/src/init.py" --extract --portfolio-path "./$S" --student-number "$S"' ';'
