#!/usr/bin/python3

"""
Tool to transcribe audio (mp3) to text.

Code modified from: https://pythonbasics.org/transcribe-audio/
"""

import speech_recognition as sr
from pydub import AudioSegment
import sys
import os
import subprocess


if len(sys.argv) == 1:
    print("This is a small program that will attempt to transcribe text from an audio file.")
    print("Syntax: python transcribe_audio.py <audio_file>")
    sys.exit(1)

fileToTranscribe = sys.argv[1]
fileToTranscribe = fileToTranscribe.strip()
if len(fileToTranscribe) == 0 or not fileToTranscribe:
    raise ValueError("File to transribe is empty")
print("Selected {}".format(fileToTranscribe))
if not os.path.isfile(fileToTranscribe):
    raise ValueError("Could not find file to transcribe: '{}'".format(fileToTranscribe))

filenameWithExt = os.path.basename(fileToTranscribe)
path = os.path.dirname(fileToTranscribe)
filename, extension = os.path.splitext(filenameWithExt)
print("Path: {}".format(path))
print("Base filename: {}".format(filename))
print("File Extension: {}".format(extension))
if extension != ".mp3" and extension != ".wav":
    raise ValueError("input file must be an mp3 or a wav file")

outputTextFile = os.path.join(path, "{}_transcription.txt".format(filename))
isTemporaryWavFile = False
if extension == ".mp3":
    # convert mp3 file to wav        
    isTemporaryWavFile = True
    wavFileToTranscribe = "~{}.wav".format(filename)
    if len(path) > 0:
        wavFileToTranscribe = os.path.join(path, wavFileToTranscribe)
    print("Converting to wave file '{}'".format(wavFileToTranscribe))    
    try:                                               
        sound = AudioSegment.from_mp3(fileToTranscribe)
        sound.export(wavFileToTranscribe, format="wav")
    except:
        raise RuntimeError("Converting failed, trying manual ffmpeg conversion to wav format and try to transcribe the .wav file.")
else:
    wavFileToTranscribe = fileToTranscribe

# use the audio file as the audio source                                        
r = sr.Recognizer()
with sr.AudioFile(wavFileToTranscribe) as source:
    audio = r.record(source)  # read the entire audio file
    transcription = r.recognize_google(audio)
    with open(outputTextFile, "w") as outFile:
        outFile.writelines(transcription)

if os.path.isfile(isTemporaryWavFile):
    print("Removing wav file {}".format(wavFileToTranscribe))
    os.remove(wavFileToTranscribe)

print("Transcription has been written to '{}'".format(outputTextFile))
print("\n\n------------------------------------------------\n")
print("Please format the transcription so it is readable, this")
print("will be taken into account when marking your work.")
print("\n------------------------------------------------\n")