#!/bin/sh

## Handy script to run in the same directory as your downloaded file from Brightspace

LOG="unzip-all.log"
target="."
ZIP=""
if [ "x$1" != "x" ]; then
    target="$1"
fi

if [ ! -d "$target" ]; then
    unzip "$target" 2>&1 | tee -a "${LOG}"
    ZIP="$target"
    target=$(dirname "$target")
fi

# this gets the absolute path of the given directory
# https://stackoverflow.com/questions/4175264/how-to-retrieve-absolute-path-given-relative
BASE="$(basename "$target")"
#echo ${BASE}
target="$(cd "$(dirname "$target")"; pwd)/${BASE}"

echo "Searching in directory: ${target}" | tee -a "${LOG}"
echo "Finding and unzipping all zip files in place" | tee -a "${LOG}"
# https://stackoverflow.com/questions/14813130/find-all-zips-and-unzip-in-place-unix/14813275
# Had to alter these to create a directory with the student number as the original zip process did not go this
if [ "x$ZIP" == "x" ]; then
    echo "Unzipping explicit zip file"
    find "$target" -type f -name "*.zip" -exec sh -c 'echo "{}" && X="`dirname \"{}\"`/`basename \"{}\" | sed \"s/\.zip$//\"`" && mkdir "$X" && unzip -d "$X" "{}"' ';' 2>&1 | tee -a "${LOG}"
else
    echo "Unzipping implicit zip files"
    find "$target" -type f -name "*.zip" ! -name "${ZIP}" -exec sh -c 'echo "{}" && X="`dirname \"{}\"`/`basename \"{}\" | sed \"s/\.zip$//\"`" && mkdir "$X" && unzip -d "$X" "{}"' ';' 2>&1 | tee -a "${LOG}"
fi

#
# This is the version that assumes the zip file already has the directory.
# Ideally we want something more robust that will actually test to see whether
#   the root directory is the same as the student number in the zip file or not.
#   We could do this by doing a test unzip and then check the verbose output.
#
# if [ "x$ZIP" == "x" ]; then
#     echo "Unzipping explicit zip file"
#     find "$target" -type f -name "*.zip" -exec sh -c 'echo "{}" && X="`dirname \"{}\"`" && mkdir "$X" && unzip -d "$X" "{}"' ';' 2>&1 | tee -a "${LOG}"
# else
#     echo "Unzipping implicit zip files"
#     find "$target" -type f -name "*.zip" ! -name "${ZIP}" -exec sh -c 'echo "{}" && X="`dirname \"{}\"`" && mkdir "$X" && unzip -d "$X" "{}"' ';' 2>&1 | tee -a "${LOG}"
# fi



echo | tee -a "${LOG}"
echo "A log of the files unzipped can be found in '${LOG}'." | tee -a "${LOG}"
echo | tee -a "${LOG}"
