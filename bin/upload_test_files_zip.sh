#!/bin/bash

if [ ! "UHI_HOST" ]; then
  echo "UHI_HOST is not defined in environment"
  exit 1
fi

if [ ! "UHI_HOST_USERNAME" ]; then
  echo "UHI_HOST_USERNAME is not defined in environment"
  exit 1
fi

zip -r test_files.zip test_files
scp test_files.zip ${UHI_HOST_USERNAME}@${UHI_HOST}:/var/www/html/submission_test_files.zip
