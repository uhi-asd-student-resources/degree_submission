@echo off

set PYTHONEXE="python.exe"

set argc=0
for %%x in (%*) do Set /A argC+=1

if %argc% == 0 goto RUN_NO_ARGS

:RUN_WITH_ARGS
    %PYTHONEXE% %0\..\src\init.py --build %*
    goto END_OF_FILE

:RUN_NO_ARGS
    %PYTHONEXE% %0\..\src\init.py --build
    goto END_OF_FILE

:END_OF_FILE
