#!/bin/bash

PYTHON=$(which python3)
export PYTHONPATH=${PYTHONPATH}:$(pwd):$(pwd)/src
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ $#  -eq 0 ]; then
    "${PYTHON}" "${SCRIPT_DIR}/src/init.py" --build
else
    "${PYTHON}" "${SCRIPT_DIR}/src/init.py" --build "$@"
fi
