# Developer Instructions

If you are planning to add functionality or an interface.  Please contact the author as there may well be a reason why we have done this the way that we have.

## Testing

### Linux

```
git submodule update --init
./test_files/unpack.sh
source venv/bin/activate
# The first time you run this it may take a while as it create a very large file for compression testing.
./run_tests.sh
```

If the tests fail, make sure that you have an up to date test_files directory and that you have all the required dependencies installed.
If the test fails then this sometime is due to something dodgy in the way the git python is handling the git API I think.

```
export PYTHONPATH=src:tests
# example individual test suite to run
python -m unittests tests/PortfolioBuildCommand_test.py
```

The other reason is that the submodule in test_files was not committed and so you have an old version following the next set of instructions if this happens.

### Help! I need to update the test_files directory?

If the degree_submission_test_files repository requires updating:

```
cd test_files
git pull origin master
# don't for get to commit your update to the submodule
cd ..
git commit -m "updated submodule"
git push
```

### Windows

To run the tests you need to unzip the test_files.zip.  This is compressed as it contains .git directories which will conflict with the main git repository.

```
unzip test_files.zip
# WARNING: this will no longer work unless you have generated the very_large_file using the linux version
./run_tests.sh
```

To get the tests to run in windows then do the following:

Open test_files.zip and extract the test_files directory to the degree_submissions directory.  You can do this via Git Bash or in Explorer.

Open ```cmd``` and do the following:

```
cd c:\Users\<username>\degree_submissions
run_tests.bat
```

### Unresolved errors

If you get errors like ```[Win Error 5]``` then run each set of tests separately.  Currently the Repositories_test.py causes issues but this is unresolved.