# Todo list

Here are some ideas for extensions.

1. Add a switch such that any fatal errors are fatal for students, but can be only warnings for markers.
2. Add a way to regenerate a mapping.csv file from the repositories, you would need to get the student to say:
   * what their author names were for the git repositories
   * have some way to indicate whether the student did NOT want the work marked - I suggest assuming all work with a learning outcome should be marked and then getting them to turn it off.  If they submit too much for marking you can then penalise them in the usual manner.  This is the opposite of the current system which is probably too cautious.
   * Create a timeline of all the commits in the repository that belong to a student - would give better indication of a students engagement.  For best results you would need a config file with the uptodate term dates that is updated each academic year.
3. In assessment 

"There is little evidence that the student has applied the knowledge learned as directed from the learning outcomes to a practical activity." is the main reason for not giving the person a 2 so I think this could be made a standard answer to one of the at expected level entries rather than a separate item.

[DONE]

4. Change Jira words to Project and Time Management tools to allow for Trello etc [DONE]
5. In assessment:

In the evidence provided the student has addressed all aspects of the learning outcome.
and
Learning outcomes are relevant to the content written.

Could be merged as the reason why you turnoff the first is generally a lack of relevance. [DONE]
6. Add grade to saved sessions panel on assessment tool. [DONE]
7. Review guidance on some of the Communication criteria.
8. Added version number to degree_submission init.py [DONE]
9. Added version number to program arguments e.g. ./build_submission --version [DONE]
