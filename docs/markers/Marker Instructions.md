# Marker Instructions

This tool was designed on linux and therefore the majority of the testing has been done on that.  Windows is an afterthought currently.

> The extraction of evidence from a student's portfolio is a non-trivial task.  This application has been written to try and cope with most of the errors that students make and will try its best to find evidence where appropriate.  However, this is not always completely successful.  
> When reviewing the results **ALWAYS** check the mapping.csv and ensure that you are happy that all the evidence submitted has been suitably extracted.  
> In addition, this application will only extract the files marked by the learning outcome and may not find dependent files.  Therefore it is important that when viewing the evidence if you see references to other work that these are followed up separately.
> **If you don't know what a particular message from the tool means, then double check with the other delivery staff for clarification.**

## Recommended tools

* A Linux/Mac installation
* Git
* Python 3
* Mplayer

## Getting started

We are going to assume you are installing this and you don't mind installing python libraries into your home directory.

```
git clone git@bitbucket.org:uhi-asd-student-resources/degree_submission.git
cd degree_submission
pip install --user -r requirements.txt
export DEGREE_SUBMISSION_PATH=`$(pwd)`
```

For Macs you might need this:

```
brew install ffmpeg
brew install gtk+3
brew install libmagic
```

## Automated extraction process

1. Download the set of assessments you wish to mark from the Brightspace module.  This should give you a zip of multiple portfolios. While the student should submit the same portfolio to all modules, you cannot assume they will do this.  Therefore you should always download all assessment data.
2. Create a temporary directory named after the module code at a suitable location and copy the zip file from Brightspace into it.  In this example we assume you use /tmp.

```
mkdir /tmp/UI107006
cp "End of module portfolio Download 27 September 2021 1147 AM.zip" /tmp/UI107006
cd /tmp/UI107006
```
3. Extract all the portfolios

```
${DEGREE_SUBMISSION_PATH}/extract_brightspace_download.sh "End of module portfolio Download 27 September 2021 1147 AM.zip"
```

If your path in DEGREE_SUBMISSION_PATH has spaces, don't forget your quotes!

```
"${DEGREE_SUBMISSION_PATH}/extract_brightspace_download.sh"  "End of module portfolio Download 27 September 2021 1147 AM.zip"
```

If you have set up your Mac such that the zip file is already unzipped, then you should specify the directory that it has created e.g. (not no zip extension)

```
"${DEGREE_SUBMISSION_PATH}/extract_brightspace_download.sh"  "End of module portfolio Download 27 September 2021 1147 AM"
```

> **TROUBLESHOOTING**: If something goes wrong you might need to check the internals of the brightspace zip file to see if they have changed, or if the student has done something weird with their zip file.  In terms of code I would start looking at the ```UnzipClassDownload.py```, ```UnzipStudentPortfolio.py```, ```ExtractEntryPoints.py::setupForExtractionGivenPathToZipFile```.  The chances are something is joining a variable which already has the path in to itself.


4.  For each student you will have two directories and 1 log file should then exist.  For example if the student number is 123456789 then you should get the following:

```
# the student work as the zip file extracts to
./123456789
# the work extracted and put into the appropriate locations for review
./123456789_extracted
# the log of the extraction
./123456789_submission_extract_20210927T163544.log
```

## Issue: Tool has extracted more learning outcomes than expected

Check the students submission_configuration.json file.  Most likely they have specified the wrong set of modules.  You can either (a) modify this file for them, delete the _extracted directory and rerun, or more simply delete the extra learning outcome directories.



## Layout of Extracted Directory

1. The assets directory is a copy of any assets in their zip file.
2. The commits directory is a dump of all the commits in the repositories they submit.
3. The coversheets directory is a dump of all the files starting with regex /cover_{0,1}sheet/.
4. The other directories of the form LOW.X.Y.Z refer to learning outcome codes.
   1. Each commit with files is unpacked here with both the file at time of commit and also a copy of the latest file at HEAD.

## Reviewing Videos and Audio

You can review videos and audio at a faster pace by using the following:

```
mplayer -speed 1.75 -af scaletempo <file>
```

This is encapulated in the ```env``` file that can be sourced.  At which point you can use the following:

```
review <file>
```

## Checking for duplicate or novel portfolios

You can scan for duplicates by building a database of SHA1 sums.  Use the following to generate an initial output from outside of the module directory:

```
find UI123456 -maxdepth 3 -iname "*.zip" ! -iname "End of module*" -exec sh -c 'sha1sum "{}"' ';' > UI123456.log
```

You can then create a file of all these SHA1 lines and look for those that are different.  These are the ones you mark.

