# Docker version

* Assumes docker is installed and you are familiar with the basics

* Copy your ssh key that you use with your repositories to the degree_submission directory so that they can be copied to the docker container.
* Create your mapping.csv, repositories.txt and submission_configuration.json file.
* Run the following, replacing my_ssh_key with your key name (without the .pub):
```
docker build --target portfoliobuild -t portfoliobuild --build-arg UNAME=${USER} --build-arg SSHKEY="my_ssh_key" .
...
Successfully built 302a2cb32254
Successfully tagged portfoliobuild:latest
```
* Copy the container id that is printed if you miss it you can do the following:
```
docker image ls | head -n 2 | tail -n 1 | awk '{print $3}'
```
* Run the container
```
docker run -it portfoliobuild:latest /bin/bash
```

You can then just run build_submission and make the required changes copy to and from the host in the usual docker fashion using ```docker cp```.

