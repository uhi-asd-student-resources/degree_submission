# Degree Submission

Welcome to the Degree Submission tool.  This may look intimidating but most of this is setup and once its all on your machine the process should be painless.  Make sure you read any error messages with care and check you have complied with all the instructions.  If you still are having problems, then please contact the delivery staff with immediate effect.

This repository contains scripts and instructions to help students submit their portfolios for the BSc Hons Applied Software Development degree at University of Highlands and Islands.

> ALWAYS HAVE A COPY OF ANY FILES THAT ARE IMPORTANT TO YOU.  ONLY PLACE **COPIES** OF FILES WITHIN THE DIRECTORIES THAT THIS TOOL WILL ACT UPON - JUST IN CASE!

## Important Guidance

0. READ ALL THE INSTRUCTIONS AND ASK QUESTIONS IF THEY ARE NOT CLEAR.

1. We recommend you do not delete any repositories of work until you receive your marks and have passed.  Even then there may be elements of the work you can reflect on for subsequent years as evidence so you should take backups if you want to delete them from bitbucket for any reason.

2. It is **your** responsibility to place files in a location that we can find easily.  If for some reason we cannot find a file, then the excuse of "but the file is here" will not be taken into account.  Use the extraction tools provided to ensure that the evidence you think is relevant is picked out.  If its not then you may need to mark more work as "Portfolio Worthy".

3. Using URLs in your evidence is expected but beware that these can disappear so if you want to make sure we can find a resource then save a copy to pdf and add to your portfolio if its important enough.  Anything you have created should have the original in the portfolio, not a URL.  

4. Ensure you submit the directory named after the student directory, not the parent directory with this file in!  If you use the tools, this is not an issue.

5. Make sure you have marked work as worthy to be assessed.  This means that in the first column of the mapping.csv file only those pieces of evidence with TRUE in the first column will be marked.  Anything marked FALSE we will assume you do not consider worthy of assessment.  **If ALL evidence is marked FALSE then you will FAIL the module.**

6. Make sure that any files in the assets directory are referenced in a text file in your personal repository along with which learning outcomes they are relevent to and these text files are marked as "Portfolio Worthy".  If this is not done then we will not look at these files.

7. You should not have videos or audio files in your git repositories.  These should be in your assets directory.

8. All video and audio should be be at most 5 minutes long.  If its longer, please cut out the bit that is most relevant.  Videos and audio that is too long will be judged exceeding the word count limit and you will be penalised.

## What if I have a really slow connection?

Let the delivery staff know once you can create the zip file and we will give you a solution for your particular case.