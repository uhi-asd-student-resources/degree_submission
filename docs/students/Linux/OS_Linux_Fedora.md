# Getting started on Fedora

Start by updating the package manager repository listings:

## Installing libmagic

This is normally installed by default but in case its not.

```
sudo dnf -y install file-devel
```

## Installing ffmpeg

```
sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf -y install ffmpeg
```

Check installation with:

```
ffmpeg -version
```

## Installing python 3

```
sudo dnf -y update
sudo dnf -y install python3 python3-venv python3-pip
python3 -V
```

