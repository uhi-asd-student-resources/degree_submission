# Getting started on Ubuntu

Start by updating the package manager repository listings:

```
sudo apt -y update
```

## Installing libmagic

```
sudo apt -y install libmagic-dev
```

## Installing ffmpeg

```
sudo apt -y install ffmpeg
```

Check installation with:

```
ffmpeg -version
```

## Installing python 3

```
sudo apt -y update
sudo apt -y install python3 python3-venv python3-pip
python3 -V
```

