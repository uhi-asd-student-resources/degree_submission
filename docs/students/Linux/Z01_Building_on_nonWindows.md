# Setting up the degree submission tool on Linux or Mac

```
git clone git@bitbucket.org:uhi-asd-student-resources/degree_submission.git
./INSTALL.sh
pip install wheel
pip install -r requirements.txt
```

You are now ready to do the actual portfolio zip file creation, go to "Build Your Submission".

**Note that if you get errors about python missing Git then do the following at the command prompt**

```
# install the dependencies required, one of which is GitPython
pip install -r requirements.txt
```

