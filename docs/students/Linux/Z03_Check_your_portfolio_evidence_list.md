# Check your portal evidence list

Make sure you have at least 1 piece of work that you have marked as worthy to be assessed.  This means that in the first column of the mapping.csv file only those pieces of evidence with TRUE in the first column will be marked.  Anything marked FALSE we will assume you do not consider worthy of assessment.  If ALL evidence is marked FALSE then you will FAIL the module.
