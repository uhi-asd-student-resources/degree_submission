## Build Your Submission

> **Make sure you have at least 1 piece of work that you have marked as worthy to be assessed.  This means that in the first column of the mapping.csv file only those pieces of evidence with TRUE in the first column will be marked.  Anything marked FALSE we will assume you do not consider worthy of assessment.  If ALL evidence is marked FALSE then you will FAIL the module.**

### You are ready to configure your portfolio

This process will download the latest version of your repositories. It assumes your repositories are accessible without a username and password, and that keys are appropriately handled.  You can run this as many times as you need to, to update your repositories.


1. Change student number in submission_configuration to your student number.
2. Copy repositories.txt.example to repositories.txt.
3. Edit the repositories.txt file and add all your repositories for the year so far.
4. Download the mapping.csv and save this to the degree_submissions directory.
5. Run the following to create a build directory and clone your repositories.  This directory will be named the same as your student number.

```
./build_submission.sh
```

6. Add any videos, audio or other large files to the assets directory.  Please see instructions below on how to handle digital media evidence.
7. Modify the generated coversheets updating any field that has text in angled brackets like this: ```<<student number>>```.
8. You may add a file called "Comments for markers.txt" in the build directory for the markers to view with any additional information in.
9.  Run the following command again to complete the build process:

Next you can type:

```
./build_submission.sh
``` 

10. Continue as [Z10_Testing_Your_Zip_File_on_nonWindows.md](Z10_Testing_Your_Zip_File_on_nonWindows.md)
