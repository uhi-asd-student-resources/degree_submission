## Testing that your evidence can be found

The portfolio will automatically be extracted as it would be for the markers.  Check all warnings and when you are happy you can proceed to the next step.  If you do make a change, keep running ```buildportfolio.{sh/bat}``` until you are happy with your submission.
