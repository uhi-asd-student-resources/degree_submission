# Before you begin

You will need Git installed on your Mac before you begin.

Any files with an extension of ".bat" can be run in Windows Command Prompt.  Files with ".sh" are for Git Bash, Linux and Mac.

**Warning: should be using MacOS under the non-primary user account, then you may have issues with homebrew having permissions issues with /usr/local/Homebrew.  If this is the case then do the following:**

```
sudo chgrp admin /usr/local
sudo chmod g+w /usr/local
sudo chmod -R g+w /usr/local/Homebrew
```