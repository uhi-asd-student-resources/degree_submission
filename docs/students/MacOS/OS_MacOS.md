# Getting Started for Mac

## Install Homebrew

macOS doesn't come with a built in package manager (the `apt` bit in the Linux instructions above). The first step is to install [Homebrew](https://brew.sh), which is a mature, stable package manager for macOS. Open the Terminal app and type:

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

## Install libmagic

This may be installed alread but if not then try doing this:

```
brew install libmagic
```

## Install ffmpeg

You can now install ffmpeg with:

```
brew install ffmpeg
```

## Install Python 3

Some versions of macOS ship with Python installed. Run the following to find out which one is the default:

```
python -V
```

If it is Python 3, you are good to go. Otherwise, you problably want to use a tool called [pyenv](https://github.com/pyenv/pyenv) to manage your Python environments. 

1. Install it using `brew install pyenv` (go and make a ☕️ while it does this one)
2. Run the following commands to allow pyenv to manage the version that your Terminal uses: 
  2.1 `echo 'eval "$(pyenv init --path)"' >> ~/.zprofile`
  2.2 `echo 'eval "$(pyenv init -)"' >> ~/.zshrc`
3. Quit Terminal and re-open it.
4. Install the version of Python you want, for example `pyenv install 3.7.3`
5. Set this version as the 'global' version on your mac: `pyenv global 3.7.3`
6. Check that this has worked by typing `pyenv version`
7. Check your version again in a new Terminal window with: `python -V`
