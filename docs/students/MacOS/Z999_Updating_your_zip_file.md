# Updating your zip file

So you are near the end of semester and you want to update your submission zip file that you might have created earlier in the semester for a formative submission.

1. cd degree_submission
2. git pull
3. Edit the SubmissionConfiguration.json to make sure you have the correct student id, year and modules.
4. Edit the repositories.txt file to ensure all the repositories are there.
5. Download the latest mapping.csv file and place that in the top level directory.
6. ./build_submission.sh