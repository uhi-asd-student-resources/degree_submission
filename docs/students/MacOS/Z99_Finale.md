## Brightspace Submission Process

Submit your zip file to each appropriate module in Brightspace.

     1. Go to correct module in the Brightspace.
     2. Click Course Tools
     3. Click on Assignments
     4. Click on “End of module portfolio”
     5. Click on “Add a file”, select your zip file
     6. Click on Submit


Congratulations you have reached the end!
