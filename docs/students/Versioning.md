# Version

Make sure you are on the latest version by pulling the latest clone before attempting each submission.

To get the version number on Linux or Mac:

```
./build_submission.sh --version
# or
./build_submission.sh -V
```

on Windows:

```
build_submission.bat --version
# or
build_submission.bat -V
```
