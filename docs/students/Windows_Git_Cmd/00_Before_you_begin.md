# Before you begin

Make sure you have installed Git for Windows on your Windows machine.  We will need to use both **Git Bash** and **Git CMD** during the installation.  For updating your repository you should only require **Git CMD**.