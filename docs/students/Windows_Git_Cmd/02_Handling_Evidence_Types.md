# Handling different evidence types

## Paper Notes

If you prefer to write your notes on paper your have 3 options:

1. Take images of your notes and add them to a repository.  When you commit make sure you put the relevant learning outcome codes in the commit message.
2. Read your notes and get them transcribed (see Audio Transcription).
3. Use the image to text tools to convert them to digital files.

## Converting paper notes using Audio

* If you have done paper drawings, I recommend taking pictures of these and cropping them appropriately in a tool like Gimp, save them as PNG files.

* Once you have your transcription and your images:
  * Convert the transcription to markdown
  * Add links to the images appropriately
  * Add reference to other evidence.

* Re-read to check for spelling mistakes.

## Video Evidence

Where possible video and audio will be compressed by the submission application if its placed in the assets directory.  However if the video is still too long you can either try and manually compress it, as below, or preferably cut it down to the most relevant parts.

1. Identify and retrieve any videos of interest
2. Either using the command below or video editing software, chop out up to **10 minutes** of material or **15MB**.  Do NOT process the file in the assets directory.  **All log files will be erased in the assets directory.**

```
# This bash command will cut out a given timestamp and compress it.
# INPUT     - the input file you want to chop a piece out of
# IN        - the timestamp of the start of the piece of interest (format is: HH:MM:SS)
# OUT       - the timestamp of the end of the piece of interest (format is: HH:MM:SS)
# OUTPUT    - the output filename for the processed file (must end in ".mp4")

ffmpeg -i [INPUT] -ss [IN] -to [OUT] -pass 1 -f null /dev/null && \
    ffmpeg -i [INPUT] -filter_complex "[0:v]setpts=PTS/1.5[v];[0:a]atempo=1.5[a]" -map "[v]" -map "[a]" -pass 2 -f null /dev/null && \
    ffmpeg -i [INPUT] -filter_complex "scale=iw*min(1\,min(1024/iw\,512/ih)):-1" [OUTPUT]
```

For example:
```
ffmpeg -i "video.mp4" -ss "00:55:57" -to "00:56:43" -pass 1 -f null /dev/null && ffmpeg -i "video.mp4" -filter_complex "[0:v]setpts=PTS/1.5[v];[0:a]atempo=1.5[a]" -map "[v]" -map "[a]" -pass 2 -f null /dev/null && ffmpeg -i "video.mp4" -filter_complex "scale=iw*min(1\,min(1024/iw\,512/ih)):-1" "my_output.mp4"
```

To review a video quickly you can use the following command. This will playback the video at 1.75x speed.

```
mplayer -speed 1.75 -af scaletempo my_video.mp4
```


3. Copy the edited video into the assets directory.


## Audio Transcription

You can convert any audio files using the transcribe_audio.py file.

```
./bin/transcribe_audio.py <mp3 or wave file>
```

Example output:

```
Selected test_files/voice.mp3
Path: test_files
Base filename: voice
File Extension: .mp3
Converting to wave file 'test_files/~voice.wav'
Transcription has been written to 'test_files/voice_transcription.txt'


------------------------------------------------

Please format the transcription so it is readable, this
will be taken into account when marking your work.

------------------------------------------------
```

**You will need to clean the transcription up as its not perfect.  But it will help you capture written notes.**

