# Getting started on Windows

> > **Note this is MUCH more complicated in Windows because it does not come with an easy way to install all the components.  If you are comfortable using Linux or Linux VM, we recommend you use this as its quicker.**

## Install ffmpeg 

Install from [https://ffmpeg.org/download.html](https://ffmpeg.org/download.html).

> For a follow along video then see the Brightspace Short Videos page.

* Click on Downloads
* Click on the Windows Icon which should take you to a box titled Windows EXE files
* Click on the Windows builds from gyan.dev
* Download ffmpeg-git-full.7z, you may also need to download and install [7-zip](https://www.7-zip.org)
* Extract the ffmpeg directory to a suitable location. 
* Add this ffmpeg location to the path.

## Install GTK3 on Windows

These instructions are required by weasyprint, which you can find further information on your OS at [https://doc.courtbouillon.org/weasyprint/stable/first_steps.html](https://doc.courtbouillon.org/weasyprint/stable/first_steps.html).

* Got to https://github.com/tschoonj/GTK-for-Windows-Runtime-Environment-Installer/releases
* Download the latest gtk3-runtime-*.exe
* Run and follow the instructions (If you don’t know what some options mean, you can safely keep the default options selected.)


## Install Python 3 on Windows

> For a follow along video then see the Brightspace Short Videos page.

* Go to [https://www.python.org/downloads/windows/](https://www.python.org/downloads/windows/)
* Pick the latest stable release of Python **3**
* Scroll down to the bottom to find the downloads
* Pick the Windows Installer (64-bit) which is marked Recommended in the description.
* Once downloaded the ```python-<version>.exe``` file click on "Open File" or similar
* **Make sure you click on "Add Python X.Y to PATH" at the bottom of the first screen!**
* Select "Install Now"
* Click on Yes if Windows asks you anything
* Python will now install... wait
* Click on the "Disable path length limit"
* Click on close

> If you try and run python and it says it cannot find it then you did not tick the box to add python to your path!  Re-run the installer and try again!
