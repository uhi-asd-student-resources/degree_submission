
# Setting up on Windows with Git Bash

> For a follow along video then see the Brightspace Short Videos page.

* Open Git Bash
* Type: ```python -V``` to check python is working
* Type: ```ffmpeg -version``` to check ffmpeg is working
* Type: ```git clone git@bitbucket.org:uhi-asd-student-resources/degree_submission.git```
* Type: ```cd degree_submission```

Continue as per the [Z01_Building_on_nonWindows.md](Z01_Building_on_nonWindows.md)
