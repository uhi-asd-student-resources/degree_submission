# Setting up the degree submission tool with Windows and Git CMD

> For a follow along video then see the Brightspace Short Videos page.

* Open Git Bash
* Type: ```python -V``` to check python is working
* Type: ```ffmpeg -version``` to check ffmpeg is working
* Type: ```git clone git@bitbucket.org:uhi-asd-student-resources/degree_submission.git```
* Type: ```cd degree_submission```

* Run the "cmd" command as **administrator** (you can type this into Search hit enter)
* Type: ```cd degree_submission```
* You should be able to type ```python``` and it bring up the Python REPL, which is called IDLE.  You can do ```quit()``` to leave.  If this does not work then you need to rerun the installer and choose Modify, move through the options until you see "Add Python to environment variables" and then click on Install.  Once you have done this shutdown your cmd session and then restart it.  You should now have access to Python.
* Type: ```./INSTALL.bat```
* You can close this window now.
* Open up a new Git CMD Command Prompt (cmd) but as the user this time
* Type: ```cd degree_submission```

Go to [Z01_Building_on_Windows_with_GitCmd.md](Z01_Building_on_Windows_with_GitCmd.md)









