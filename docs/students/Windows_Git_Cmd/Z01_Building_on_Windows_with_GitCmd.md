# Setting up the degree submission tool

Open a Git Bash terminal and do the following:

```
git clone git@bitbucket.org:TomMcCallum/degree_submission.git
./INSTALL.sh
pip install wheel
pip install -r requirements.txt
```

You are now ready to being the actual portfolio zip file creation, go to "Build Your Submission".

If you have errors due to permissions remember you need to have your SSH public key in the SSH key agent.  In this example the key is ```~/.ssh/id_rsa``` in your home directory but yours might be something different.

```
eval $(ssh-agent -s)
ssh-add -l ~/.ssh/id_rsa 
```

**Note that if you get errors about python missing Git then do the following at the command prompt**

```

# install the dependencies required, one of which is GitPython
pip install -r requirements.txt
```



