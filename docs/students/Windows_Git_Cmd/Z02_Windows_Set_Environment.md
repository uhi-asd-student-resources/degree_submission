# Set the Python environment

## Check your SSH keys are in the current environment

This command should list the keys that you have in your local SSH agent:

```
ssh-add -l
```

If you get an error you may need to start the SSH agent and add your keys.
If you just get an empty list then you will need to add your keys to the agent.

