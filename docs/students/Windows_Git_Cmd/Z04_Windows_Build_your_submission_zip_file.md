# Build Your Submission

1. Change student number in submission_configuration to your student number.
2. Copy repositories.txt.example to repositories.txt.
3. Edit the repositories.txt file and add all your repositories for the year so far.
4. Download the mapping.csv and save this to the degree_submissions directory.

> **For Git CMD you need to start the ssh agent by using ```ssh-start-agent``` and add your certificate such as ```ssh-add ../.ssh/id_rsa``` or ```ssh-add ../.ssh/gitkraken_rsa```.  If you do not do this you will be unable to download your repositories and you will get errors!**

1. Run the following to create a build directory and clone your repositories.  This directory will be named the same as your student number.
   
```
# Windows command prompt
build_submission.bat
# Linux/Mac
./build_submission.sh
``` 

This process will also download the latest version of your repositories as found in ```repositories.txt```. It assumes your repositories are accessible without a username and password, and that keys are appropriately handled.  You can run this as many times as you need to, to update your repositories.
6. Add any videos, audio or other large files to the assets directory.  Please see instructions below on how to handle digital media evidence.
7. Modify the coversheet created for you by replacing text like this ```<<text>>``` with the relevant information.
8. You may add a file called "Comments for markers.txt" in the build directory for the markers to view with any additional information in.
9.  Run the following command again to complete the build process:

```
# Windows command prompt
build_submission.bat
# Linux/Mac
./build_submission.sh
``` 

10. Continue as per the Brightspace Submission Process section below