# Updating your zip file

So you are near the end of semester and you want to update your submission zip file that you might have created earlier in the semester for a formative submission.

0. Open Git **CMD**, not Git Bash
1. cd degree_submission
2. git pull
3. At this stage you might need to run the install script again 
4. Edit the SubmissionConfiguration.json to make sure you have the correct student id, year and modules.
5. Edit the repositories.txt file to ensure all the repositories are there.
6. Add any new coversheets, there should be 1 for each module.
7. Download the latest mapping.csv file and place that in the top level directory.
8. Follow the instructions from step Z01 onwards.


## [WinError 5] Access is denied

If you get these errors then try running Git CMD as Administrator rather than as the normal user.  