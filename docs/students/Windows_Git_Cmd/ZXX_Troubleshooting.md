# Troubleshooting

## I cannot see this Tom's bitbucket?

Ask Tom for access, you probably enrolled late and did not get put in the relevant group.

## When I try to use the URL from the video to access 'Principles of Programming', I end up with a "Not authorised message". (Img attached).

You have not been given to this module.  Message a delivery staff member on Slack and this will be fixed.

## Error: No repositories.txt found

Did you create it as instructed during the building phase?  You should have copied repositories.txt.example to repositories.txt.  Check the file spelling.

## ImportError: No module named git

Did you start the Python session before running the ```pip install < requirements.txt```?  If not go back and check the instructions for building your portfolio.

## Python not found

1. Is it installed? No, then install it.
2. Did you install Python 2 and not Python 3?  If so remove Python 2 and install Python 3.

## Error with clone git repository issues or password issues with keys

If you wish to keep the password on your key then you can do the following:

1. Have you started the SSH agent? In Git CMD use ```start-ssh-agent``` command.
2. Is the appropriate key in the ssh agent? Check ```ssh-add -l```
3. Check what your key is called by looking in the .ssh directory in under ```C:\Users\<your user name>\.ssh``` or on linux/Mac ```ls ~/.ssh```
4. Check using "git clone -v repo_to_test.git test" somewhere to check you can clone the repository manually from the command line.
5. Still having problems, contact the delivery staff to help.

otherwise you can remove the key with this command, hitting enter when it prompts for new password and confirmation of password.

```
ssh-keygen -p <path to ssh private key>
```


## Error: No coversheet found

1. Have you downloaded a coversheet from Brightspace? If not, then do so.
2. Did you rename the coversheet as instructed?  If not, then do so. It should have the format ```UI107001_Coversheet.docx``` where UI000000 is the module number.
3. Did you move the coversheet into the directory named after your student number?  If not, then please move the file to the correct directory.
4. Contact delivery staff for help.

## [WinError 5] Access is denied

If you get these errors then try running Git CMD as Administrator rather than as the normal user.  

## I have a really slow connection

Let the delivery staff know once you can create the zip file and we will give you a solution for your particular case.

## Brightspace upload stops halfway through in Firefox

Try Chrome, some have experienced issues with Firefox and uploading larger zip files, the cause is unknown.

## I have uploaded multiple times, is that ok?

We will always use the last completed upload as the submission.  We don't encourage multiple uploads but its also not the end of the world.

## This seems really hard and I am panicing!

1. Don't panic.
2. Grab your towel.
3. Message @staff on Slack and someone will be with you shortly.
4. Don't panic.

## How To Resolve ValueError: Unable to find resource t64.exe in package pip._vendor.distlib Error?

To Resolve ValueError: Unable to find resource t64.exe in package pip._vendor.distlib Error First Of all Open Your Terminal or Command  prompt. Then Just Uninstall Pip With This Command. 

Just Run This Command In Your Terminal.

```
python -m pip uninstall pip 
```

Now They Will Ask You Proceed (Y/n)? Just Type Y In Your Terminal. Now, Just Run this ensurepip Command: python -m ensurepip Then Just Install PIP with this command And it will Install Latest Version of PIP: 

```
python -m pip install -U pip 
```

Now Your PIP is Installed Successfully. The pip version can easily be checked using pip --version. Hope this works for you

An alternative is:

```
easy_install --upgrade pip
```

# Command prompt is complaining of no libmagic

Make sure the following are installed:

```
pip install python-magic-bin
pip install python-magic
```

If they are, uninstall them and reinstall them (yes I know, its windows):

```
pip uninstall python-magic-bin
pip uninstall python-magic
pip install python-magic-bin
pip install python-magic
```

# No entry point found for procedure deflateSetHeader in the dynamic library zlib1.dll

You most likely have an old version of zlib installed that doesn't contain the deflateSetHeader call. You should locate your dll and replace it with a newer version and/or reinstall a newer version of gtk.  If you rename the old version to zlib1.dll.old, and rerun it should then use the GTK3 version.

The old version of zlib1.dll was found on the C drive at C:\Program Files\Intel\WiFi\bin. So you might want to look there first if it exists on your machine.  This is very machine dependent and what libraries you have where will depend on what other applications have you installed.

