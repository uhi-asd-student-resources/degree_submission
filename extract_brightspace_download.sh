#!/bin/bash

PYTHON=$(which python3)

# catch when I set my environment wrong and workaround it
if [ "x${DEGREE_SUBMISSION_PATH}" == "x" -a "x${DEGREE_SUBMISSION}" != "x" ]; then
    DEGREE_SUBMISSION_PATH="${DEGREE_SUBMISSION}"
fi

if [ "x${DEGREE_SUBMISSION_PATH}" == "x" ]; then
    if [ -e "$(pwd)/$0" ]; then
        export PYTHONPATH=${PYTHONPATH}:"$(pwd)":"$(pwd)/src"
        LOCAL_PATH="."
    else
        echo "Being called in unexpected location and DEGREE_SUBMISSION_PATH not set."
        exit 1
    fi
else
    echo "Using DEGREE_SUBMISSION_PATH variable"
    LOCAL_PATH="${DEGREE_SUBMISSION_PATH}"
    export PYTHONPATH=${PYTHONPATH}:"${DEGREE_SUBMISSION_PATH}":"${DEGREE_SUBMISSION_PATH}/src"
fi
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

echo "Script path  :  ${SCRIPT_DIR}"
echo "Python 3 path: ${PYTHON}"
for ii in "$@"; do
    echo "Argument     : ${ii}"
done

if [ $#  -eq 0 ]; then
    "${PYTHON}" "${SCRIPT_DIR}/src/init.py" --extractclass
else
    "${PYTHON}" "${SCRIPT_DIR}/src/init.py" --extractclass "$@"
fi

echo "The extraction process has completed."
