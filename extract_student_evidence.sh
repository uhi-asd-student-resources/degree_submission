#!/bin/bash

PYTHON=$(which python3)

# catch when I set my environment wrong and workaround it
if [ "x${DEGREE_SUBMISSION_PATH}" == "x" -a "x${DEGREE_SUBMISSION}" != "x" ]; then
    DEGREE_SUBMISSION_PATH="${DEGREE_SUBMISSION}"
fi

if [ "x${DEGREE_SUBMISSION_PATH}" == "x" ]; then
    if [ -e "$(pwd)/$0" ]; then
        export PYTHONPATH=${PYTHONPATH}:"$(pwd)":"$(pwd)/src"
        LOCAL_PATH="."
    else
        echo "Being called in unexpected location and DEGREE_SUBMISSION_PATH not set."
        exit 1
    fi
else
    echo "Using DEGREE_SUBMISSION_PATH variable"
    LOCAL_PATH="${DEGREE_SUBMISSION_PATH}"
    export PYTHONPATH=${PYTHONPATH}:"${DEGREE_SUBMISSION_PATH}":"${DEGREE_SUBMISSION_PATH}/src"
fi
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

echo "${SCRIPT_DIR}"
echo "${PYTHON}"
for ii in "$@"; do
    echo "$ii"
done

if [ $#  -eq 0 ]; then
    "${PYTHON}" "${SCRIPT_DIR}/src/init.py" --extract
else
    "${PYTHON}" "${SCRIPT_DIR}/src/init.py" --extract "$@"
fi

echo "The extraction process has completed."

# N=$(grep CONFIRM *.log | wc -l)
# if [ $N -gt 0 ]; then    
#     echo "Here are some items you need to manually check."
#     grep CONFIRM *.log
#     echo
# fi

# N=$(grep "\(WARN\)\|\(CONFIRM\)" *.log | wc -l)
# if [ $N -gt 0 ]; then
#     echo "There were some warnings, check you know what these are and take appropriate action."
#     grep "\(WARN\)\|\(CONFIRM\)" *.log
#     echo
# fi

# N=$(grep ERROR *.log | wc -l)
# if [ $N -gt 0 ]; then
#     echo "There were some errors, check you know what these are and take appropriate action."
#     grep ERROR *.log
#     echo
# fi
