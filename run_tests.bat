@echo off

set PYTHONPATH=%PYTHONPATH%;$0\..\src\;$0\..\tests\
echo %PYTHONPATH%
python -m unittest discover tests "*_test.py"
