#!/usr/bin/env bash

if [ "$(uname)" == "Darwin" ]; then
    # Do something under Mac OS X platform        
    OS="DARWIN"
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    # Do something under GNU/Linux platform
    OS="LINUX"
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    # Do something under 32 bits Windows NT platform
    OS="WINDOWS"
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
    # Do something under 64 bits Windows NT platform
    OS="WINDOWS"
fi

# This works in linux but does not work in Git Bash
# because Python for Windows expects the PYTHONPATH to be
# in C:\Users\...\...\;C:\Users\...\... format like PATH.
if [ "x${OS}" == "xWINDOWS" ]; then
    WINDIR=$(cmd //c cd | sed "s_\\\\_\\\\\\\\_g")
    export PYTHONPATH="${PYTHONPATH};$WINDIR\\\\src;$WINDIR\\\\tests"
else
    export PYTHONPATH="${PYTHONPATH}:$(pwd):$(pwd)/src:$(pwd)/tests"
    if [ ! -d "test_files" ]; then
      echo "Requires test_files directory to run, check you have run 'git submodule update --init'"
      exit 1
    fi

    if [ ! -e "test_files/very_large_file.txt" ]; then
      echo "Creating 3.4G very_large_file.txt, this may take a while..."
      ./test_files/make_very_large_test_file.sh
    fi
fi

echo "Using PYTHONPATH=${PYTHONPATH}"
python helloworld.py

# Why does this hang on windows?
python -m unittest discover tests "*_test.py"
