import datetime
import os
from Constants import DEFAULT_SUBMISSION_CONFIG_FILENAME
import UnzipStudentPortfolio
import PrettyOutput
from ExtractEntryPoints import runExtractPerStudent
from ProgramArguments import ProgramArguments
import Logger

class BrightspaceSubmission:
    def __init__(self):
        self.raw = None                    # raw value
        self.firstNumber  = None           # 6 digit number at start of folder
        self.secondNumber = None           # 6 digit number at start of folder after hyphen
        self.firstName = None
        self.lastName = None
        self.submissionDate = None         # text version 
        self.submissionTime = None
        self.submissionTimestamp = None     # DateTime version
        self.path = None                    # path to this directory
        self.zipFiles = []
        self.portfolios = []
    
    def getDirectory(self):
        return os.path.join(self.path, self.raw)

    def size(self):
        return len(self.zipFiles)

    def hasNoZipFiles(self):
        return self.size() == 0

    def isValid(self):
        result = self.firstName is not None
        result &= self.lastName is not None
        result &= self.submissionTimestamp is not None
        result &= self.path is not None
        return result

    def loadZipFiles(self):
        self.zipFiles = []
        for root, _, files in os.walk(self.getDirectory()):
            for f in files:
                filePath = os.path.join(root, f)
                _, ext = os.path.splitext(f)
                if ext == ".zip":
                    self.zipFiles.append(filePath)
    
    def extract(self, userProgramArguments):
        for zipFile in self.zipFiles:
            studentNumber, _ = os.path.splitext(os.path.basename(zipFile))
            PrettyOutput.prettyInfo("Extracting portfolio {} for {}".format(self.raw, studentNumber))
            programArguments = ProgramArguments.createFromExisting(userProgramArguments)
            programArguments.portfolioPath = self.getDirectory()
            programArguments.action = "extract"
            programArguments.configFile = os.path.join(self.getDirectory(), studentNumber, DEFAULT_SUBMISSION_CONFIG_FILENAME)
            programArguments.studentNumber = studentNumber
            programArguments.zipPath = zipFile

            # internally if the student number is the test one 123456789 then
            # we assume the build user wanted this.
            programArguments.allowTestStudent = True
            programArguments.setLogFileName()
            Logger.info("LogFilePath: {}".format(programArguments.logFilePath))

            # this calls the unzip routine so no need to call twice
            runExtractPerStudent(programArguments)
            

    @staticmethod
    def create(directoryName, path=None):
        submission = BrightspaceSubmission()
        bits = directoryName.split(" ")
        first, second = bits[0].split("-")
        submission.path = path
        submission.raw = directoryName
        submission.firstNumber = first
        submission.secondNumber = second
        submission.firstName = bits[2]
        submission.lastName = bits[3]
        submission.submissionDate = "{} {} {}".format(bits[5], bits[6], bits[7])
        submission.submissionTime = "{} {}".format(bits[8], bits[9])
        dateStr = "{} {} BST".format(submission.submissionDate, submission.submissionTime)
        submission.submissionTimestamp = datetime.datetime.strptime(dateStr, "%d %B %Y %I%M %p %Z")
        return submission
