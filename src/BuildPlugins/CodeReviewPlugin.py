import PrettyOutput
from  PortfolioTreeVisitor import PortfolioTreeVisitor


class CodeReviewPlugin(PortfolioTreeVisitor):
    def __init__(self, config):
        super().__init__(config)
        self.videoFileCount = 0

    def matchFile(self, path, ext):
        return ext == ".mp4"
        
    def start(self):
        self.videoFileCount = 0
        return True

    def run(self, path):
        self.videoFileCount += 1
        return True

    def finish(self):
        if self.videoFileCount == 0:
            msg = "You have no video files (files with extension mp4), this could mean you are missing some code reviews. Check and rerun if you have left them out."
            PrettyOutput.prettyWarning(msg)

