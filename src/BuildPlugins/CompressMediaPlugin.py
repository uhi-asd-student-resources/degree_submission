import os
import ffmpeg
import magic
import PortfolioChecks
import Logger
from PortfolioTreeVisitor import PortfolioTreeVisitor
from PrettyOutput import prettyFixException, prettyInfo, prettyWarning
import Video


class CompressMediaPlugin(PortfolioTreeVisitor):
    def __init__(self, config):
        super().__init__(config)
        self.compressed= 0
        self.uncompressable = 0

    def matchFile(self, file, ext):
        return Video.isAllowedVideoFormatExtension(ext)

    def start(self):
        self.compressed = 0
        self.uncompressable = 0
        return True

    def run(self, path):
        beforeFileSize = os.path.getsize(path)
        mime = magic.Magic(mime=True)
        mime_type = mime.from_file(path)
        if "video" in mime_type:
            Logger.info("Found video: {} ({} MB)".format(
                path, round(beforeFileSize/1024/1024, 2)))
            if beforeFileSize > PortfolioChecks.FILESIZE_MAX_LIMIT:
                d = os.path.dirname(path)
                b, ext = os.path.splitext(os.path.basename(path))
                compressedFilePath = os.path.join(d, b + "_compressed" + ext)
                Video.compress_video(path, compressedFilePath,
                                    PortfolioChecks.FILESIZE_MAX_LIMIT)
                if os.path.isfile(compressedFilePath):
                    afterFileSize = os.path.getsize(compressedFilePath)
                    if afterFileSize > PortfolioChecks.FILESIZE_MAX_LIMIT:
                        self.uncompressable += 1
                        os.remove(compressedFilePath) # not worth keeping a bad compression
                        prettyWarning(
                            "Compressed file {} is still too large!  Manual cropping required.".format(compressedFilePath))
                        
                    else:
                        prettyInfo("Compressed {} down to {} MB. The new file is called {}. Please remove the original.".format(
                            path, round(afterFileSize/1024/1024, 2), compressedFilePath))
                        self.compressed += 1
        return True

    def finish(self):
        # If any action was taken then the user will need to clean out there assets directory anyway
        # so we throw an exception on both of these cases.
        if self.compressed > 0 or self.uncompressable > 0:
            prettyFixException("Please remove large files which have now been compressed.  If there are any files that have not been compressed, please crop and remove them from the assets directory.")
