import PrettyOutput
import re
import CoversheetUtilities
from PortfolioTreeVisitor import PortfolioTreeVisitor
import regex

class CoversheetChecksPlugin(PortfolioTreeVisitor):
    def __init__(self, config):
        super().__init__(config)
        self.unsignedCoversheetCount = 0
        self.signedCoversheetCount =0
        self.signedCoversheets = []

    def matchFile(self, path, ext):
        result = ext == ".docx" and (self._isCoversheetAlreadySigned(path) or self._isCoversheetNeedingToBeSigned(path))
        return result
    
    def _isCoversheetNeedingToBeSigned(self, path):
        expr = r"ui\d\d\d\d\d\d_coversheet_must_sign_before_submission\.docx"
        if re.search(expr, path, re.IGNORECASE):
            return True
        return False

    def _isCoversheetAlreadySigned(self, path):
        expr = r"ui\d\d\d\d\d\d_coversheet_signed\.docx"
        if re.search(expr, path, re.IGNORECASE):
            return True
        return False

    def start(self):
        self.unsignedCoversheetCount = 0 
        self.signedCoversheetCount =0
        return True

    def _whyIsCoversheetNotMarkedAsSigned(self, path):
        msg = "Coversheet {} has not been signed\n\t".format(path)
        isCompleted = CoversheetUtilities.isCoversheetCompleted(path)
        if not isCompleted:
            incompleteSections = CoversheetUtilities.whyIsCoversheetNotComplete(path)
            for item in list(set(incompleteSections)):
                msg += "\tSection '{}' has not been completed\n\t".format(item)
            PrettyOutput.prettyFixException(msg)
        else:
            PrettyOutput.prettyFixException("Coversheet {} is signed but filename has not changed. Rerun build script.".format(path))

    def run(self, path):
        if self._isCoversheetAlreadySigned(path):
            self.signedCoversheetCount += 1
            self.signedCoversheets.append(path)

        if self._isCoversheetNeedingToBeSigned(path):
            self.unsignedCoversheetCount += 1
            self._whyIsCoversheetNotMarkedAsSigned(path)
        return True

    def finish(self):
        n = len(list(set(self.config.moduleList)))
        if self.signedCoversheetCount != n:

            for csheet in self.signedCoversheets:
                PrettyOutput.prettyInfo("Coversheet found: {}".format(csheet))

            msg = "Not enough coversheets have been signed. Expected {} but found {}.\n\t".format(n, self.signedCoversheetCount)
            PrettyOutput.prettyFixException(msg)
