"""
Creates a new set of coversheets.
If a coversheet is already signed, then we change the file name to reflect this.
"""
import Logger
import docx
import os
import shutil
from DegreeModulesCollection import createDegreeModuleCollection
import PrettyOutput
import CoversheetUtilities
from PortfolioTreeVisitor import PortfolioTreeVisitor 

class CreateCoversheetsPlugin(PortfolioTreeVisitor):
    """
    This plugin is different as it does not operate on files and only happens once.
    Therefore, we have put the called in the start function just in case it gets put
    in the wrong place it will still only happen once per run.
    """
    def __init__(self, config):
        super().__init__(config)
        self.createdCoversheetCount =0
        self.signedCoversheetCount =0

    def _createCoversheetsForSigning(self):
        year = self.config.yearList
        modules = self.config.moduleList
        modulesCollection = createDegreeModuleCollection()
        n = len(year)
        if n != len(modules):
            PrettyOutput.prettyRuntimeException("Expected year and modules lists to be the same length, but they aren't. Contact delivery team.")

        for ii in range(n):
            yr = year[ii]
            m = modules[ii]
            #coversheetPath = modulesCollection.getCoversheetTemplatePath(yr, m)
            thisFilePath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "..")
            coversheetPath = os.path.join(thisFilePath, "coversheet_templates", "Cover Sheet.docx")
            unsignedCoversheetFileName = modulesCollection.getCoversheetNeedsSigningFileName(yr, m)
            unsignedCoversheetPath = os.path.join(self.config.buildDirectory, unsignedCoversheetFileName)
            Logger.info("Coversheet template: {}".format(coversheetPath))
            Logger.info("Coversheet to be signed: {}".format(unsignedCoversheetPath))
            signedFileName = modulesCollection.getCoversheetSignedFileName(yr, m)
            signedPath = os.path.join(self.config.buildDirectory, signedFileName)                
            if os.path.isfile(unsignedCoversheetPath) or os.path.isfile(signedPath):
                # oSize = os.path.getsize(coversheetPath)
                # nSize = os.path.getsize(unsignedCoversheetPath)
                # Logger.info("Comparing file sizes for signature, template: {} new: {}".format(oSize, nSize))
                # if nSize != oSize:
                #     # interesting a saved version in openoffice can be SMALLER than its original
                #     # so we need to compare for different not just if one is bigger than the other.
                if os.path.isfile(unsignedCoversheetPath):
                    if CoversheetUtilities.isCoversheetCompleted(unsignedCoversheetPath):
                        if not os.path.isfile(signedPath):
                            Logger.info("Changing coversheet name to signed {}".format(signedPath))
                            shutil.move(unsignedCoversheetPath, signedPath)
                            self.signedCoversheetCount += 1
                        else:
                            Logger.info("Coversheet is now signed: {}".format(signedPath))
                    else:
                        Logger.info("Signed coversheet {} is not completed".format(signedPath))
                elif os.path.isfile(signedPath):
                    Logger.info("Coversheet is now signed: {}".format(signedPath))
            else:
                Logger.info("Creating coversheet to be signed {}".format(unsignedCoversheetPath))
                #shutil.copyfile(coversheetPath, unsignedCoversheetPath)
                modulesCollection.fillInCoversheet(coversheetPath, unsignedCoversheetPath, yr, m)
                self.createdCoversheetCount += 1

    def start(self):
        self.createdCoversheetCount =0
        self.signedCoversheetCount =0
        self._createCoversheetsForSigning()

    def run(self):
        return True
    
    def finish(self):
        pass
