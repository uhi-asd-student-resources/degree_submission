import os
import PrettyOutput
from PortfolioTreeVisitor import PortfolioTreeVisitor


class DotGitDirectoryExistsPlugin(PortfolioTreeVisitor):
    def __init__(self, config):
        self.config = config
        self.dotGitDirectoryCount = 0

    def matchDirectory(self, path):
        if os.path.isdir(os.path.join(path, ".git")):
            return True
        return False
            
    def start(self):
        self.dotGitDirectoryCount = 0
        return True

    def run(self, path):
        self.dotGitDirectoryCount += 1
        return True

    def finish(self):
        if self.dotGitDirectoryCount == 0:
            msg = "Could not find any git repositories in your build directory - this is weird, have you deleted the .git directories perhaps?  If so, please don't."
            PrettyOutput.prettyFixException(msg)
            
