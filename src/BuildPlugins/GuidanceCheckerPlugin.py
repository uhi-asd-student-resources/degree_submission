"""
Takes a specification from guidance.json and checks for matches.
"""
import os
from PortfolioTreeVisitor import PortfolioTreeVisitor
import PrettyOutput
import json
import regex
import Logger
from DirectoryUtils import isDotGitDirectory, isGitRoot

GUIDANCE_JSON = "guidance.json"

class GuidanceCheckerPlugin(PortfolioTreeVisitor):
    """
    The guidance checker runs across the whole build directory.
    It could be refactored to do a better job of fitting in with the plugin system but
    for now its a bit of an odd duck so we just call it at the end.
    """
    def __init__(self, config):
        super().__init__(config)

    def matchDirectory(self, path):
        return False

    def matchFile(self, path, ext):
        return False

    def checkGuidanceSpecIsValid(self, spec):
        expectedKeys = [ "matchType", "match", "name", "message", "errorLevel", "type" ]
        if "inGitRoot" not in spec:
            spec["inGitRoot"] = False
        if "inGitRoot" in spec:
            v = spec["inGitRoot"]
            if v != True and v != False:
                raise ValueError("inGitRoot key must be true or false")

        for e in expectedKeys:
            if e not in spec:
                raise ValueError("guidance specification is missing '{}' key".format(e))    
        v = spec["type"]
        if v != "file" and v != "directory":
            raise ValueError("type key must have value either file or directory")
        v = spec["matchType"]
        if v != "exists" and v != "missing":
            raise ValueError("type key must have value either file or directory")
        if not isinstance(spec["match"], list):
            raise ValueError("match value should be a list")
        spec["errorLevel"] = spec["errorLevel"].lower()
        v = spec["errorLevel"]
        if v != "guidance" and v != "info" and v != "error" and v != "warning":
            raise ValueError("errorLevel key must have a value from [ guidance, info, warning, error ]")
        return spec

    def doGuidanceExistanceCheck(self, directory, spec):
        # When checking for existance a list of matches means that at least one matches
        # so we interpret these as OR
        matches = spec["match"]
        matchWhat = spec["type"]
        inGitRoot = spec["inGitRoot"]
        for root, dirs, files in os.walk(directory):
            if inGitRoot and not isGitRoot(root):
                continue
            if isDotGitDirectory(root):
                continue
            for m in matches:
                if matchWhat == "directory":
                    for d in dirs:
                        if regex.match(m, d, regex.IGNORECASE):
                            msg = spec["message"]
                            msg = msg.replace("{0}", os.path.join(root,d))
                            msg = msg.replace("{1}", m)
                            PrettyOutput.showGuidance(spec["errorLevel"], msg)
                if matchWhat == "file":
                    for f in files:
                        if regex.match(m, f, regex.IGNORECASE):
                            msg = spec["message"]
                            msg = msg.replace("{0}", os.path.join(root,f))
                            msg = msg.replace("{1}", m)
                            PrettyOutput.showGuidance(spec["errorLevel"], m)
                        

    def doGuidanceMissingCheck(self, directory, spec):
        # When checking for missing a list of possible alternatives is given
        # so they ALL have to be missing - interpreted as an AND
        matches = spec["match"]
        matchWhat = spec["type"]
        for root, dirs, files in os.walk(directory):
            if not isGitRoot(root):
                continue
            if isDotGitDirectory(root):
                continue
            doesExist = False
            for m in matches:
                if matchWhat == "directory":
                    for d in dirs:
                        if regex.match(m, d, regex.IGNORECASE):
                            doesExist = True
                            break
                if matchWhat == "file":
                    for f in files:
                        #print("{} {} {}".format(root,m,f))
                        if regex.match(m, f, regex.IGNORECASE):
                            doesExist = True
                            break
            if not doesExist:
                msg = spec["message"]
                msg = msg.replace("{0}", root)
                msg = msg.replace("{1}", m)
                PrettyOutput.showGuidance(spec["errorLevel"], msg)
        

    def doGuidanceCheck(self, directory, spec):
        Logger.info("Guidance checker: {}".format(directory))
        Logger.info("Specification: {}".format(spec["name"]))

        if spec["matchType"] == "exists":
            self.doGuidanceExistanceCheck(directory, spec)
        else:
            self.doGuidanceMissingCheck(directory, spec)

    def guidanceChecker(self, directory):
        thisFilePath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        guidanceJsonFile = os.path.join(thisFilePath, "data", GUIDANCE_JSON)
        with open(guidanceJsonFile, "r") as inFile:
            guidanceSpec = json.load(inFile) 

            for spec in guidanceSpec:
                spec = self.checkGuidanceSpecIsValid(spec)
                self.doGuidanceCheck(directory, spec)
    
    def start(self):
        return True

    def run(self, path):
        return True

    def finish(self):
        self.guidanceChecker(self.config.buildDirectory)