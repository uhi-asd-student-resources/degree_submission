import os
import PrettyOutput
from PortfolioTreeVisitor import PortfolioTreeVisitor

FILESIZE_MAX_LIMIT = 15 * 1024 * 1000

class MediaFileConstraintsPlugin(PortfolioTreeVisitor):
    """
    This plugin assumes you have already attempted compressin in CompressMediaPlugin.
    This provides a final check all is well.
    """
    def __init__(self, config):
        super().__init__(config)
        self.largeFiles = []

    def matchFile(self, path, ext):
        fileSize = os.path.getsize(path)
        return fileSize > FILESIZE_MAX_LIMIT

    def start(self):
        self.largeFiles = []
        return True

    def run(self, path):
        if os.path.isfile(path):
            self.largeFiles.append(path)
        return True

    def finish(self):
        if len(self.largeFiles) > 0:
            largeFilesText = '\n\t'.join(self.largeFiles)
            PrettyOutput.prettyFixException("There are files which are too large, please compress or remove these before submitting to Brightspace.\n\t{}".format(largeFilesText))
    