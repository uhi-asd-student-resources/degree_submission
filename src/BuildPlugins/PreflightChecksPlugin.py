import PrettyOutput
import Logger
import os
import shutil
import Repositories
from Utilities import isNewerFile
from PortfolioTreeVisitor import PortfolioTreeVisitor
import SubmissionConfiguration

class PreflightChecksPlugin(PortfolioTreeVisitor):
    """
    This is slightly different in that its run once per portfolio build.
    """
    def __init__(self, config):
        super().__init__(config)

    def _createSubmissionDirectoryIfNotExists(self, directory):
        if os.path.isdir(directory):
            Logger.info("{} already exists".format(directory))
            return False
        else:
            Logger.info("Creating submission directory: {}".format(directory))
            os.mkdir(directory)
            return True


    def _createAssetsDirectoryIfNotExists(self, directory):
        if os.path.isdir(directory):
            Logger.info("{} already exists".format(directory))
            return False
        else:
            Logger.info("Creating media assets directory: {}".format(directory))
            os.mkdir(directory)
            return True

    def _checkAndUpdateRepositoriesTxt(self):
        
        if not os.path.isfile(self.config.configRepositoryListFilePath):
            if not os.path.isfile(self.config.repositoryListFilePath):
                PrettyOutput.prettyFixException("Expected to find the file {}.\n\tCheck if this file exists, if it does not then you need to copy repositories.txt.example to repositories.txt.\n\tAdd your repository urls to this file.".format(self.config.configRepositoryListFilePath))
        if os.path.isfile(self.config.configRepositoryListFilePath):
            # always copy over the repositories.txt in the build directory 
            # if there is one in the parent directory
            if isNewerFile(self.config.configRepositoryListFilePath, self.config.repositoryListFilePath):
                Logger.info("Overwriting {} with {}".format(self.config.configRepositoryListFilePath, self.config.repositoryListFilePath))
                shutil.copyfile(self.config.configRepositoryListFilePath, self.config.repositoryListFilePath)
        

    def _checkAndCopyConfigFileIfExists(self):
        # TODO(tm) should we only be copying over NEWER version?
        if self.config.configFilePath:
            shutil.copyfile(self.config.configFilePath, self.config.buildConfigFilePath)
        
    def _readRepositoriesListAndClone(self):
        repositoryList = self.config.readRepositories()
        repositoryList = Repositories.clone_repository_list(repositoryList, self.config.buildDirectory)
        self.config.repositoryList = repositoryList

    def _check_for_mapping_file(self, otherPlacesToLook=[]):
        """
        In general the mapping file in the PORTFOLIO BUILD directory is used as the 
        basis for the configuration, however sometimes the student will copy it into the 
        parent directory. So we need to copy it in.
        If they have a problem, then sometimes they overwrite only the parent one and not the
        one in the build directory, so we check and copy that back over.  
        We do NOT change the one in the parent directory.
        """
        overwriteMappingsFileInBuildDirectory = False

        mappingFileInBuildDirectoryFilePath = os.path.join(self.config.buildDirectory, SubmissionConfiguration.DEFAULT_MAPPING_FILENAME)
        if not os.path.isfile(mappingFileInBuildDirectoryFilePath):
            overwriteMappingsFileInBuildDirectory = True

        absolutePath = os.path.abspath(mappingFileInBuildDirectoryFilePath)
        parentPath = os.path.dirname(os.path.dirname(absolutePath))
        otherPlacesToLook.append(parentPath)
        for location in otherPlacesToLook:
            # first, check for mapping.csv in the location
            potentialReplacement = os.path.join(location, "mapping.csv")
            if os.path.isfile(potentialReplacement):
                if overwriteMappingsFileInBuildDirectory:
                    Logger.info("Copying {} to {}".format(
                        potentialReplacement, mappingFileInBuildDirectoryFilePath))
                    shutil.copyfile(potentialReplacement, mappingFileInBuildDirectoryFilePath)
                    break
                else:
                    # is the one in our location newer than the one that already exists
                    overwriteBuildDirectoryMappingFile = isNewerFile(potentialReplacement, mappingFileInBuildDirectoryFilePath)

                    if overwriteBuildDirectoryMappingFile:
                        Logger.info("Overwriting {} to {}".format(
                            potentialReplacement, mappingFileInBuildDirectoryFilePath))
                        shutil.copyfile(potentialReplacement, mappingFileInBuildDirectoryFilePath)
                        break
        
        if os.path.isfile(mappingFileInBuildDirectoryFilePath):
            Logger.info("{} found".format(mappingFileInBuildDirectoryFilePath))
        else:
            PrettyOutput.prettyFixException("{} was not found, please export from portal and then retry".format(
                mappingFileInBuildDirectoryFilePath))

    def _makeCommentsForMarkersTextFileIfNotExists(self):
        buildDirectory = self.config.buildDirectory
        instructionsMessage = """
        # Use this file to let the markers know if you have an extended periods of illness
        # or have been in employment for a period during the semester.  This should be in the
        # following format:
        #   StartDate-EndDate     Reason
        # This allows the markers to be aware of situations that may need to be accounted for
        # when assessing your work.  All absences will be marked in line with UHI Academic
        # Regulations.
        # Large periods of extended absense may be covered by Mitigating Circumstances so
        # contact the Programme Lead before submission.
        #
        """
        fileName = "Comments for markers.txt"
        filePath = os.path.join(buildDirectory, fileName)
        if os.path.isfile(filePath):
            return
        with open(filePath, "w") as outFile:
            outFile.write(instructionsMessage)
        Logger.info("Creating {}".format(filePath))

    def start(self):
        """
        Check the config object for stuff that we need that might not be there.
        """
        if not self.config or not self.config.configFilePath:
            PrettyOutput.prettyRuntimeException(
                "unexpected error: configFilePath not set")
        
        didWeHaveToCreateTheBuildDirectory = self._createSubmissionDirectoryIfNotExists(self.config.buildDirectory)
        self.config.prepareDirectoryNotFullBuild = didWeHaveToCreateTheBuildDirectory
        self._createAssetsDirectoryIfNotExists(self.config.assetsDirectory)
        self._makeCommentsForMarkersTextFileIfNotExists()
        self._checkAndCopyConfigFileIfExists()
        self._check_for_mapping_file()
        self._checkAndUpdateRepositoriesTxt()
        self._readRepositoriesListAndClone()
        # return False if we should not continue on with build after
        # the preparation stage
        return not self.config.prepareDirectoryNotFullBuild
