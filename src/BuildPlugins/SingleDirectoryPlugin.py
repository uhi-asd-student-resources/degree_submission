"""
Checks for single directory with no other files or anything else. These are a directory antipattern.
"""
import os
from PortfolioTreeVisitor import PortfolioTreeVisitor
import PrettyOutput
from DirectoryUtils import isDotGitDirectory

class SingleDirectoryPlugin(PortfolioTreeVisitor):
    def __init__(self, config):
        super().__init__(config)
        self.singleDirectoryCount= 0
        self.loneDirectories = []

    def matchDirectory(self, path):
        for _, dirs, files in os.walk(path):
            return len(files) == 0 and len(dirs) == 1
        return False

    def _checkForSingleDirectory(self, directory):
        self.loneDirectories.append(directory)
        self.singleDirectoryCount += 1
    
    def start(self):
        self.singleDirectoryCount = 0
        return True

    def run(self, path):
        if os.path.isdir(path):
            self._checkForSingleDirectory(path)
        return True

    def finish(self):
        if len(self.loneDirectories) > 0:
            loneDirectoryText = '\n'.join(self.loneDirectories)
            PrettyOutput.prettyGuidance("Found a lone directory '{}'. A lone directory is a directory that has no files and only a single directory in it, this extra directory is there for no other reason that to make the marker do an extra click. This is an antipattern that you should try to avoid as it adds extra clicks for the user for no benefit. Don't change it to get rid of it, just try to avoid them please.".format(loneDirectoryText))

