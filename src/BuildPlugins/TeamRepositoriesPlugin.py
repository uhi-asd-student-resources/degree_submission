from PortfolioTreeVisitor import PortfolioTreeVisitor
import Repositories
from DirectoryUtils import isGitRoot
import PrettyOutput
import Logger

class TeamRepositoriesPlugin(PortfolioTreeVisitor):
    def __init__(self, config):
        super().__init__(config)
        self.teamRepositories = []
        self.soloRepositories = []
        self.warnings = []
        self.info = []
        self.authors = []
        if self.config is not None and self.config.mappingRecords is not None:
            self.authors = self.config.mappingRecords.getAuthors()
    

    def matchDirectory(self, path):
        return isGitRoot(path)

    def start(self):
        self.teamRepositories = []
        self.soloRepositories = []
        self.warnings = []
        return True

    def run(self, path):
        if len(self.authors) > 0:
            if Repositories.is_team_project(path, self.authors):
                info = "Repository '{}' identified as a team project".format(path)
                self.teamRepositories.append(path)
                Logger.info(info)
            else:
                self.soloRepositories.append(path)
                info = "Repository '{}' identified as a solo project".format(path)
                Logger.info(info)
        return True

    def finish(self):
        if len(self.teamRepositories) == 0:
            warning = "No team projects found.  If you were part of a team, please add the repository to the repositories.txt file so that your contribution can be assessed."
            self.warnings.append(warning)
            if self.config is not None:
                PrettyOutput.prettyWarning(warning)


