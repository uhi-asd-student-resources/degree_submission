import docx
import os
import re


COVERSHEET_TEMPLATE=os.path.join(os.path.dirname(os.path.dirname(__file__)), "coversheet_templates", "Cover Sheet.docx")
assert(os.path.isfile(COVERSHEET_TEMPLATE))

def whyIsCoversheetNotComplete(path):    
    template = docx.Document(COVERSHEET_TEMPLATE)
    doc = docx.Document(path)

    tags = [
        "Your student number",
        "Your college",
        "The date of submission here",
        "Type your first and last name here or put a signature",
        "Date Submitted"
    ]
    
    incompleteSections = []
    for tableIndex, t in enumerate(doc.tables):
        for rowIndex, row in enumerate(t.rows):
            for cellIndex, cell in enumerate(row.cells):
                # print("CELL {}".format(cell.text))
                for tag in tags:
                    if "<<{}>>".format(tag.lower()) in str(cell.text).strip().lower():
                        if tag not in incompleteSections:
                            # print("{} {} {}".format(path, cell.text, tag))
                            incompleteSections.append(tag)

                    # check blank cells that were marked with a tag in the original
                    if str(template.tables[tableIndex].rows[rowIndex].cells[cellIndex].text).strip().lower() == "<<{}>>".format(tag.lower()):
                        cleanedText = re.sub(r"\s*","",str(cell.text))
                        if len(cleanedText) == 0:
                            if tag not in incompleteSections:
                                # print("{} {} {}".format(path, cell.text, tag))
                                incompleteSections.append(tag)
    return incompleteSections

def isCoversheetCompleted(path):
    reasons = whyIsCoversheetNotComplete(path)
    return len(reasons) == 0

