"""
Basic class for a degree module
"""

WORD_COUNTS_MIN = [ 0, 2500, 3000, 3500, 4000]
WORD_COUNTS_MAX = [ 0, 2500, 3000, 3500, 4000]


class DegreeModule:
    def createFromCsv(row):
        module = DegreeModule()
        module.year = int(row[0])
        module.code = row[1]
        module.number = int(module.code[len(module.code)-2:])
        module.name = row[2]
        module.semester = row[3]
        module.dateIssued = row[4]
        module.dateDue = row[5]
        module.wordCount = int(row[6])
        module.lecturer = row[7]
        module.learningOutcomes = row[8]
        return module

    def __init__(self):
        self.year = None
        self.name = None
        self.code = None
        self.number = None                  # last two digits of the course code
        self.semester = None
        self.lecturer = None
        self.dateIssued = None
        self.dateDue = None

    def getWordCount(self):
        return [WORD_COUNTS_MIN[self.year], WORD_COUNTS_MAX[self.year]]
