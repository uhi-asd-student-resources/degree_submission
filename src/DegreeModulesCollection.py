import csv
import os
import Logger
import docx
import LearningOutcomesCollection
from DegreeModule import DegreeModule

class DegreeModulesCollection:
    def __init__(self):
        self.modules = []

    def add(self, module):
        for m in self.modules:
            if m.name == module.name:
                raise ValueError("module already exists")
        self.modules.append(module)

    def getWordCount(self):
        wordCount = [0,0]
        for m in self.modules:
            wc = m.getWordCount()
            wordCount[0] += wc[0]
            wordCount[1] += wc[1]
        return wordCount

    def log(self):
        for m in self.modules:
            Logger.info("[MODULE] {} {} {}".format(m.year, m.code, m.name))

    def size(self):
        return len(self.modules)

    def find(self, text:str):
        """
        Find a module by either the course code or its name
        """
        for m in self.modules:
            if m.code.upper() == text.upper() or m.name.upper() == text.upper():
                return m
        return None

    def findByNumber(self, yr, n):
        for m in self.modules:
            if m.number == int(n) and m.year == int(yr):
                return m
        return None

    def filter(self, yearList, moduleList):
        assert(len(yearList) == len(moduleList))
        newCollection = DegreeModulesCollection()
        for index, moduleNumber in enumerate(moduleList):
            for m in self.modules:
                if m.year == yearList[index] and m.number == moduleNumber:
                    newCollection.add(m)
        return newCollection

    # def getCoversheetTemplatePath(self, year, moduleNumber):
    #     sourcePath = os.path.join("coversheet_templates", "Year"+str(year))
    #     module = self.findByNumber(year, moduleNumber)
    #     if module is None:
    #         raise ValueError("Could not find module for {},{}".format(year, moduleNumber))
    #     fileName = module.code + " Assessment Cover Sheet.docx"
    #     templatePath = os.path.join(sourcePath, fileName)
    #     return templatePath

    def getCoversheetNeedsSigningFileName(self, year, moduleNumber):
        module = self.findByNumber(year, moduleNumber)
        if module is None:
            raise ValueError("Could not find module for {},{}".format(year, moduleNumber))
        fileName = module.code + "_coversheet_must_sign_before_submission.docx"
        return fileName

    def getCoversheetSignedFileName(self, year, moduleNumber):
        module = self.findByNumber(year, moduleNumber)
        if module is None:
            raise ValueError("Could not find module for {},{}".format(year, moduleNumber))
        fileName = module.code + "_coversheet_signed.docx"
        return fileName

    def createAllCoversheets(self, targetPath):
        sourcePath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        sourcePath = os.path.join(sourcePath, "coversheet_templates", "Cover Sheet.docx")
        if not os.path.isdir(targetPath):
            raise ValueError("targetPath {} is not a valid directory".format(targetPath))
        if not os.path.isfile(sourcePath):
            raise ValueError("Coversheet {} template is not valid".format(sourcePath))
        created = []
        for m in self.modules:
            newTargetPath = os.path.join(targetPath, "{}_must_be_signed_before_submission.docx".format(m.code))
            self.fillInCoversheet(sourcePath, newTargetPath, m.year, m.number)
            created.append(newTargetPath)
        return created

    def fillInCoversheet(self, sourcePath, targetPath, year, moduleNumber):
        module = self.findByNumber(year, moduleNumber)
        if module is None:
            raise ValueError("Could not find module for {},{}".format(year, moduleNumber))
        doc = docx.Document(sourcePath)
        titleStyle = doc.styles["DegreeTitle"]
        for p in doc.paragraphs:
            if "<<module>>" in p.text:
                p.style = titleStyle
                p.text = module.name
        learningOutcomesCollection = LearningOutcomesCollection.createDefaultLearningOutcomesCollection()

        contentStyle = doc.styles["ModuleInfoText"]
        for t in doc.tables:
            for i, row in enumerate(t.rows):
                for cell in row.cells:
                    if "<<date issued>>" in cell.text:
                        cell.text = module.dateIssued
                    if "<<date due>>" in cell.text:
                        cell.text = module.dateDue
                    if "<<module>>" in cell.text:
                        cell.text = module.name
                    if "<<code>>" in cell.text:
                        cell.text = module.code
                    if "<<learning outcomes>>" in cell.text :
                        cell.text = module.learningOutcomes
                    if "<<word count>>" in cell.text:
                        cell.text = str(module.wordCount) + " (equivalent)"
                    if "<<lecturer>>" in cell.text:
                        cell.text = module.lecturer
                    if "<<LO1>>" in cell.text:
                        lo = learningOutcomesCollection.filterByModuleNumberAndLoIndex(module.year, module.number, 1)
                        cell.text = lo.loCode
                    if "<<LO2>>" in cell.text:
                        lo = learningOutcomesCollection.filterByModuleNumberAndLoIndex(module.year, module.number, 2)
                        cell.text = lo.loCode
                    if "<<LO3>>" in cell.text:
                        lo = learningOutcomesCollection.filterByModuleNumberAndLoIndex(module.year, module.number, 3)
                        cell.text = lo.loCode
                    if "<<LO4>>" in cell.text:
                        lo = learningOutcomesCollection.filterByModuleNumberAndLoIndex(module.year, module.number, 4)
                        cell.text = lo.loCode
                    if "<<LO5>>" in cell.text:
                        lo = learningOutcomesCollection.filterByModuleNumberAndLoIndex(module.year, module.number, 5)
                        cell.text = lo.loCode
                    if "<<LO1DESC>>" in cell.text:
                        lo = learningOutcomesCollection.filterByModuleNumberAndLoIndex(module.year, module.number, 1)
                        cell.text = lo.description
                    if "<<LO2DESC>>" in cell.text:
                        lo = learningOutcomesCollection.filterByModuleNumberAndLoIndex(module.year, module.number, 2)
                        cell.text = lo.description
                    if "<<LO3DESC>>" in cell.text:
                        lo = learningOutcomesCollection.filterByModuleNumberAndLoIndex(module.year, module.number, 3)
                        cell.text = lo.description
                    if "<<LO4DESC>>" in cell.text:
                        lo = learningOutcomesCollection.filterByModuleNumberAndLoIndex(module.year, module.number, 4)
                        cell.text = lo.description
                    if "<<LO5DESC>>" in cell.text:
                        lo = learningOutcomesCollection.filterByModuleNumberAndLoIndex(module.year, module.number, 5)
                        cell.text = lo.description
                    cell.paragraphs[0].style = contentStyle
        doc.save(targetPath)
        

def createDegreeModuleCollection():
    thisFilePath = os.path.dirname(os.path.abspath(__file__))
    moduleFilePath = os.path.join(thisFilePath, "data", "modules.csv")
    modules = DegreeModulesCollection()
    if not os.path.isfile(moduleFilePath):
        raise RuntimeError("Missing learning outcomes data file ({}).".format(moduleFilePath))
    with open(moduleFilePath, "r",  encoding="utf8") as inFile:
        data = csv.reader(inFile, delimiter=',', quotechar='"')
        for index, row in enumerate(data):
            if index == 0:
                continue
            elif len(row) == 0:
                continue
            elif len(row[0].strip()) == 0:
                continue
            elif row[0].strip()[0] == '#':
                continue
            else:
                module = DegreeModule.createFromCsv(row)
                modules.add(module)
                # Logger.info("[Module] {} {} {} {} {}".format(module.year, module.code, module.number, module.name, module.semester))
    if modules.size() != 22:
        raise RuntimeError("Found only {} modules, not the expected 24.  File used was {}.".format(modules.size(), moduleFilePath))     
    return modules