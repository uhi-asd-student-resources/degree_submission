import os
import shutil
import regex
import hashlib
import Logger

DOT_GIT_DIRECTORY_NAME = ".git"


def isDotGitDirectory(dir):
    """
    Check if the directory is a .git
    """
    intermediateDirectory = os.path.sep + DOT_GIT_DIRECTORY_NAME + os.path.sep
    lastDirectory = os.path.basename(dir)
    return lastDirectory == DOT_GIT_DIRECTORY_NAME or intermediateDirectory in dir


def isGitRoot(dir):
    """
    Does this directory have a .git
    """
    gitDot = os.path.join(dir, DOT_GIT_DIRECTORY_NAME)
    return os.path.isdir(gitDot)


def findFileInSiblingDirectories(filePath):
    """
    Given a file we look at the other sibling directories
    for the file
    """
    if os.path.isdir(filePath):
        raise RuntimeError(
            "{} is a directory, should be a file at least".format(filePath))
    currentDir = os.path.dirname(filePath)
    baseFileNameToFind = os.path.basename(filePath)
    for root, _, _ in os.walk(currentDir):
        if isDotGitDirectory(root):
            continue

        # check as the user has written it
        filePathToCheckExists = os.path.join(root, baseFileNameToFind)
        if os.path.isfile(filePathToCheckExists):
            return filePathToCheckExists

        # if the user has come from Windows then the user may have used the wrong case
        # for part of the filename so we try a variety
        name, ext = os.path.splitext(baseFileNameToFind)
        filePathToCheckExists = os.path.join(root, name+ext.upper())
        if os.path.isfile(filePathToCheckExists):
            return filePathToCheckExists

        filePathToCheckExists = os.path.join(root, name+ext.lower())
        if os.path.isfile(filePathToCheckExists):
            return filePathToCheckExists

        filePathToCheckExists = os.path.join(root, name.upper()+ext.upper())
        if os.path.isfile(filePathToCheckExists):
            return filePathToCheckExists

        filePathToCheckExists = os.path.join(root, name.lower()+ext.lower())
        if os.path.isfile(filePathToCheckExists):
            return filePathToCheckExists
    return None


def findFileInRepository(path):
    """
    An obsidian path can be relative to the root of the library and therefore if we 
    are in a file that is a subdirectory of the library we won't necessarily find it where 
    we think it should be.
    Therefore we need to search the the repository for the file with a matching file name.
    """
    if os.path.isfile(path):
        return path
    basename = os.path.basename(path)
    currentDir = os.path.dirname(path)
    parentDir = os.path.dirname(currentDir)
    # check subdirectories for the file
    for root, dirs, _ in os.walk(currentDir):
        for d in dirs:
            newPath = os.path.join(root, d, basename)
            foundPath = findFileInSiblingDirectories(newPath)
            if foundPath is not None:
                return foundPath

    if isGitRoot(currentDir):
        return None
    foundPath = findFileInRepository(os.path.join(parentDir, basename))
    return foundPath


def extractCopyOfFile(config, originalLocation, destinationLocation):
    config.mapExtractedFilesToOriginalFiles[destinationLocation] = originalLocation
    shutil.copy(originalLocation, destinationLocation)


def isLatex(fileName):
    fileName, ext = os.path.splitext(fileName)
    return ext.lower() == ".tex"


def isMarkdown(fileName):
    fileName, ext = os.path.splitext(fileName)
    return ext.lower() == ".md"


def isTextFile(fileName):
    fileName, ext = os.path.splitext(fileName)
    return ext.lower() == ".md" or ext.lower() == ".txt" or ext.lower() == ".tex"


def isLatest(fileName):
    return "_latest." in fileName.lower()


def isLearningOutcomeEvidenceDirectory(path):
    return regex.search(r'LO\d+\.\d+\.\d+\.\d+', path, regex.IGNORECASE | regex.UNICODE) is not None


def sha256sum(filename):
    h = hashlib.sha256()
    b = bytearray(128*1024)
    mv = memoryview(b)
    with open(filename, 'rb', buffering=0) as f:
        for n in iter(lambda: f.readinto(mv), 0):
            h.update(mv[:n])
    return h.hexdigest()
