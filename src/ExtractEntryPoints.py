
import sys
import os
import SubmissionConfiguration
import Logger
import PrettyOutput
import UnzipStudentPortfolio
from Utilities import isStudentNumberValid, fixModuleListIfMissing
from PortfolioExtractCommand import PortfolioExtractCommand

def setupForExtractionGivenPathToZipFile(programArguments,configuration):
    # user has given --zip-file which is normally named something like
    # 123456789.zip where the filename is the student number.
    zipPath = programArguments.zipPath
    Logger.info("[EXTRACTENTRY] Zip file provided by user {}".format(zipPath))
    
    if not os.path.isfile(zipPath):
        raise RuntimeError("[EXTRACTENTRY] User provided zip file does not exist: {}".format(zipPath))
    
    zipFileName = os.path.basename(zipPath)
    zipFilePrefix, zipExt= os.path.splitext(zipFileName)

    if not zipExt == ".zip":
        PrettyOutput.prettyFixException("[EXTRACTENTRY] Zip file {} does not have a zip extension.".format(zipFileName))

    if not programArguments.studentNumber:
        programArguments.studentNumber = zipFilePrefix
        Logger.info("[EXTRACTENTRY] Student Number parsed from given zip file: {}".format(programArguments.studentNumber))

    if not isStudentNumberValid(programArguments.studentNumber):
        PrettyOutput.prettyFixException("Student number taken from zip filename is not valid {}".format(programArguments.studentNumber))    

    Logger.info("[EXTRACTENTRY] Student Number: {}".format(programArguments.studentNumber))
    
    configuration = SubmissionConfiguration.createSubmissionConfigurationFromStudentNumber(studentNumber=programArguments.studentNumber)
    configuration.zipFileName = programArguments.zipPath
    Logger.info("ProgramArguments.configFile = {}".format(programArguments.configFile))
    Logger.info("ProgramArguments.portfolioPath = {}".format(programArguments.portfolioPath))

    # if no target directory is given we assume we want to unzip the contents
    # to a directory called after the student number
    if not programArguments.portfolioPath:
        programArguments.portfolioPath = "."
    programArguments.portfolioPath = UnzipStudentPortfolio.unzip_file(configuration.zipFileName, programArguments.portfolioPath,  programArguments.studentNumber)
    Logger.info("[After Unzipping] ProgramArguments.portfolioPath = {}".format(programArguments.portfolioPath))

    # rebuild configuration path now zip file has completed extracting
    if not os.path.isfile(programArguments.configFile):
        programArguments.configFile = os.path.join(programArguments.portfolioPath, programArguments.configFile)
        if not os.path.isfile(programArguments.configFile):
            PrettyOutput.prettyFixException("Unable to find the configuration file {}, something unexpected has happened with the directories we expected to find as we unzipped the student portfolios.  Please check and fix.")

    Logger.info("[EXTRACTENTRY] [ARGM] Configuration File path: {}".format(programArguments.configFile))
    Logger.info("[EXTRACTENTRY] [ARGM] Student Number         : {}".format(programArguments.studentNumber))
    Logger.info("[EXTRACTENTRY] [ARGM] Portfolio path         : {}".format(programArguments.portfolioPath))
    Logger.info("[EXTRACTENTRY] [ARGM] Extract path           : {}".format(programArguments.extractPath))
    configuration = SubmissionConfiguration.createSubmissionConfigurationFromFile(configFilePath=programArguments.configFile, studentNumber=programArguments.studentNumber, absolutePathToBuildDirectory=programArguments.portfolioPath, extractDirectory=programArguments.extractPath)
    Logger.info("[EXTRACTENTRY] [CONF] Configuration File path: {}".format(configuration.configFilePath))
    Logger.info("[EXTRACTENTRY] [CONF] Student Number         : {}".format(configuration.studentNumber))
    Logger.info("[EXTRACTENTRY] [CONF] Build Directory        : {}".format(configuration.buildDirectory))
    Logger.info("[EXTRACTENTRY] [CONF] Extract directory      : {}".format(configuration.extractDirectory))
    return configuration

def setupForExtractionPathToPortfolioGiven(programArguments, configuration):
    # Marker has specified an unzipped portfolio directory
    # this should have as its last directory the student number
    if not programArguments.studentNumber:
        if not programArguments.portfolioPath:
            PrettyOutput.prettyRuntimeException("Either --zip-file or --portfolio-path must be set")
        programArguments.studentNumber = os.path.basename(programArguments.portfolioPath)
        if not isStudentNumberValid(programArguments.studentNumber):
            PrettyOutput.prettyFixException("Student number {} from portfolioPath {} is not valid".format(programArguments.studentNumber, programArguments.portfolioPath))
        Logger.info("Student Number from portfolio path: {}".format(programArguments.studentNumber))

    if not isStudentNumberValid(programArguments.studentNumber):
        PrettyOutput.prettyFixException("Student number is not valid {}".format(programArguments.studentNumber))

    Logger.info("Portfolio Directory: {}".format(programArguments.portfolioPath))
    Logger.info("Student Number: {}".format(programArguments.studentNumber))

    programArguments.configFile = os.path.join(programArguments.portfolioPath, programArguments.configFile)
    Logger.info("Using configuration file: {}".format(programArguments.configFile))
    configuration = SubmissionConfiguration.createSubmissionConfigurationFromFile(configFilePath=programArguments.configFile, studentNumber=programArguments.studentNumber, absolutePathToBuildDirectory=programArguments.portfolioPath, extractDirectory=programArguments.extractPath)
    return configuration

def setupForExtraction(programArguments, configuration):
    """
    Ways an extract can happen:
    1) User passed the path to a zip file to us
    2) User passed the unzipped portfolio path to us
    3) User is a student and has the portfolio in the current directory already and knows nothing of zip files.
    4) We are calling ourselves from the build command
    5) We are calling outselves from the extractclass command
    """
    if programArguments.zipPath:
        return setupForExtractionGivenPathToZipFile(programArguments, configuration)
    elif programArguments.portfolioPath:
        return setupForExtractionPathToPortfolioGiven(programArguments, configuration)
    else:
        Logger.info("Assuming user is a student checking their portfolio")
        configuration = SubmissionConfiguration.createSubmissionConfigurationFromFile(programArguments.configFile)
        return configuration

def runExtractPerStudent(programArguments):
    # we store this in the portfolio for a build action
    # we store this in the extracted for an extracted action
    configuration = None

    Logger.basicConfig(name=programArguments.action,
                    level=Logger.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    filename=sys.stdout,
                    filemode='a')
    
    if programArguments.action == "extract":
        configuration = setupForExtraction(programArguments, configuration)        
    else:
        configuration = SubmissionConfiguration.createSubmissionConfigurationFromFile(programArguments.configFile)
    
    configuration.loggerFilePath = programArguments.logFilePath
    configuration.action = programArguments.action
    configuration.markPortfolioWorthyOnly = programArguments.markPortfolioWorthyOnly
    configuration.ignoreErrors = programArguments.ignoreErrors
    configuration.compressMediaFiles = programArguments.compressMediaFiles
    configuration.portfolioPath = programArguments.portfolioPath
    configuration.doTranscriptions = not programArguments.skipTranscription
    
    if configuration.action.lower() == "extract":
        # at this point the extract directory does not exist so we have to create the log outside of it.
        parentOfExtractDirectory = os.path.dirname(configuration.extractDirectory)
        Logger.get().filePath = os.path.join(parentOfExtractDirectory, programArguments.logFilePath)
    else:
        raise RuntimeError("Should not be here, action should be extract! Contact delivery staff.")
    Logger.info("Logging path is: {}".format( Logger.get().filePath))

    if configuration:
        fixModuleListIfMissing(programArguments, configuration)
        
    
    programArguments.printToLog()
    configuration.relevantModuleCollection.log()
    configuration.relevantLearningOutcomes.log()

    handlers = []
    handlers.append(PortfolioExtractCommand(configuration))

    try:
        for handler in handlers:
            if handler.match(configuration.action):
                handler.run()
    except RuntimeError as err:
        print(err)
    except ValueError as err:
        print(err)
    finally:
        Logger.pop()