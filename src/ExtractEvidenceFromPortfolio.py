"""
For each record in the mapping.csv we want to extract the files that relate to that particular commit.
We then also get the lifetime of the file after that initial tagging and add that to the same extraction directory.
We then find the very latest version of the file and add that to the directory.
This is a belt and braces approach to try and make sure we get everything the student intended us to find.
"""

from ast import Mod
import Logger
import os
import pydriller
import PrettyOutput
from DirectoryUtils import extractCopyOfFile, findFileInRepository
import pytz
from Utilities import bytes_as_string

def isFileAlreadyInTrackingList(trackingList, file, loCode):
    for t in trackingList:
        if t["file"] == file and t["loCode"] == loCode:
            return True
    return False


def extractEvidenceFromStudentPortfolio(config):
    # this is a bit naughty we look directly into the mappingRecords collection at the array
    mappingRecords = config.mappingRecords.mappingRecords
    extractDirectory = config.extractDirectory

    Logger.info("Extracting evidence from repositories that matches the mapping.csv")

    for mr in mappingRecords:
        loPath = os.path.join(extractDirectory, mr.learningOutcomeCode)
        if not os.path.isdir(loPath):
            os.mkdir(loPath)
            if not os.path.isdir(loPath):
                PrettyOutput.prettyRuntimeException("Unable to create path {}".format(loPath))
            Logger.info("Created directory {}".format(loPath))

        Logger.info("Checking mapping record repository: {}".format(mr.repository))
        Logger.info("Associated repository directory: {}".format(mr.repositoryDirectory))
        if not mr.repositoryDirectory:
            PrettyOutput.prettyWarning("Repository directory not found: {} for {}".format(mr.repositoryDirectory, mr.repository))
            continue

        assert(mr.repository is not None)
        assert(mr.repositoryDirectory is not None)
        
        # extract files pointed to by the hash marked so we don't view the current version
        # but the version the student submitted.
        commitDirectory = os.path.join(loPath, mr.digest)
        if not os.path.isdir(commitDirectory):
            os.mkdir(commitDirectory)
        
        Logger.info("Saving files related to commit {} to: {}".format(mr.digest, commitDirectory))

        try:
            # find the commit in the repository
            # if it is not there then an exception is thrown
            pydriller.Repository(mr.repositoryDirectory, single=mr.digest)


            # We extract each commit but we also want to see the evolution of the file
            # up to and including the latest version.
            filesToTrack = []

            # First we are going to extract all the files we see that are relevant.
            # We will then go commit by commit looking for the evolution of those files and
            # then putting those modifications with the latest version.
            for commit in pydriller.Repository(mr.repositoryDirectory, single=mr.digest).traverse_commits():
                Logger.info("Extracting commit {} by {}".format(commit.hash, commit.author.name))
                committed_date_utc = commit.committer_date.astimezone(pytz.timezone('UTC'))
                committed_date_str = committed_date_utc.strftime("%Y%m%dT%H%M%S_UTC")
                Logger.info("Committed on: {} (UTC: {})".format(commit.committer_date, committed_date_utc))
                Logger.info("Associated commit message was: {}".format(commit.msg))

                for file in commit.modified_files:

                    Logger.info("File modification: {}".format(file.change_type))
                    #print("{} {}".format(commit.hash, file.change_type))
                    when = "TAGGED"
                    fileName = None
                    if file.new_path is not None:
                        fileName = os.path.basename(file.new_path)
                    else:
                        # this will happen when we have an add
                        fileName = os.path.basename(file.old_path)
                    name, ext = os.path.splitext(fileName)
                    if name[0] == ".":
                        name = name[1:]
                    type = str(file.change_type).replace("ModificationType.", "")
                    saveAsFilePath = os.path.join(commitDirectory, "{}_{}_{}_{}_{}{}".format(name,committed_date_str,commit.hash[0:7],type,when,ext))
                    
                    if file.change_type == pydriller.ModificationType.ADD:
                        Logger.info("File committed (new_path): {}".format(file.new_path))
                        if not isFileAlreadyInTrackingList(filesToTrack, file.new_path, mr.learningOutcomeCode):
                            with open(saveAsFilePath, "w", encoding="utf8") as outFile:
                                outFile.write(file.source_code)
                                Logger.info("Saved file to {} ({})".format(saveAsFilePath, bytes_as_string(os.path.getsize(saveAsFilePath))))
                                filesToTrack.append({
                                    "loCode": mr.learningOutcomeCode,
                                    "author_name": commit.author.name,
                                    "hash": commit.hash,
                                    "file": file.new_path,
                                    "extracted_to": saveAsFilePath,
                                    "repositoryDirectory": mr.repositoryDirectory
                                })
                                config.mapExtractedFilesToOriginalFiles[saveAsFilePath] = os.path.join(mr.repositoryDirectory, file.new_path)
                    elif file.change_type == pydriller.ModificationType.COPY:
                        # ignore a copy as we will pick this up in the transition
                        # Logger.info("File committed (old_path): {}".format(file.old_path))
                        # Logger.info("File committed (new_path): {}".format(file.new_path))
                        pass
                    elif file.change_type == pydriller.ModificationType.DELETE:
                        # ignore a delete as we will pick this up in the transition
                        # Logger.info("File committed (old_path): {}".format(file.old_path))
                        # new_path is None
                        pass
                    elif file.change_type == pydriller.ModificationType.MODIFY:
                        if not isFileAlreadyInTrackingList(filesToTrack, file.old_path, mr.learningOutcomeCode):
                            Logger.info("File committed (old_path): {}".format(file.old_path))
                            with open(saveAsFilePath, "w", encoding="utf8") as outFile:
                                outFile.write(file.source_code)
                                Logger.info("Saved file to {} ({})".format(saveAsFilePath, bytes_as_string(os.path.getsize(saveAsFilePath))))
                                filesToTrack.append({
                                    "loCode": mr.learningOutcomeCode,
                                    "author_name": commit.author.name,
                                    "hash": commit.hash,
                                    "file": file.old_path,
                                    "extracted_to": saveAsFilePath,
                                    "repositoryDirectory": mr.repositoryDirectory
                                })
                                config.mapExtractedFilesToOriginalFiles[saveAsFilePath] = os.path.join(mr.repositoryDirectory, file.new_path)

                    elif file.change_type == pydriller.ModificationType.RENAME:
                        # In the case of a rename there is no source_code or source_code_before.
                        # We there need to get the values of this before this hash.
                        Logger.info("File renamed (new_path): {}".format(file.old_path))
                        if not isFileAlreadyInTrackingList(filesToTrack, file.old_path, mr.learningOutcomeCode):
                            # we want to track the old file and the lifetime will capture the new
                            filesToTrack.append({
                                "loCode": mr.learningOutcomeCode,
                                "author_name": commit.author.name,
                                "hash": commit.hash,
                                "file": file.old_path,
                                "extracted_to": saveAsFilePath,
                                "repositoryDirectory": mr.repositoryDirectory
                            })
                            config.mapExtractedFilesToOriginalFiles[saveAsFilePath] = os.path.join(mr.repositoryDirectory, file.old_path)

                    elif file.change_type == pydriller.ModificationType.UNKNOWN:
                        # ignore unknown, detect during lifetime scan
                        # Logger.info("File committed (old_path): {}".format(file.old_path))
                        # Logger.info("File committed (new_path): {}".format(file.new_path))
                        # PrettyOutput.prettyWarning("Unknown git modification in repo {} at digest {}. Please check out what happened and see if it is important.".format(mr.repositoryDirectory, mr.digest))
                        pass

            # print(filesToTrack)

            for fileToTrack in filesToTrack:
                repo = pydriller.Repository(fileToTrack["repositoryDirectory"])
                seenOriginalHash = False
                for commit in repo.traverse_commits():
                    if commit.hash == fileToTrack["hash"]:
                        seenOriginalHash = True
                        continue
                    if commit.author.name == fileToTrack["author_name"]:
                        for file in commit.modified_files:
                            # short circuit if this is not the file we are tracking
                            if file.new_path != fileToTrack["file"] and file.old_path != fileToTrack["file"]:
                                continue

                            committed_date_utc = commit.committer_date.astimezone(pytz.timezone('UTC'))
                            committed_date_str = committed_date_utc.strftime("%Y%m%dT%H%M%S_UTC")
                            type = str(file.change_type).replace("ModificationType.", "")
                            if file.old_path is not None:
                                fileName = os.path.basename(file.old_path)          
                            else:
                                # this will happen when we have an add
                                fileName = os.path.basename(file.new_path)
                            name, ext = os.path.splitext(fileName)
                            if name[0] == ".":      # make sure its not hidden from marker
                                name = name[1:]

                            when = "BEFORE"
                            if seenOriginalHash:
                                when = "AFTER"
                            saveAsFilePath = os.path.join(commitDirectory, "{}_{}_{}_{}_{}{}".format(name,committed_date_str,commit.hash[0:7],type, when, ext))
                            
                            if file.change_type == pydriller.ModificationType.ADD:
                                if file.new_path == fileToTrack["file"]:
                                    # we can have this is the student marked up a rename
                                    with open(saveAsFilePath, "w", encoding="utf8") as outFile:
                                        outFile.write(file.source_code)

                            elif file.change_type == pydriller.ModificationType.COPY:
                                if file.old_path == fileToTrack["file"]:
                                    with open(saveAsFilePath, "w", encoding="utf8") as outFile:
                                        outFile.write("File was copied from {} to {} on the {} by {}.".format(file.old_path, file.new_path, committed_date_str, commit.author.name))

                            elif file.change_type == pydriller.ModificationType.DELETE:
                                if file.old_path == fileToTrack["file"]:
                                    with open(saveAsFilePath, "w", encoding="utf8") as outFile:
                                        outFile.write("File was deleted {} by {}.".format(committed_date_str, commit.author.name))

                            elif file.change_type == pydriller.ModificationType.MODIFY:
                                if file.old_path == fileToTrack["file"]:
                                    #print("SOURCE CODE: {}\n\n{}".format(file.source_code, file.source_code_before))
                                    if file.source_code is not None:
                                        with open(saveAsFilePath, "w", encoding="utf8") as outFile:
                                            outFile.write(file.source_code)

                            elif file.change_type == pydriller.ModificationType.RENAME:
                                if file.old_path == fileToTrack["file"]:
                                    with open(saveAsFilePath, "w", encoding="utf8") as outFile:
                                        outFile.write("File was renamed to {} on the {} by {}.".format(file.new_path, committed_date_str, commit.author.name))
                                    fileToTrack["file"] = file.new_path

                            elif file.change_type == pydriller.ModificationType.UNKNOWN:
                                if file.new_path == fileToTrack["file"] or file.old_path == fileToTrack["file"]:
                                    PrettyOutput.prettyWarning("Unknown git modification in repo {} at digest {}. Please check out what happened and see if it is important.".format(mr.repositoryDirectory, mr.digest))


            # once we have the lifetime, if the file still exists
            # then we hunt it out, this is our fallback position
            for trackedFile in filesToTrack:
                fileName = os.path.basename(trackedFile["file"])
                name, ext = os.path.splitext(fileName)
                if name[0] == '.':
                    name = name[1:]

                copyOfCurrentVersionPath = os.path.join(commitDirectory, name + "_latest"+ext)
                currentVersionOfFile = os.path.join(trackedFile["repositoryDirectory"], trackedFile["file"])
                Logger.info("Checking for latest version of {}".format(currentVersionOfFile))
                if os.path.isfile(currentVersionOfFile):
                    Logger.info("Copying latest version to {}".format(copyOfCurrentVersionPath))
                    extractCopyOfFile(config, currentVersionOfFile, copyOfCurrentVersionPath)
                else:
                    Logger.info("Searching for {}".format(currentVersionOfFile))
                    foundFile = findFileInRepository(currentVersionOfFile)
                    if foundFile is None:
                        PrettyOutput.prettyWarning("[CONFIRM] Current version of file {} was not found, probably moved or deleted.".format(currentVersionOfFile))
                    else:
                        Logger.info("Found file in new location from when original marked, copying latest version to {}".format(copyOfCurrentVersionPath))
                        extractCopyOfFile(config, foundFile, copyOfCurrentVersionPath)
                        config.redirectedFiles[currentVersionOfFile] = foundFile


        except Exception as e: 
            print(e)
            PrettyOutput.prettyFixException("Something went wrong with the extraction. Maybe an invalid SHA code found {} in repository {}, downloaded to {}, you may wish to fix or remove this from the mapping file.".format(mr.digest, mr.repository, mr.repositoryDirectory))
        
