from DirectoryUtils import isLearningOutcomeEvidenceDirectory, isLatest, isTextFile
from PortfolioTreeVisitor import PortfolioTreeVisitor
import regex
import Logger
import PrettyOutput
import os

class CitationCounterPlugin(PortfolioTreeVisitor):
    def __init__(self, config):
        super().__init__(config)
        self.citationCount = 0

    def matchFile(self, path, ext):
        dirOnly = os.path.dirname(path)
        return isLearningOutcomeEvidenceDirectory(dirOnly) and isTextFile(path) and isLatest(path)

    def _countCitationsInText(self, content):
        yearCitations = regex.findall("\(\d\d\d\d\w{0,2}\)", content, regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
        singleNameAndYearCitations = regex.findall("\([\p{L}\.,& ]+\d\d\d\d\w{0,2}\)", content, regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
        doubleNameAndYearCitations = regex.findall("\([\p{L}\.,& ]+\d\d\d\d\w{0,2};[\p{L}\.& ]+\d\d\d\d\w{0,2}\)", content, regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
        tripleNameAndYearCitations = regex.findall("\([\p{L}\.,& ]+\d\d\d\d\w{0,2};[\p{L}\.,& ]+\d\d\d\d\w{0,2};[\p{L}\.,& ]+\d\d\d\d\w{0,2}\)", content, regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
        citationCount = len(yearCitations) + len(singleNameAndYearCitations) + len(doubleNameAndYearCitations) + len(tripleNameAndYearCitations)
        return citationCount

    def _countCitations(self, path):
        with open(path, "r", encoding="utf8") as inFile:
            content = inFile.read()
            return self._countCitationsInText(content)
    
    def start(self):
        self.citationCount = 0
        return True

    def run(self, path):
        thisCount = self._countCitations(path)
        Logger.info("{} citations potentially found in {}".format(thisCount, path))
        self.citationCount += thisCount
        return True

    def finish(self):
        if self.citationCount == 0:
            PrettyOutput.prettyWarning("No citations found in any markdown or text file that has been marked with a learning outcome.")