import os
from PortfolioTreeVisitor import PortfolioTreeVisitor
import Logger
import Repositories
from DirectoryUtils import isGitRoot
import PrettyOutput

class CommitLogCapturePlugin(PortfolioTreeVisitor):
    degreeSubmissionRoot = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

    def __init__(self, config, targetPath = None):
        super().__init__(config)
        self.commitCount = 0
        self.targetDirectory = targetPath 
        if self.config and not targetPath:
            self.targetDirectory = os.path.join(self.config.extractDirectory, "commits")
        if self.targetDirectory and not os.path.isdir(self.targetDirectory):
            os.mkdir(self.targetDirectory)
            Logger.info("Created directory {}".format(self.targetDirectory))

    def matchDirectory(self, path):
        return isGitRoot(path) and not path.endswith(CommitLogCapturePlugin.degreeSubmissionRoot)

    def _dumpCommitLogs(self, gitDirectory, targetLogFilePath):
        Logger.info("Dumping commits from {} to {}".format(gitDirectory, targetLogFilePath))
        commits = Repositories.read_in_commits(gitDirectory)
        Logger.info("Found {} commits in {}".format(len(commits), gitDirectory))
        Repositories.write_commits(commits, targetLogFilePath)
        return len(commits)
    
    def start(self):
        self.commitCount = 0
        return True

    def run(self, path):
        assert(os.path.isdir(path))
        #print(path)
        dumpTextFilePath = os.path.join(self.targetDirectory, os.path.basename(path)+".log")
        thisCommitCount = self._dumpCommitLogs(path, dumpTextFilePath)
        self.commitCount += thisCommitCount
        if thisCommitCount > 0 and not os.path.isfile(dumpTextFilePath):
            PrettyOutput.prettyWarning("Expected the file {} to exist, but it does not.".format(dumpTextFilePath))
        return True

    def finish(self):
        if self.commitCount == 0:
            PrettyOutput.prettyWarning("No commits were found, are you sure this is a complete portfolio?")