from PortfolioTreeVisitor import PortfolioTreeVisitor
import PrettyOutput
import Logger
import os
import re
from DirectoryUtils import extractCopyOfFile

class CoversheetExtractionPlugin(PortfolioTreeVisitor):

    def __init__(self, config, targetDirectory = None):
        super().__init__(config)
        self.coversheetCount = 0
        self.coversheets = []
        self.targetDirectory = targetDirectory
    
    def matchFile(self, path, ext):
        fileName = os.path.basename(path)
        pattern = "^ui\d\d\d\d\d\d_coversheet_signed\.docx$"
        matched = re.search(pattern, fileName, re.IGNORECASE)
        return matched is not None and ext == ".docx"

    def start(self):
        self.coversheetCount = 0
        self.coversheets = []
        return True

    def run(self, path):
        Logger.info("Copying coversheet: {}".format(path))
        fileName = os.path.basename(path)
        extractedCopyPath = os.path.join(self.targetDirectory,fileName)
        extractCopyOfFile(self.config, path, extractedCopyPath)
        self.coversheetCount += 1
        self.coversheets.append(path)
        return True

    def finish(self):
        if self.coversheetCount == 0:
            PrettyOutput.prettyWarning("There are no coversheets found in portfolio. Check with delivery staff as these should have been generated during the portfolio build process.")