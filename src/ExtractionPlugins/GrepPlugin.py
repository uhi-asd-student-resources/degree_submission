
from PortfolioTreeVisitor import PortfolioTreeVisitor
import os
from DirectoryUtils import extractCopyOfFile, isTextFile
import Logger
import re
import json

class GrepPlugin(PortfolioTreeVisitor):

    def __init__(self, config, sourceDirectory=None):
        super().__init__(config)
        self.sourceDirectory = sourceDirectory
        self.matchCount = 0
        if self.sourceDirectory is None:
            self.sourceDirectory = self.config.buildDirectory
        thisFilePath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        grepFilePath = os.path.join(thisFilePath, "data", "grep.json")
        assert(os.path.isfile(grepFilePath))
        self.grepTerms = []
        with open(grepFilePath, "r", encoding="utf8") as inFile:
            self.grepTerms = json.load(inFile)
        self.grepDirectory = os.path.join(self.config.extractDirectory, "grep")
        if not os.path.isdir(self.grepDirectory):
            Logger.info("Creating grep directory: {}".format(self.grepDirectory))
            os.mkdir(self.grepDirectory)

    def matchFile(self, path, ext):
        fileName = os.path.basename(path)
        ignoreFiles = [ ".gitignore", ".gitkeep" ]
        if fileName.lower() in ignoreFiles:
            return False
        return True

    def _createTargetDirectoryInExtractedArea(self, fullTargetPath):
        if not os.path.isdir(fullTargetPath):
            Logger.info("Creating path {}".format(fullTargetPath))
            os.mkdir(fullTargetPath)


    def _grepForTerm(self, term, sourceFilePath):
        fullTargetPath = os.path.join(self.grepDirectory, term["directory"])
        self._createTargetDirectoryInExtractedArea(fullTargetPath)

        fileNameIncExt = os.path.basename(sourceFilePath)
        fileNameNoExt, ext = os.path.splitext(fileNameIncExt)
        successfulPattern = None
        
        # check the filename and paths for the patterns
        foundInFile = False
        for p in term["pattern"]:
            relativePath = sourceFilePath.replace(self.config.buildDirectory,"")
            # remove the first directory
            if relativePath[0] == '/':
                relativePath = relativePath[1:]
            pos = relativePath.find('/')
            if pos >= 0:
                relativePath = relativePath[(pos+1):]
            if re.search(p, relativePath, re.IGNORECASE):
                successfulPattern = p
                foundInFile = True
                break

        # check if the pattern is in the contents of the file
        if not foundInFile and isTextFile(ext):
            with open(sourceFilePath, "r", encoding="utf8") as inFile:
                for line in inFile:
                    for p in term["pattern"]:
                        if re.search(p, line, re.IGNORECASE):
                            successfulPattern = p
                            foundInFile = True
                            break
                    if foundInFile:
                        break

        if foundInFile:
            self.matchCount += 1
            Logger.info("Found {} [{}] in {}".format(term["directory"], successfulPattern, sourceFilePath))
            targetPath = os.path.join(fullTargetPath, fileNameIncExt)
            counter = 0
            while os.path.isfile(targetPath):
                counter += 1
                targetPath = os.path.join(fullTargetPath, "{}_{}{}".format(fileNameNoExt, str(counter), ext))
            if counter > 0:
                targetPath = os.path.join(fullTargetPath, "{}_{}{}".format(fileNameNoExt, str(counter), ext))
            extractCopyOfFile(self.config, sourceFilePath, targetPath)

    def _grepForTerms(self, path):
        for g in self.grepTerms:
            if self.config.relevantLearningOutcomes:
                if "lo" in g:
                    if not self.config.relevantLearningOutcomes.contains(g["lo"]):
                        continue
            self._grepForTerm(g, path)

    def start(self):
        self.matchCount = 0
        return True

    def run(self, path):
        self._grepForTerms(path)
        return True

    def finish(self):
        pass
