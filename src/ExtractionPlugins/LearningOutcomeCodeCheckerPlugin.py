"""
Look for 
(a) at least one learning outcome per markdown document.
(b) if 2 learning outcomes in different topics e.g. Agile and Technical quotes, warn about relevance.
"""
from DirectoryUtils import isDotGitDirectory, isTextFile, isLatest, isLearningOutcomeEvidenceDirectory
import os
import PrettyOutput
import regex
import Logger
import LearningOutcomesCollection
from PortfolioTreeVisitor import PortfolioTreeVisitor

class LearningOutcomeCodeCheckerPlugin(PortfolioTreeVisitor):
  def __init__(self, config):
    super().__init__(config)
    self.learningOutcomeList = LearningOutcomesCollection.createDefaultLearningOutcomesCollection()
    self.warnings = []

  def matchFile(self, path, ext):
    dirOnly = os.path.dirname(path)
    return isTextFile(path) and isLatest(path) and isLearningOutcomeEvidenceDirectory(dirOnly)

  def start(self):
    self.warnings = []
    return True

  def run(self, path):
    self._checkLearningOutcomeCodes(path)
    return True

  def finish(self):
    pass

  def _checkLearningOutcomeCodes(self, file):
    with open(file, "r",encoding="utf-8") as inFile:
      contents = inFile.read()
      loText = regex.findall("LO[\.\s]*\d+\.\d+\.\d+\.\d+", contents, regex.MULTILINE | regex.UNICODE | regex.IGNORECASE)
      loText = list(set(loText))
      if len(loText) == 0:
        warning = "File '{}' has been marked with a learning outcome in a commit but has no learning outcome in the contents e.g. (LO1.1.1.1)".format(file)
        self.warnings.append(warning)
        if self.config is not None:
          PrettyOutput.prettyWarning(warning)
      topic = []
      for lo in loText:
        topic.append(self.learningOutcomeList.getTopicFor(lo))
      uniqueTopicList = list(set(topic))
      topicLength = len(uniqueTopicList)
      if topicLength > 1:
        warning = "{} contains learning outcomes in the text that cover more than one topic {}.  Topics include Agile, Business, Metaskills, Security, Technical. It is highly unlikely that work can be relevant to more than 1 of these.  You might want to review the learning outcomes and remove the ones that are not relevant.".format(file, uniqueTopicList)
        self.warnings.append(warning)
        if self.config is not None:
          PrettyOutput.prettyWarning(warning)

  
