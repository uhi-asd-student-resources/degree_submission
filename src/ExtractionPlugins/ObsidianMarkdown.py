"""
Standalone functions we can test for identifying Obsidian markdown
"""
import regex
import PrettyOutput
from DirectoryUtils import findFileInRepository
import os

REGEX_EMBEDDED_IMAGE = "!\[\[(?:file:///)?([\s\w\.\\\/:%_\+\.\~\?\&\=\-\#\^]+?\.png)\]\]"
REGEX_INTERNAL_IMAGE = "(?:^|[^!])\[\[(?:file:///)?([\s\w\.\\\/:%_\+\.\~\?\&\=\-\#\^]+?\.png)\]\]"
REGEX_MARKDOWN_LINKS = "(?:^|[^!])\[\[(?:file:///)?([\s\w\.\\\/:%_\+\.\~\?\&\=\-\^]+?)\]\]"
REGEX_EMBEDDED_BLOCK = "!\[\[(?:file:///)?([\s\w\.\\\/:%_\+\.\~\?\&\=\-]+\#\^\w+)\]\]"
REGEX_SECTION_BLOCK = "!\[\[(?:file:///)?([\s\w\.\\\/:%_\+\.\~\?\&\=\-]+\#[\w\s\w\.\\\/:%_\+\.\~\?\&\=\-]+)\]\]"
REGEX_LINKED_BLOCK = "(?:^|[^!])\[\[(?:file:///)?([\s\w\.\\\/:%_\+\.\~\?\&\=\-]+\#\^\w+)\]\]"

# Some of the files are obsidian like but follow the GitHub markdown style.
# We would also like to incorporate these into our HTML versions.
REGEX_GITHUB_LINK_GENERIC = "!\[.*?\]\(.*?\.png\)"
REGEX_GITHUB_LINK_LOCATION = "!\[(.*?)\]\((?:file:///)?([\s\w\.\\\/:%_\+\.\~\?\&\=\-\#\^]+?\.png)\)"

REGEX_GITHUB_DIRECT_IMAGE_LINK_GENERIC = "\[.*?\]\(.*?\.png\)"
REGEX_GITHUB_DIRECT_IMAGE_LINK_LOCATION = "\[(.*?)\]\((?:file:///)?([\s\w\.\\\/:%_\+\.\~\?\&\=\-\#\^]+?\.png)\)"


def obsidianIsBlock(link):
    """
    Is the string an obsidian block
    https://help.obsidian.md/How+to/Link+to+blocks
    """
    return "#^" in link


def extractLinkFromSyntax(link):
    link = link.replace("[[", "")
    link = link.replace("]]", "")
    link = link.strip()
    return link


def obsidianGetBlockFromLink(link):
    """
    Get the block from the link e.g. myfile#^4e5d2a
    https://help.obsidian.md/How+to/Link+to+blocks
    """
    link = extractLinkFromSyntax(link)
    if "#^" not in link:
        return None
    _, block = link.split("#^")
    return block


def obsidianIsUrl(link):
    """
    Is the link just a normal url.
    Here we use a crude regex just looking for http/https at the start.
    """
    link = extractLinkFromSyntax(link)
    return regex.match("^https?://", link)


def obsidianGetFileFromLink(link):
    """
    Get the file that is in the link.  We are not interesting any blocks so 
    we chop at the hash/pound sign.  This might truncate html links but we are 
    assuming the link is a file not a url.
    """
    link = extractLinkFromSyntax(link)
    if "#" not in link:
        return link
    source, _ = link.split("#")
    return source


def obsidianGetSectionFromLink(link):
    """
    Get a section/heading reference from a link.  e.g. [[myfile.md#myFirstHeading]]
    https://help.obsidian.md/How+to/Internal+link
    """
    link = extractLinkFromSyntax(link)
    if "#^" in link:
        return None
    if "#" not in link:
        return None
    _, section = link.split("#")
    return section


def obsidianGetBlockText(linkPath, block):
    """
    Get the block pointed to by the block reference.
    linkPath is the path of the file the block is in
    block is the block reference e.g. ^4e3d1a
    """
    if not os.path.isfile(linkPath):
        newLinkPath = findFileInRepository(linkPath)
        if newLinkPath is None:
            PrettyOutput.prettyWarning(
                "obsidianGetBlockText :: Could not find {}".format(linkPath))
            return None
        else:
            linkPath = newLinkPath
    if block[0] != "^":
        block = "^"+block
    with open(linkPath, "r", encoding="utf-8") as inFile:
        contents = inFile.read()
        index = contents.find(block)
        if index < 0:
            # block reference was not found in this file
            return None
        if index == 0:
            # reference was at the start of the file so nothing to return
            return ""
        startOfBlock = 0
        startOfPreviousText = -1
        for lookback in range(index-1, 0, -1):
            if startOfPreviousText < 0:  # look back for for first non-whitespace character after which we look for \n\n
                if not contents[lookback].isspace():
                    startOfPreviousText = lookback+1
            else:
                if contents[lookback] == '\n' and contents[lookback-1] == '\n':
                    startOfBlock = lookback+1
                    break
        return contents[startOfBlock:startOfPreviousText]


def obsidianGetSectionText(linkPath, section):
    """
      Find the text, get the level of the section
      We continue to grab until we find a section marker which is the same or higher than our current section marker
      Spaces are handled as per any other character e.g. [[file#Section with space in]]
      linkPath: path to the file
      section: name of the heading to find
    """
    if section[0] == '#':
        section = section[1:]
    if not os.path.isfile(linkPath):
        newLinkPath = findFileInRepository(linkPath)
        if newLinkPath is None:
            PrettyOutput.prettyWarning(
                "obsidianGetBlock :: Could not find {}".format(linkPath))
            return None
        else:
            linkPath = newLinkPath
    with open(linkPath, "r", encoding="utf-8") as inFile:
        linesOfContent = inFile.readlines()
        requiredText = []
        hasHeading = False
        found = False
        indentSize = -1
        for l in linesOfContent:
            strippedString = l.lstrip()
            if len(strippedString) > 0 and strippedString[0] == '#':
                hasHeading = True
                if indentSize > 0:
                    firstSpacePos = strippedString.find(" ")
                    thisIndentSize = (firstSpacePos-1)
                    if thisIndentSize <= indentSize:
                        break
            else:
                hasHeading = False

            if not found and hasHeading and section in l:
                requiredText.append(l.rstrip("\n"))
                firstSpacePos = strippedString.find(" ")
                indentSize = (firstSpacePos-1)
                found = True
            else:
                if found:
                    requiredText.append(l.rstrip("\n"))
        sectionText = '\n'.join(requiredText)
        sectionText = sectionText.rstrip("\n")
        return sectionText
