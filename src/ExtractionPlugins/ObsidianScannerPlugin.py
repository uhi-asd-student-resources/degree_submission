"""
Obsidian file (.md) scanner to pull out linked files and put them
in the right place for us to mark
"""
import os
import PrettyOutput
import regex
from DirectoryUtils import findFileInRepository, isLearningOutcomeEvidenceDirectory, isMarkdown, isLatest
import Logger
from weasyprint import HTML
import markdown
import datetime
from ExtractionPlugins.ObsidianMarkdown import *
from PortfolioTreeVisitor import PortfolioTreeVisitor

HTML_MARKDOWN_TEMPLATE = os.path.join(os.path.dirname(
    os.path.dirname(__file__)), "data", "markdown_template.html")
assert(os.path.isfile(HTML_MARKDOWN_TEMPLATE))


class ObsidianScannerPlugin(PortfolioTreeVisitor):
    def __init__(self, config):
        super().__init__(config)
        self.html_markdown_template_code = None
        with open(HTML_MARKDOWN_TEMPLATE, "r") as inFile:
            contents = inFile.read()
            self.html_markdown_template_code = contents
        self.warnings = []

    def matchFile(self, path, ext):
        dirOnly = os.path.dirname(path)
        return isMarkdown(path) and isLatest(path) and isLearningOutcomeEvidenceDirectory(dirOnly)

    def start(self):
        self.warnings = []
        return True

    def run(self, path):
        Logger.info("[OBSIDIAN] Processing {}".format(path))
        filePathInPortfolio = self._findExtractedFileInPortfolio(path)
        if filePathInPortfolio is not None:
            self._transformToHtmlAndPdf(path, filePathInPortfolio)
        return True

    def finish(self):
        pass

    def _replaceTagsWithValues(self, html, valueDict):
        html = html.replace("{{title}}", valueDict["title"])
        html = html.replace("{{file_date}}", valueDict["file_date"])
        html = html.replace("{{extraction_path}}",
                            valueDict["extraction_path"])
        html = html.replace("{{original_path}}", valueDict["original_path"])
        html = html.replace("{{main}}", valueDict["main"])
        return html

    def _findExtractedFileInPortfolio(self, filePathInExtractedDirectory):
        filePathInBuildDirectory = self.config.getOriginalPath(
            filePathInExtractedDirectory)
        if not os.path.isfile(filePathInBuildDirectory):
            filePathInBuildDirectory = self.config.redirectedFiles[filePathInExtractedDirectory]
            if not os.path.isfile(filePathInBuildDirectory):
                PrettyOutput.prettyWarning("Failed to find the redirected file {} when finding extracted file ".format(
                    filePathInBuildDirectory, filePathInExtractedDirectory))
                return None
        return filePathInBuildDirectory

    def _transformToHtmlAndPdf(self, filePathInExtractedDirectory, filePathInBuildDirectory):
        """
        All links will be relative to the original location so that is why we are passed this.
        We then make every relative to the path of the new obsidian markdown file
        """
        assert(os.path.isfile(filePathInExtractedDirectory))
        assert(os.path.isfile(filePathInBuildDirectory))

        destinationDirectoryInExtractedDir = os.path.dirname(
            filePathInExtractedDirectory)
        sourceDirectoryInBuildDir = os.path.dirname(filePathInBuildDirectory)

        # create the filenames for the new html and pdf file based on the original filename
        # in the build directory.
        fileNameWithExt = os.path.basename(filePathInBuildDirectory)
        fileName, ext = os.path.splitext(fileNameWithExt)
        htmlPath = os.path.join(
            destinationDirectoryInExtractedDir, fileName + ".html")
        pdfPath = os.path.join(
            destinationDirectoryInExtractedDir, fileName + ".pdf")

        with open(filePathInBuildDirectory, "r", encoding="utf-8") as inFile:
            contents = inFile.read()

            # When generating the html, any images will still refer back into the build directory so
            # we need to know the relative path from the subdirectory within the extractedDirectory to
            # the original portfolio location in the buildDirectory.
            relativePathFromDestinationToSourceDirectory = os.path.relpath(
                sourceDirectoryInBuildDir, destinationDirectoryInExtractedDir)

            contents = self._resolveLinksRecursively(contents, relativePathFromDestinationToSourceDirectory,
                                                     sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)

            # markdown will allow us to pass through any transformed HTML
            htmlFragment = markdown.markdown(contents, output_format="html5")
            htmlMarkdownTemplate = self.html_markdown_template_code
            replacementKeyValuePairs = {
                "title": filePathInBuildDirectory,
                "file_date": datetime.datetime.now().strftime("%Y-%m-%d %H:%m"),
                "original_path": filePathInBuildDirectory,
                "extraction_path": filePathInExtractedDirectory,
                "main": htmlFragment
            }
            finalHtmlContents = self._replaceTagsWithValues(
                htmlMarkdownTemplate, replacementKeyValuePairs)

            with open(htmlPath, "w", encoding="utf-8", errors="xmlcharrefreplace") as outFile:
                outFile.write(finalHtmlContents)
            HTML(htmlPath).write_pdf(pdfPath)

    def _resolveLinksRecursively(self, contents, relativePath, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir):
        """
        We keep resolving the links until the contents has no more links in or we have gone to a depth of 10
        """
        isChanged = True
        count = 0
        while isChanged:

            previousContents = contents
            newContents = self._resolveLinksInText(
                contents, relativePath, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
            isChanged = False
            if previousContents != newContents:
                isChanged = True
                contents = newContents
            count += 1
            if count >= 10:
                PrettyOutput.prettyFixException("Looks like an infinite loop might be here: \n{}\n{}\n{}".format(
                    relativePath, sourceDirectoryInBuildDir, filePathInBuildDirectory))
        return contents

    def _resolveLinksInText(self, contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir):
        contents = self._resolveEmbeddedImages(contents, relativePathFromDestinationToOriginal,
                                               sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        contents = self._resolveInternalImages(contents, relativePathFromDestinationToOriginal,
                                               sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        contents = self._resolveInternalMarkdownLinks(
            contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        contents = self._resolveEmbeddedBlockLinks(
            contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        contents = self._resolveEmbeddedSectionLinks(
            contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        contents = self._resolveLinkedBlockLinks(
            contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        contents = self._resolveGitHubImageLinks(
            contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        contents = self._resolveGitHubDirectImageLinks(
            contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        return contents

    def _findImageInBuildDirectory(self, imageReference, sourceDirectoryInBuildDir):
        """
        imageReference is a raw link from Obsidian so may take many forms.  
        This function untangles it and finds the file in the students portfolio (buildDirectory)
        Returns a path relative to the student portfolio in buildDirectory or None if not found
        """

        # Obsidian allows you to reference without a directory any file within the obsidian directory hierarchy
        # so we need to be canny about how we try and retrieve the image.
        imagePathWhichIsRelativeToObsidianLibraryRoot = obsidianGetFileFromLink(
            imageReference)
        if os.path.isabs(imagePathWhichIsRelativeToObsidianLibraryRoot):
            # we don't allow absolute references as our directory won't be the same as the students
            # hopefully that is obvious.
            PrettyOutput.prettyWarning(
                "You have absolute paths (paths beginning with a drive letter or a /) in your markdown files.  Please correct to use relative paths.  This link cannot be followed.")

        # We need to search the student portfolio starting from where the original file was.
        # The resulting path will be relative to the student portfolio we are working with.
        # If the file is not found we will be returning None.
        foundFileRelativeToWorkingDirectory = os.path.join(
            sourceDirectoryInBuildDir, imagePathWhichIsRelativeToObsidianLibraryRoot)

        # remove the current directory bit which is not required
        # foundFileRelativeToWorkingDirectory = foundFileRelativeToWorkingDirectory.replace(
        #     "./", "")

        foundFileRelativeToWorkingDirectory = findFileInRepository(
            foundFileRelativeToWorkingDirectory)
        return foundFileRelativeToWorkingDirectory

    def _resolveEmbeddedImages(self, contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir):
        """
        See https://help.obsidian.md/How+to/Embed+files for more information on image embeddings.
        Here we only handle embeddings from LOCAL image files.
        """
        imageEmbeddings = regex.findall(REGEX_EMBEDDED_IMAGE,
                                        contents, regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
        for img in imageEmbeddings:
            if not obsidianIsUrl(img):
                Logger.info("Embedding {}".format(img))
                imageRelativeToWorkingDirectory = self._findImageInBuildDirectory(
                    img, sourceDirectoryInBuildDir)

                if imageRelativeToWorkingDirectory is None:
                    PrettyOutput.prettyWarning(
                        "Obsidian broken embedded image '{}' in '{}'".format(img, filePathInBuildDirectory))
                    contents = contents.replace("[[{}]]".format(img), "[{} (Broken link)]({})".format(
                        img, filePathInBuildDirectory))
                    continue

                # We get the relative path from the destination directory to where we are running the
                # extraction script, normally in the parent of the project root.
                # e.g. expected value is something along the lines of ../../../
                relativePathToDestinationPathFromWorkingDirectory = os.path.relpath(
                    os.path.dirname(imageRelativeToWorkingDirectory), destinationDirectoryInExtractedDir)
                imagePathRelativeFromDestinationDirectory = os.path.join(
                    relativePathToDestinationPathFromWorkingDirectory, img)

                # now we are going to create some new markdown, we do it this way in case there are other
                # standard transformation options embedded in the img link which we have lopped off when finding the file.
                imageStandardMarkdownText = "![{}]({})".format(
                    img, imagePathRelativeFromDestinationDirectory)
                # this will give us something like <img src="blah.png" alt="blah">
                htmlFragment = markdown.markdown(
                    imageStandardMarkdownText, output_format="html5")
                # this HTML transforms the markdown link into a clickable piece of HTML
                quoteText = "<div class='embed'><div class='quote'><a href='{}' target='_blank'>{}</a></div><p>Embedded image reference: {}</p></div>".format(
                    imagePathRelativeFromDestinationDirectory, htmlFragment, img)
                contents = contents.replace("![[{}]]".format(img), quoteText)
        return contents

    def _resolveInternalImages(self, contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir):
        """
        An internal image is a non-embedded link of the form [[pink.png]].
        """
        imageInternalLinks = regex.findall(
            REGEX_INTERNAL_IMAGE, contents, regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
        for img in imageInternalLinks:
            if not obsidianIsUrl(img):
                Logger.info("Sourcing local image {}".format(img))
                imageRelativeToWorkingDirectory = self._findImageInBuildDirectory(
                    img, sourceDirectoryInBuildDir)

                if imageRelativeToWorkingDirectory is None:
                    PrettyOutput.prettyWarning(
                        "Obsidian broken embedded image '{}' in '{}'".format(img, filePathInBuildDirectory))
                    contents = contents.replace("[[{}]]".format(img), "[{} (Broken link)]({})".format(
                        img, filePathInBuildDirectory))
                    continue

                # We get the relative path from the destination directory to where we are running the
                # extraction script, normally in the parent of the project root.
                # e.g. expected value is something along the lines of ../../../
                relativePathToDestinationPathFromWorkingDirectory = os.path.relpath(
                    os.path.dirname(imageRelativeToWorkingDirectory), destinationDirectoryInExtractedDir)
                imagePathRelativeFromDestinationDirectory = os.path.join(
                    relativePathToDestinationPathFromWorkingDirectory, img)

                quoteText = "![{}]({})".format(
                    img, imagePathRelativeFromDestinationDirectory)
                htmlFragment = markdown.markdown(
                    quoteText, output_format="html5")
                quoteText = "<div class='embed'><div class='quote'><a href='{}' target='_blank'>{}</a></div><p>Linked image reference: {}</p></div>".format(
                    imagePathRelativeFromDestinationDirectory, htmlFragment, img)
                contents = contents.replace("[[{}]]".format(img), quoteText)
        return contents

    def _resolveInternalMarkdownLinks(self, contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir):
        # Obsidian links between markdown files.
        internalNonBlocksLinks = regex.findall(
            REGEX_MARKDOWN_LINKS, contents, regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
        for link in internalNonBlocksLinks:
            if os.path.isabs(link):
                PrettyOutput.prettyWarning(
                    "You have absolute paths in your markdown files.  Please correct to use relative paths.")
            newLink = os.path.join(relativePathFromDestinationToOriginal, link)

            # links to other markdown files so we want to replace with <a> tag
            _, prefix = os.path.splitext(link)
            if len(prefix) == 0:
                newLink = newLink + ".md"
            # this will make the outer markdown translator convert this
            # to an ordinary html <a> tag.
            contents = contents.replace("[[{}]]".format(
                link), "[{}]({})".format(link, newLink))
        return contents

    def _resolveEmbeddedBlockLinks(self, contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir):
        # import blocks of text from other files into this file and put as a quote
        embeddedBlocksLinks = regex.findall(
            REGEX_EMBEDDED_BLOCK, contents, regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
        for link in embeddedBlocksLinks:
            block = obsidianGetBlockFromLink(link)
            linkPath = obsidianGetFileFromLink(link)
            if os.path.isabs(linkPath):
                PrettyOutput.prettyWarning(
                    "You have absolute paths in your markdown files.  Please correct to use relative paths.")
            newLink = os.path.join(sourceDirectoryInBuildDir, linkPath)
            _, prefix = os.path.splitext(linkPath)
            if len(prefix) == 0:
                newLink = newLink + ".md"
            quoteText = obsidianGetBlockText(newLink, block)
            if quoteText is None:
                quoteText = "<pre>WARNING: Unable to find and retrieve block: {}\nParsed as {} at {}.</pre>".format(
                    link, newLink, block)
            else:
                htmlFragment = markdown.markdown(
                    quoteText, output_format="html5")
                quoteText = "<div class='embed'><div class='quote'>{}</div><p>Embedded block reference: {}</p></div>".format(
                    htmlFragment, link)
            contents = contents.replace("![[{}]]".format(link), quoteText)
        return contents

    def _resolveEmbeddedSectionLinks(self, contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir):
        embeddedSectionLinks = regex.findall(
            "!\[\[(?:file:///)?([\s\w\.\\\/:%_\+\.\~\?\&\=\-]+\#[\w\s\w\.\\\/:%_\+\.\~\?\&\=\-]+)\]\]", contents, regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
        for link in embeddedSectionLinks:
            section = obsidianGetSectionFromLink(link)
            linkPath = obsidianGetFileFromLink(link)
            if os.path.isabs(linkPath):
                PrettyOutput.prettyWarning(
                    "You have absolute paths in your markdown files.  Please correct to use relative paths.")
            newLink = os.path.join(sourceDirectoryInBuildDir, linkPath)
            _, prefix = os.path.splitext(linkPath)
            if len(prefix) == 0:
                newLink = newLink + ".md"
            quoteText = obsidianGetSectionText(newLink, section)
            isChanged = True
            if quoteText is None:
                quoteText = "<pre>WARNING: Unable to find and retrieve block: {}\nParsed as {} at {}.</pre>".format(
                    link, newLink, section)
            else:
                htmlFragment = markdown.markdown(
                    quoteText, output_format="html5")
                quoteText = "<div class='embed'><div class='quote'>{}</div><p>Embedded block reference: {}</p></div>".format(
                    htmlFragment, link)
            contents = contents.replace("![[{}]]".format(link), quoteText)
        return contents

    def _resolveLinkedBlockLinks(self, contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir):
        # A linked block is one that obsidian would allow the user to click on and go to that
        # block.  Here we want to embed it in our HTML.
        linkedBlocksLinks = regex.findall(REGEX_LINKED_BLOCK,
                                          contents, regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
        for link in linkedBlocksLinks:
            block = obsidianGetBlockFromLink(link)
            linkPath = obsidianGetFileFromLink(link)
            if os.path.isabs(linkPath):
                PrettyOutput.prettyWarning(
                    "You have absolute paths in your markdown files.  Please correct to use relative paths.")
            newLink = os.path.join(sourceDirectoryInBuildDir, linkPath)
            _, prefix = os.path.splitext(linkPath)
            if len(prefix) == 0:
                newLink = newLink + ".md"
            quoteText = obsidianGetBlockText(newLink, block)
            if quoteText is None:
                quoteText = "<pre>WARNING: Unable to find and retrieve block: {}\nParsed as {} at {}.</pre>".format(
                    link, newLink, block)
            else:
                htmlFragment = markdown.markdown(
                    quoteText, output_format="html5")
                quoteText = "<div class='embed'><div class='quote'>{}</div><p>Linked block reference: {}</p></div>".format(
                    htmlFragment, link)
            contents = contents.replace("[[{}]]".format(link), quoteText)
        return contents

    def _resolveGitHubImageLinks(self, contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir):
        """
        See https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax#images for more information on image embeddings.
        Here we only handle embeddings from LOCAL image files.
        """
        imageEmbeddings = regex.findall(REGEX_GITHUB_LINK_GENERIC,
                                        contents, regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
        for embedding in imageEmbeddings:
            items = regex.findall(REGEX_GITHUB_LINK_LOCATION, embedding,
                                  regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
            (alttext, img) = items[0]
            Logger.info("Github Alt Text: {}".format(alttext))
            Logger.info("Github Image   : {}".format(img))
            if not obsidianIsUrl(img):
                imageRelativeToWorkingDirectory = self._findImageInBuildDirectory(
                    img, sourceDirectoryInBuildDir)

                if imageRelativeToWorkingDirectory is None:
                    PrettyOutput.prettyWarning(
                        "Broken Github embedded image '{}' in '{}' (searched in {})".format(img, filePathInBuildDirectory, sourceDirectoryInBuildDir))
                    contents = contents.replace(embedding, "[{} (Broken link)]({})".format(
                        img, filePathInBuildDirectory))
                    continue

                # We get the relative path from the destination directory to where we are running the
                # extraction script, normally in the parent of the project root.
                # e.g. expected value is something along the lines of ../../../
                relativePathToDestinationPathFromWorkingDirectory = os.path.relpath(
                    os.path.dirname(imageRelativeToWorkingDirectory), destinationDirectoryInExtractedDir)
                imagePathRelativeFromDestinationDirectory = os.path.join(
                    relativePathToDestinationPathFromWorkingDirectory, img)

                # now we are going to create some new markdown, we do it this way in case there are other
                # standard transformation options embedded in the img link which we have lopped off when finding the file.
                imageStandardMarkdownText = "![{}]({})".format(
                    alttext, imagePathRelativeFromDestinationDirectory)
                # this will give us something like <img src="blah.png" alt="blah">
                htmlFragment = markdown.markdown(
                    imageStandardMarkdownText, output_format="html5")
                # this HTML transforms the markdown link into a clickable piece of HTML
                quoteText = "<div class='embed'><div class='quote'><a href='{}' target='_blank'>{}</a></div><p>Embedded image reference: {}<br/>Alt text: {}</p></div>".format(
                    imagePathRelativeFromDestinationDirectory, htmlFragment, img, alttext)
                contents = contents.replace(
                    embedding, quoteText)
        return contents

    def _resolveGitHubDirectImageLinks(self, contents, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir):
        """
        See https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax#images for more information on image embeddings.
        Here we only handle embeddings from LOCAL image files.
        """
        imageEmbeddings = regex.findall(REGEX_GITHUB_DIRECT_IMAGE_LINK_GENERIC,
                                        contents, regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
        for embedding in imageEmbeddings:
            items = regex.findall(REGEX_GITHUB_DIRECT_IMAGE_LINK_LOCATION, embedding,
                                  regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
            (alttext, img) = items[0]
            Logger.info("Github Alt Text: {}".format(alttext))
            Logger.info("Github Image   : {}".format(img))
            if not obsidianIsUrl(img):
                imageRelativeToWorkingDirectory = self._findImageInBuildDirectory(
                    img, sourceDirectoryInBuildDir)

                if imageRelativeToWorkingDirectory is None:
                    PrettyOutput.prettyWarning(
                        "Broken Github embedded image '{}' in '{}' (searched in {})".format(img, filePathInBuildDirectory, sourceDirectoryInBuildDir))
                    contents = contents.replace(embedding, "[{} (Broken link)]({})".format(
                        img, filePathInBuildDirectory))
                    continue

                # We get the relative path from the destination directory to where we are running the
                # extraction script, normally in the parent of the project root.
                # e.g. expected value is something along the lines of ../../../
                relativePathToDestinationPathFromWorkingDirectory = os.path.relpath(
                    os.path.dirname(imageRelativeToWorkingDirectory), destinationDirectoryInExtractedDir)
                imagePathRelativeFromDestinationDirectory = os.path.join(
                    relativePathToDestinationPathFromWorkingDirectory, img)

                # now we are going to create some new markdown, we do it this way in case there are other
                # standard transformation options embedded in the img link which we have lopped off when finding the file.
                imageStandardMarkdownText = "![{}]({})".format(
                    alttext, imagePathRelativeFromDestinationDirectory)
                # this will give us something like <img src="blah.png" alt="blah">
                htmlFragment = markdown.markdown(
                    imageStandardMarkdownText, output_format="html5")
                # this HTML transforms the markdown link into a clickable piece of HTML
                quoteText = "<div class='embed'><div class='quote'><a href='{}' target='_blank'>{}</a></div><p>Embedded image reference: {}<br/>Alt text: {}</p></div>".format(
                    imagePathRelativeFromDestinationDirectory, htmlFragment, img, alttext)
                contents = contents.replace(
                    embedding, quoteText)
        return contents
