from DirectoryUtils import isMarkdown, isTextFile
from PortfolioTreeVisitor import PortfolioTreeVisitor
from DirectoryUtils import isLearningOutcomeEvidenceDirectory, isLatest, isTextFile
import os
import PrettyOutput
import regex
import Logger

class ReferenceCountPlugin(PortfolioTreeVisitor):

    def __init__(self, config):
        super().__init__(config)
        self.referenceCount = 0
        self.info = []
        self.warnings = []

    def matchFile(self, path, ext):
        dirOnly = os.path.dirname(path)
        return isTextFile(path) and isLatest(path) and isLearningOutcomeEvidenceDirectory(dirOnly)

    def start(self):
        self.referenceCount = 0
        self.info = []
        self.warnings = []
        return True

    def run(self, path):
        self.referenceCount += self._countReferences(path)
        return True

    def finish(self):
        if self.referenceCount == 0:
            warning = "No references were found in your files, this could be because the format does not match what we are looking for but it could also be that you have not put any in. It is advised that you attempt to cite your work using the Harvard referencing style."
            self.warnings.append(warning)
            if self.config is not None:
                PrettyOutput.prettyWarning(warning)

    def _countReferences(self, path):
        totalReferences = 0
        with open(path, "r", encoding="utf8") as inFile:
            content = inFile.read()
            references = regex.findall("(?:^|\n)([\p{L}\-\.,& ]+ \d\d\d\d[,\.])", content, regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
            referenceCount = len(references)
            totalReferences += referenceCount
            if totalReferences > 0:
                info = "{} references potentially found in {}".format(referenceCount, path)
                self.info.append(info)
                Logger.info(info)
        return totalReferences
