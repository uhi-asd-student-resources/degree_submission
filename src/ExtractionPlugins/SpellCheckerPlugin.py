from spellchecker import SpellChecker
from DirectoryUtils import isDotGitDirectory, isTextFile, isLatest, isLearningOutcomeEvidenceDirectory
import os
import PrettyOutput
import re
import ExtractionPlugins.SpellingAndGrammar as SpellingAndGrammar
from PortfolioTreeVisitor import PortfolioTreeVisitor

class SpellCheckerPlugin(PortfolioTreeVisitor):

    degreeSubmissionRootPath = os.path.dirname(os.path.dirname(
        os.path.dirname(__file__)))
    spellCheckListFilePath = os.path.join(degreeSubmissionRootPath, "spellings.txt")

    def __init__(self, config=None, spellingList=None):
        super().__init__(config)
        self.spellChecker = None
        if not spellingList:
            self.spellingList = self.spellCheckListFilePath
        else:
            self.spellingList = spellingList
        if not self.spellChecker and self.spellingList:
            self.spellChecker = SpellChecker()
            self.spellChecker.word_frequency.load_text_file(self.spellingList)

    def _isCapitalisedCorrectly(self, spellings, word):
        if not isinstance(spellings, list):
            spellings = [spellings]
        expectedWord = word.upper()
        if expectedWord in spellings:
            if not word.isupper():
                PrettyOutput.prettywarning(
                    "{} should be all uppercase, like '{}', as it is an abbreviation or mnemonic", word, expectedWord)
                return False
        return True

    def _cleanText(self, contents):
        contents = SpellingAndGrammar.removeMarkdownLinkFromString(contents)
        contents = SpellingAndGrammar.removeCitationsFromString(contents)
        contents = SpellingAndGrammar.removeReferencesFromString(contents)
        contents = SpellingAndGrammar.removeLearningOutcomesFromString(
            contents)
        contents = SpellingAndGrammar.removeLikelyUrlsFromString(contents)
        contents = SpellingAndGrammar.removeFilePathsFromString(contents)
        contents = SpellingAndGrammar.removeReferencesFromString(contents)
        contents = SpellingAndGrammar.removeApostropheSuffixesFromString(
            contents)
        contents = SpellingAndGrammar.removeObsidianBlocksFromString(contents)
        contents = SpellingAndGrammar.removePunctuationFromString(contents)
        return contents

    def _getBagOfWordsForFile(self, contents):
        contents = self._cleanText(contents)
        cleanContents = re.split(
            "\s", contents, re.MULTILINE | re.UNICODE | re.IGNORECASE)
        bagOfWords = []
        for word in cleanContents:
            if SpellingAndGrammar.isWordOfSignificantLength(word):
                if not SpellingAndGrammar.isNumber(word):
                    bagOfWords.append(word)
        return bagOfWords

    def _readUserSpellCheckList(self, file):
        with open(file, "r", encoding="utf-8") as inFile:
            spellingContent = inFile.read()
            spellings = spellingContent.split("\n")
            return spellings
        return spellings

    def _filterForCapitalisedWordsOnly(self, wordlist):
        return [x.strip() for x in wordlist if x.isupper()]

    def _checkCapitalisationForWordList(self, wordList):
        spellings = self._readUserSpellCheckList(self.spellCheckListFilePath)
        spellings = self._filterForCapitalisedWordsOnly(spellings)
        incorrectCount = 0
        for word in wordList:
            incorrectCount += not self._isCapitalisedCorrectly(spellings, word)
        return incorrectCount

    def spellCheckText(self, text):
        listOfWordsToCheckSpellingFor = self._getBagOfWordsForFile(
            text)
        self._checkCapitalisationForWordList(listOfWordsToCheckSpellingFor)
        misspelledWordList = self.spellChecker.unknown(
            listOfWordsToCheckSpellingFor)
        return misspelledWordList

    def _spellCheck(self, file):
        with open(file, "r", encoding="utf-8") as inFile:
            contents = inFile.read()
            misspelledWordList = self.spellCheckText(contents)
            if len(misspelledWordList) > 0:
                PrettyOutput.prettyWarning("The file '{}' has the following spelling mistakes: {}.  It is recommended you check your work before submission and correct spelling mistakes.  There may be many false positives if the words are technical in nature, if so you can add the words to 'spellings.txt' for them to be ignored in future runs.".format(file, misspelledWordList))
            return len(misspelledWordList)

    def spellCheckFile(self, filePath):
        if isTextFile(filePath) and isLatest(filePath):
            if os.path.isfile(filePath):
                errors = self._spellCheck(filePath)
                hasMoreThanOneParagraph = SpellingAndGrammar.checkParagraphsInFile(filePath)
                return errors > 0 or not hasMoreThanOneParagraph
            else:
                PrettyOutput.prettyWarning(
                    "Could not find file: {}".format(filePath))
        return False

    def spellCheckDirectory(self, dirPath):
        for root, _, files in os.walk(dirPath):
            if isDotGitDirectory(root):
                continue
            if not isLearningOutcomeEvidenceDirectory(root):
                continue
            for f in files:
                extractedFilePath = os.path.join(root, f)
                self._spellCheckFile(extractedFilePath)

    def start(self):
        return True

    def run(self, path=None):
        if os.path.isfile(path):
            self._spellCheckFile(path)
        elif os.path.isdir(path):
            self._spellCheckDirectory(path)
        elif self.config is not None:
            self._spellCheckDirectory(self.config.extractDirectory)
        else:
            PrettyOutput.prettyFixException(
                "Developer Error: Spell checker expected a path and did not receive either a file/directory nor was config set.")
        return True
    
    def finish(self):
        pass