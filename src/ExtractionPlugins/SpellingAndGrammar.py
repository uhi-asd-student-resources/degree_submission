import regex
import re
import PrettyOutput

COMMON_REGEX_FLAGS = regex.MULTILINE | regex.UNICODE | regex.IGNORECASE

def removeMarkdownLinkFromString(contents):
  contents = regex.sub("\[\[.*?\]\]", " ", contents, COMMON_REGEX_FLAGS)
  contents = regex.sub("\[.*?\]", " ", contents, COMMON_REGEX_FLAGS)
  return contents

def removeCitationsFromString(contents):
  contents = regex.sub("\(.*?\)", " ", contents, COMMON_REGEX_FLAGS)
  return contents

def removeReferencesFromString(contents):
  contents = regex.sub("(^|\s)(\p{L}[\p{L}\.,& ]+\(?\d\d\d\d\)?[., ].*?)(\n|$)", r"\1 \3", contents, COMMON_REGEX_FLAGS)
  return contents

def removeLearningOutcomesFromString(contents):
  contents = regex.sub("(^|\s*)\(\s*[Ll][Oo]\s*\d+\.\d+\.\d+\.\d+\s*\)", r"\1 " , contents, COMMON_REGEX_FLAGS)
  contents = regex.sub("(^|\s*)\(\s*\d+\.\d+\.\d+\.\d+\s*\)", r"\1 " , contents, COMMON_REGEX_FLAGS)
  contents = regex.sub("(^|\s)\s*[Ll][Oo]\s*\d+\.\d+\.\d+\.\d+\s*", r"\1 " , contents, COMMON_REGEX_FLAGS)
  contents = regex.sub("(^|\s)\s*\d+\.\d+\.\d+\.\d+\s*", r"\1 " , contents, COMMON_REGEX_FLAGS)
  return contents;

def removeLikelyUrlsFromString(contents):
  # remove urls, this is a very greedy regex on purpose
  contents = regex.sub(r"(https?:.*?)(\s|$)", r" \2", contents, COMMON_REGEX_FLAGS)
  return contents

def removeFilePathsFromString(contents):
    contents = regex.sub(r"([\w\/\:\\\&_\-]*\.\w{1,3})([^\w]|$)", r" \2", contents, COMMON_REGEX_FLAGS)
    return contents

def removeObsidianBlocksFromString(contents):
  contents = regex.sub(r"(\^#[0-9A-Za-z]{1,6})(\s|$)", r" \2", contents, COMMON_REGEX_FLAGS)
  return contents

def removeApostropheSuffixesFromString(contents):
  # remove apostrophe s and t
  contents = re.sub(r"([\'’]s)(\s|$)", r" \2", contents, COMMON_REGEX_FLAGS)
  contents = re.sub(r"([\'’]d)(\s|$)", r" \2", contents, COMMON_REGEX_FLAGS)
  contents = re.sub(r"(n[\'’]t)(\s|$)", r" \2", contents, COMMON_REGEX_FLAGS)
  return contents

def removePunctuationFromString(contents):
  # remove punctuation
  contents = re.sub("[\.:;,\-\/\@\&\#\~\"\'’\(\)\*\>_\<\[\]\u2018\u2019\u0022\u201C\u201D\!]", " ", contents, COMMON_REGEX_FLAGS)
  return contents

def removeMultipleSpacesFromString(contents):
  # remove multiple white spaces
  contents = re.sub("\s\s+", " ", contents, regex.MULTILINE)
  contents = re.sub("\n\n+", "\n", contents, regex.MULTILINE)
  return contents 

def isWordOfSignificantLength(word):
  return len(word) > 3

def isNumber(word):
  w = re.match(r"^[+-]?\d\.?\d*(?:e[+-]?\d\.?\d*)?$", word)
  return w is not None

def hasParagraphs(text):
  contents = text.strip()
  words = re.split(r"\s", contents)
  wordCount = len(words)
  newlineCharCount = contents.count("\n")
  if wordCount > 50:
    if newlineCharCount == 0:
      return False, wordCount, newlineCharCount
  return True, wordCount, newlineCharCount

def checkParagraphsInFile(filePath):
  with open(filePath, "r", encoding="utf-8") as inFile:
    contents = inFile.read()    
    success, wordCount, newlineCharCount = hasParagraphs(contents)
    if not success:
      PrettyOutput.prettyWarning("You have {} words in the file {} and only {} paragraph. Please review and make this more readable.".format(wordCount, filePath, newlineCharCount+2))
    return success
