import speech_recognition as sr
from pydub import AudioSegment
import os
import Logger
import speech_recognition
import PrettyOutput

def transcribe(fileToTranscribe, targetDirectory):
  success = True
  fileToTranscribe = fileToTranscribe.strip()
  if len(fileToTranscribe) == 0 or not fileToTranscribe:
      raise ValueError("[Transcribe] File to transcribe is empty")
  Logger.info("Attempting to transcribe '{}'".format(fileToTranscribe))
  if not os.path.isfile(fileToTranscribe):
      raise ValueError("[Transcribe] Could not find file to transcribe: '{}'".format(fileToTranscribe))

  filenameWithExt = os.path.basename(fileToTranscribe)
  path = os.path.dirname(fileToTranscribe)
  filename, extension = os.path.splitext(filenameWithExt)
  Logger.info("[Transcribe] Path: {}".format(path))
  Logger.info("[Transcribe] Base filename: {}".format(filename))
  Logger.info("[Transcribe] File Extension: {}".format(extension))
  if extension != ".mp3" and extension != ".wav" and extension != ".mp4":
      raise ValueError("[Transcribe] input file must be an mp3 or a wav file")

  outputTextFile = os.path.join(targetDirectory, "{}_transcription.txt".format(filename))
  if os.path.isfile(outputTextFile):
    Logger.info("Transcription {} already found, assuming student added this, exiting.".format(outputTextFile))
    return True

  isTemporaryWavFile = False
  if extension == ".mp4":
    # convert mp3 file to wav        
    isTemporaryWavFile = True
    wavFileToTranscribe = "~{}.wav".format(filename)
    if len(path) > 0:
        wavFileToTranscribe = os.path.join(path, wavFileToTranscribe)
    Logger.info("[Transcribe] Converting to wave file '{}'".format(wavFileToTranscribe))    
    try:                                               
        sound = AudioSegment.from_file(fileToTranscribe, "mp4")
        out = sound.export(wavFileToTranscribe, format="wav")
        out.close()
    except:
        raise RuntimeError("[Transcribe] Converting mp4 failed, trying manual ffmpeg conversion to wav format and try to transcribe the .wav file.")
  elif extension == ".mp3":
    # convert mp3 file to wav        
    isTemporaryWavFile = True
    wavFileToTranscribe = "~{}.wav".format(filename)
    if len(path) > 0:
        wavFileToTranscribe = os.path.join(path, wavFileToTranscribe)
    Logger.info("[Transcribe] Converting to wave file '{}'".format(wavFileToTranscribe))    
    try:                                               
        sound = AudioSegment.from_mp3(fileToTranscribe)
        out = sound.export(wavFileToTranscribe, format="wav")
        out.close()
    except:
        raise RuntimeError("[Transcribe] Converting mp3 failed, try manual ffmpeg conversion to wav format and try to transcribe the .wav file.")
  else:
      wavFileToTranscribe = fileToTranscribe

  # use the audio file as the audio source     
  PrettyOutput.prettyInfo("[Transcribe] Transcribing {} ... please wait".format(wavFileToTranscribe))
  try:
    r = sr.Recognizer()
    with sr.AudioFile(wavFileToTranscribe) as source:
        audio = r.record(source)  # read the entire audio file
        transcription = r.recognize_google(audio)
        words = transcription.split(" ")
        sentences = [[]]
        for w in words:
          if len(sentences[len(sentences)-1]) < 15:
            sentences[len(sentences)-1].append(w)
          else:
            sentences.append([])
        with open(outputTextFile, "w", encoding="utf8") as outFile:
          for s in sentences:
            lineContent = ' '.join(s)
            outFile.write(lineContent + "\n")
        PrettyOutput.prettyInfo("[Transcribe] {} was transcribed successfully")
  except speech_recognition.UnknownValueError as ex:
    Logger.error("[Transcribe] UnknownValueError exception was thrown with text '{}' (note if blank, exception string was empty)".format(ex))
    PrettyOutput.prettyWarning("[Transcribe] There was an error, the audio file '{}' was not transcribed.".format(wavFileToTranscribe))
    success = False

  if isTemporaryWavFile:
      Logger.info("[Transcribe] Removing temporary wav file {}".format(wavFileToTranscribe))
      os.remove(wavFileToTranscribe)

  Logger.info("[Transcribe] Transcription has been written to '{}'".format(outputTextFile))
  return success
