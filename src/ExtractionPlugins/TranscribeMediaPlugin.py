from PortfolioTreeVisitor import PortfolioTreeVisitor
import os
from ExtractionPlugins.TranscribeAudio import transcribe
import PrettyOutput

class TranscribeMediaPlugin(PortfolioTreeVisitor):

    def __init__(self, config, doTranscriptions=False):
        super().__init__(config)
        self.attemptedTranscribeCount = 0
        self.successTranscribeCount = 0
        self.warnings = []
        self.doTranscriptions = doTranscriptions
        if self.config:
            self.doTranscriptions = self.config.doTranscriptions

    def matchFile(self, path, ext):
        return ext == ".mp4" or ext == ".mp3" or ext == ".wav"

    def start(self):
        return True

    def run(self, path):
        if self.doTranscriptions:
            self.attemptedTranscribeCount += 1
            try:
                success = transcribe(path, os.path.dirname(path))
            except ValueError as e:
                PrettyOutput.prettyWarning(e)
            except RuntimeError as e:
                PrettyOutput.prettyWarning(e)
            finally:
                if success:
                    self.successTranscribeCount += 1
        return True

    def finish(self):
        # potential to raise more warnings if attempted != successful transcribe count
        pass