"""
Look through all files looking for bitbucket references.
Match reference to any of the repositories they have submitted.
If found, then grab the relevant file from there.
If not found, then warn.
"""

from DirectoryUtils import findFileInRepository, isDotGitDirectory, isTextFile, isLatest, isLearningOutcomeEvidenceDirectory
import os
from PortfolioTreeVisitor import PortfolioTreeVisitor
import PrettyOutput
import regex
from ExtractionPlugins.UrlHandlers.BitbucketUrlHandler import BitbucketUrlHandler
from ExtractionPlugins.UrlHandlers.WebexUrlHandler import WebexUrlHandler

class UrlExtractorPlugin(PortfolioTreeVisitor):
    def __init__(self, config):
        super().__init__(config)
        self.handlers = []

    def matchFile(self, path, ext):
        dirOnly = os.path.dirname(path)
        return isTextFile(path) and isLatest(path) and isLearningOutcomeEvidenceDirectory(dirOnly)

    def addHandler(self, handler):
        self.handlers.append(handler)

    def extractFromText(self, text):
        urls = regex.findall("(?P<url>https?://[\w\.\\\/:%_\+\.\~\?\&\=\-\#\^]+)",
                                text, regex.UNICODE | regex.MULTILINE | regex.IGNORECASE)
        cleanedUrls = []
        for url in urls:
            if url.endswith("."):
                url = url[:(len(url)-2)]
            cleanedUrls.append(url)
        return cleanedUrls
            
    def extractFromFile(self, filePath):
        if isTextFile(filePath) and isLatest(filePath):
            if os.path.isfile(filePath):
                with open(filePath, "r", encoding="utf-8") as inFile:
                    contents = inFile.read()
                    urls = self.extractFromText(contents)
                    return [{
                        "urls": urls,
                        "filePath": filePath
                    }]
            else:
                PrettyOutput.prettyWarning(
                    "Could not find file: {}".format(filePath))
        return None

    def extractFromDirectory(self, directoryPath):
        all_extracted_urls = []
        if not os.path.isdir(directoryPath):
            return
        for root, _, files in os.walk(directoryPath):
            if isDotGitDirectory(root):
                continue
            if not isLearningOutcomeEvidenceDirectory(root):
                continue
            for f in files:
                extractedPath = os.path.join(root, f)
                extracted_urls = self.extractFromFile(extractedPath)
                if extracted_urls:
                    all_extracted_urls += extracted_urls
        return all_extracted_urls

    def _runHandlersOverUrls(self, extractedUrls):
        if not extractedUrls:
            return
        for extract in extractedUrls:
            filePath = extract["filePath"]
            urls = extract["urls"]
            for url in urls:
                for handler in self.handlers:
                    if handler.match(url):
                        handler.execute(filePath, url)

    def start(self):
        return True

    def run(self, path=None):
        if path is None:
            path = self.config.extractDirectory
        extractedUrls = []
        if os.path.isfile(path):
            extractedUrls = self.extractFromFile(path)
        elif os.path.isdir(path):
            extractedUrls = self.extractFromDirectory(path)
        
        self._runHandlersOverUrls(extractedUrls)
        return True

    def finish(self):
        pass

def createUrlExtractor(config):
    plugin = UrlExtractorPlugin(config)
    plugin.addHandler(WebexUrlHandler(config))
    plugin.addHandler(BitbucketUrlHandler(config))
    return plugin