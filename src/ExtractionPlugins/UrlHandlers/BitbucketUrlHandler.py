from ExtractionPlugins.UrlHandlers.UrlHandler import UrlHandler
import PrettyOutput
import os
import Repositories
import regex
import Logger
import pydriller
from DirectoryUtils import findFileInRepository
import shutil

class BitbucketUrlHandler(UrlHandler):
    def __init__(self, config):
        super().__init__(config)
    
    def match(self, url):
        return regex.search("bitbucket\.org", url, regex.IGNORECASE | regex.UNICODE) is not None

    def execute(self, filePath, url ):
        # We are looking to see if we have the bitbucket file in what the student has
        # added to their portfolio.
        repositories = self.config.repositoryList
        found = False
        matchedRepo = None
        for r in repositories:
            if r.startswith("git@"):
                pos = r.find(":")
                repo = r[pos+1:]
            else:
                repo = r
            if regex.search(repo, url, regex.IGNORECASE | regex.UNICODE):
                Logger.info(
                    "[Bitbucket Ref] Matched {} to {} (used {})".format(r, url, repo))
                found = True
                matchedRepo = r
                break

            if repo.endswith(".git"):
                repo = repo.replace(".git", "")
            if regex.search(repo, url, regex.IGNORECASE | regex.UNICODE):
                Logger.info(
                    "[Bitbucket Ref] Matched {} to {} (used {})".format(r, url, repo))
                found = True
                matchedRepo = r
                break

        if not found:
            PrettyOutput.prettyWarning(
                "[Bitbucket Reference Extraction] Failed to match any of the student's local repositories to {}. This means the bitbucket link is not part of a repository you have in your portfolio.  If you think this repository SHOULD be part of your portfolio, then you need to add the repository to the repositories.txt file.  Otherwise you can ignore this warning.".format(url))
            return

        repoMap = Repositories.matchRepositoryToLocalDirectory(
            matchedRepo, self.config.buildDirectory)
        commits = regex.findall("commits/(\w+)", url,
                                regex.UNICODE | regex.IGNORECASE)
        if len(commits) == 0:
            Logger.error("Commit {} was not found in {}".format(
                url, repoMap["directory"]))
        else:
            commit = commits[0]

            try:
                for curr in pydriller.Repository(repoMap["directory"], single=commit).traverse_commits():
                    for m in curr.modified_files:
                        if m.change_type.name != "DELETE" and m.new_path:
                            srcPath = os.path.join(
                                repoMap["directory"], m.new_path)
                            if not os.path.isfile(srcPath):
                                foundSrcPath = findFileInRepository(srcPath)
                                if foundSrcPath is None:
                                    PrettyOutput.prettyWarning(
                                        "[Bitbucket Ref] Unable to find {} linked to commit {}".format(srcPath, commit))
                                    return
                                else:
                                    Logger.info("[Bitbucket Ref] Searched for {}, found {}".format(
                                        srcPath, foundSrcPath))
                                    srcPath = foundSrcPath
                            destDir = os.path.dirname(filePath)
                            name, ext = os.path.splitext(
                                os.path.basename(filePath))
                            destPath = os.path.join(destDir, "{}_ref_{}_{}".format(
                                name, commit[0:7], m.filename))
                            if not os.path.isfile(destPath):
                                Logger.info("[Bitbucket Ref] Copying reference source from {} to {}".format(
                                    srcPath, destPath))
                                shutil.copy(srcPath, destPath)
            except ValueError as e:
                PrettyOutput.prettyWarning("[BitbucketUrlHandler] When attempting to retrieve the url '{}' from file '{}'. Commit {} was not found in {}.  The following ValueError was given: {}.  This may be caused by using bfg-repo-cleaner on a repository, in which case you can ignore this error.  Otherwise, you might want to investigate your repository further.".format(url, filePath, commit, repoMap["directory"], e))
