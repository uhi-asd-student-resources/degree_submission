"""
Handlers for urls extracted from the students work
They are passed the file that the url was found in and the url
"""

class UrlHandler:
    """
    Abstract command class once we have extracted all the urls with the UrlExtractor
    """
    def __init__(self, config):
        self.config = config

    def match(self, url):
        pass

    def execute(self, filePath, url):
        pass

