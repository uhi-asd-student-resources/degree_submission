from ExtractionPlugins.UrlHandlers.UrlHandler import UrlHandler
import PrettyOutput
import regex

class WebexUrlHandler(UrlHandler):
    def __init__(self, config):
        super().__init__(config)

    def match(self, url):
        return regex.search("uhi\.webex\.com", url, regex.IGNORECASE | regex.UNICODE) is not None

    def execute(self, filePath, url):
        PrettyOutput.prettyGuidance(
            "Webex url found ({}) in {}, any video being included should follow the video guidelines and be included in the assets directory. Video urls will be ignored.".format(url, filePath))
