from DirectoryUtils import isMarkdown, isTextFile
from PortfolioTreeVisitor import PortfolioTreeVisitor
from DirectoryUtils import isLearningOutcomeEvidenceDirectory, isLatest, isTextFile, sha256sum
import os
import PrettyOutput
import re
import Logger


class WordCountPlugin(PortfolioTreeVisitor):

    def __init__(self, config, expectedWordCount=[2500, 2500]):
        super().__init__(config)
        self.wordCount = 0
        self.warnings = []
        self.hashedFiles = []
        # min and max self.wordCount for year/modules
        self.expectedWordCount = expectedWordCount
        if self.config is not None:
            self.expectedWordCount = self.config.expectedWordCountLimits

    def matchFile(self, path, ext):
        dirOnly = os.path.dirname(path)
        return isTextFile(path) and isLatest(path) and isLearningOutcomeEvidenceDirectory(dirOnly)

    def start(self):
        self.wordCount = 0
        self.wordCountPerFile = []
        self.info = []
        self.warnings = []
        self.hashedFiles = []
        return True

    def run(self, path):
        self.wordCount += self._wordCount(path)
        return True

    def finish(self):
        Logger.info("Word count of extracted files: {}".format(self.wordCount))
        if self.config is not None:
            Logger.info("Expected word count for modules {} in years {}: {} - {}".format(
                self.config.moduleList, self.config.yearList, self.expectedWordCount[0], self.expectedWordCount[1]))

        warning = None
        if self.wordCount > self.expectedWordCount[0] * 1.10 and self.wordCount < self.expectedWordCount[1] * 1.21:
            warning = "Word count {} is within 11-20% bounds - may incur 5% mark deduction. This is just a guide and if you are happy with your submission then please continue.".format(
                self.wordCount)
        elif self.wordCount > self.expectedWordCount[0] * 1.20 and self.wordCount < self.expectedWordCount[1] * 1.31:
            warning = "Word count {} is within 21-30% of bounds - may incur 10% mark deduction. This is just a guide and if you are happy with your submission then please continue.".format(
                self.wordCount)
        elif self.wordCount > self.expectedWordCount[0] * 1.31 and self.wordCount < self.expectedWordCount[1] * 1.41:
            warning = "Word count {} is within 31-40% of bounds - may incur 10% mark deduction. This is just a guide and if you are happy with your submission then please continue.".format(
                self.wordCount)
        elif self.wordCount > self.expectedWordCount[0] * 1.41 and self.wordCount < self.expectedWordCount[1] * 1.51:
            warning = "Word count {} is within 41-50% of bounds - may incur 20% mark deduction. This is just a guide and if you are happy with your submission then please continue.".format(
                self.wordCount)
        elif self.wordCount >= self.expectedWordCount[0] * 1.51:
            warning = "Word count {} is within 51%+ of bounds - may incur 50% mark deduction. This is just a guide and if you are happy with your submission then please continue.".format(
                self.wordCount)
        elif self.wordCount <= self.expectedWordCount[0] * 0.80:
            warning = "Word count {} is less than or equal to 80% of expected word count. This is just a guide and if you are happy with your submission then please continue.".format(
                self.wordCount)
        else:
            Logger.info("Word count {} is within bounds. This is just a guide and if you are happy with your submission then please continue.".format(
                self.wordCount))
        if warning:
            self.warnings.append(warning)
            PrettyOutput.prettyWarning(warning)

    def _wordCount(self, path):
        # Calculate the word count
        # WARNING: if the same file exists in 2 hashes this currently will count it twice.
        totalCount = 0
        hash = sha256sum(path)
        if hash in self.hashedFiles:
            return totalCount
        with open(path, "r", encoding="utf8") as inFile:
            contents = inFile.read()
            contents = re.sub("\s\s*", " ", contents)
            contentsArray = contents.split(" ")
            contentsArray = [x for x in contentsArray if len(x.strip()) > 0]
            thisWordCount = len(contentsArray)
            Logger.info("Word count: {} SHA1: {} Path: {}".format(
                thisWordCount, hash, path))
            self.hashedFiles.append(hash)
            totalCount += thisWordCount
            return totalCount
