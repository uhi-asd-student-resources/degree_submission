"""
Basic class for a learning outcome
"""
class LearningOutcome:
    def createFromCsvData(row):
        lo = LearningOutcome()
        lo.theme = int(row[0])
        lo.moduleName = row[1]
        lo.type = row[2]
        lo.moduleNumber = int(row[3])
        lo.loNumber = int(row[4])
        lo.loCode = row[5]
        lo.description = row[6]
        lo.year = int(row[7])
        return lo
        
    def __init__(self):
        self.theme = None            # grouped by Software Dev (1), Operations(2), Data (3)
        self.year = 0
        self.moduleName = None
        self.type = None
        self.moduleNumber = None
        self.loNumber = None            # learning outcome number which is the index
        self.loCode = None              # the learning outcome code
        self.description = None

    def isSoftwareDevelopmentTheme(self):
        return self.theme % 3 == 1

    def isOperationsTheme(self):
        return self.theme % 3 == 2

    def isDataTheme(self):
        return self.theme % 3 == 0
