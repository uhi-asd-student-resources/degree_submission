import os
import csv
from LearningOutcome import LearningOutcome
import Logger

class LearningOutcomesCollection:
    DEFAULT_LEARNING_OUTCOMES_FILEPATH = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "data", "learningoutcomes.csv")

    def __init__(self, values=None):
        self.data = []

    def add(self, lo):
        self.data.append(lo)

    def size(self):
        return len(self.data)

    def log(self):
        for loItem in self.data:
            Logger.info("[LO] {} {} {}".format(loItem.moduleName, loItem.loCode, loItem.description))

    def contains(self, lo):
        if not lo:
            raise RuntimeError(
                "Attempted to find {} in LearningOutcomeCollection".format(lo))
        for loItem in self.data:
            if loItem.loCode == lo:
                return True
        return False

    def getTopicFor(self, code):
        for loItem in self.data:
            if loItem.loCode == code:
                return loItem.type
        return None

    def filter(self, yearList, moduleList):
        assert(len(yearList) == len(moduleList))
        newCollection = LearningOutcomesCollection()
        for index, moduleNumber in enumerate(moduleList):
            for loItem in self.data:
                if loItem.year == yearList[index] and loItem.moduleNumber == moduleNumber:
                    newCollection.add(loItem)
        return newCollection

    def filterByYear(self, yr):
        if not yr:
            raise RuntimeError(
                "year is equal to {}, this is an invalid value must be 1-4".format(yr))
        if isinstance(yr, list) == False:
            yr = [yr]
        newCollection = LearningOutcomesCollection()
        for lo in self.data:
            if lo.year in yr:
                newCollection.add(lo)
        return newCollection

    def filterByModuleNumber(self, modules):
        if not modules:
            raise RuntimeError(
                "modules is equal to {}, this is an invalid value must be 1-6".format(modules))
        if isinstance(modules, list) == False:
            modules = [modules]
        newCollection = LearningOutcomesCollection()
        for lo in self.data:
            if lo.moduleNumber in modules:
                newCollection.add(lo)
        return newCollection

    def filterByModuleNumberAndLoIndex(self, yearNumber, moduleNumber, loIndex):
        """
        Returns the learning outcome that matches the module number e.g. UI107006 and the loIndex, which 
        is the index of the learning outcome.  Will be in the order that it is in the learningoutcomes.csv.
        """
        index = 1
        for lo in self.data:
            if lo.moduleNumber == moduleNumber and lo.year == yearNumber:
                if index == loIndex:
                    return lo
                index += 1
        return None

def createDefaultLearningOutcomesCollection():
    defaultFilePath = LearningOutcomesCollection.DEFAULT_LEARNING_OUTCOMES_FILEPATH
    if not os.path.isfile(defaultFilePath):
        raise RuntimeError(
            "Missing learning outcomes data file ({}).".format(defaultFilePath))
    learningOutcomes = LearningOutcomesCollection()
    with open(defaultFilePath, "r",  encoding="utf8") as inFile:
        data = csv.reader(inFile, delimiter='|', quotechar='"')
        for index, row in enumerate(data):
            if index == 0:
                continue
            elif len(row) == 0:
                continue
            elif len(row[0].strip()) == 0:
                continue
            elif row[0].strip()[0] == '#':
                continue
            else:
                lo = LearningOutcome.createFromCsvData(row)
                learningOutcomes.add(lo)
        if learningOutcomes.size() != 120:
            raise RuntimeError("Found only {} learning outcomes, not the expected 120.".format(
                learningOutcomes.size()))
    return learningOutcomes
