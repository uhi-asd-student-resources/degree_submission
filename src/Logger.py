"""
We were going to use the logging package but when we implemented extractclass I wanted to be able
to have multiple handlers that I could select which the default was and the basic logger did 
not seem to have an obvious way to do this.

Not thread-safe or anything other than a quick hack to solve an issue.
"""
from distutils.debug import DEBUG
import sys
import datetime

NOTSET  = 0
ALL     = 0
DEBUG   = 10
INFO    = 20
WARN    = 30
ERROR   = 40
CRITICAL = 50
QUIET   = 255

def getLevelName(level):
    if level == NOTSET or level == ALL:
        return "NOTSET"
    elif level == DEBUG:
        return "DEBUG"
    elif level == INFO:
        return "INFO"
    elif level == WARN:
        return "WARN"
    elif level == ERROR:
        return "ERROR"
    elif level == CRITICAL:
        return "CRITICAL"
    elif level == QUIET:
        return "QUIET"

def parseFormatString(text):
    tokens = []
    pos = 0
    lastPos = 0
    while pos < len(text):
        if text[pos] == "%" and text[pos+1] == "(":
            if pos > lastPos:
                token = {
                    "type": "text",
                    "value": text[lastPos:pos]
                }
                tokens.append(token)
            
            width = None
            key = None
            type = "string"

            closePos = pos
            while closePos < len(text):
                if text[closePos] == ")":
                    break
                closePos+=1
            key = text[pos:(closePos+1)]
            pos = closePos+1

            if text[pos] == "s":
                type = "string"
                pos += 1
            elif text[pos] == "f":
                type = "float"
                pos += 1
            elif text[pos] == "-":
                startWidth = pos
                endWidth = pos+1
                while endWidth < len(text):
                    if text[endWidth] == "s":
                        break
                    endWidth+=1
                width = text[(startWidth+1):endWidth]
                pos = endWidth +1
            token = {
                "label": key,
                "type": type,
                "width": width
            }
            tokens.append(token)
            lastPos = pos
            continue
        pos += 1
    if pos > lastPos:
        token = {
            "type": "text",
            "value": text[lastPos:pos]
        }
        tokens.append(token)
    return tokens

class Logger:
    def __init__(self, name, level=INFO, format="%(asctime)s %(name)-12s %(levelname)-8s %(msg)s", datefmt="%Y-%m-%d %H:%M:%S", filename=sys.stdout, filemode="a+"):
        self.name = name
        self.handlers = []
        self.minLogLevel = level
        self.format = format
        self.dateFormat = datefmt
        self.filePath = filename
        self.fileMode = filemode

    def compose(self, level, msg):
        asctime = datetime.datetime.strftime(datetime.datetime.now(), self.dateFormat)
        levelname = getLevelName(level)
        tokens = parseFormatString(self.format)
        formattedMessage = ""
        for t in tokens:
            if t["type"] == "text":
                formattedMessage += "{}".format(t["value"])
            elif t["type"] == "string":
                partialFormattedMessage = ""
                if t["width"]:
                    partialFormattedMessage = "{:<"+t["width"]+"}"
                else:
                    partialFormattedMessage = "{}"
                if t["label"] == "%(asctime)":
                    partialFormattedMessage = partialFormattedMessage.format(asctime)
                elif t["label"] == "%(name)":
                    partialFormattedMessage = partialFormattedMessage.format(self.name)
                elif t["label"] == "%(levelname)":
                    partialFormattedMessage = partialFormattedMessage.format(levelname)
                elif t["label"] == "%(msg)" or t["label"] == "%(message)":
                    partialFormattedMessage = partialFormattedMessage.format(msg)
                
                formattedMessage += partialFormattedMessage
        return formattedMessage


    def log(self, level, msg):
        if level >= self.minLogLevel:
            formattedMessage = self.compose(level, msg)    
            formattedMessage += "\n"
            if self.filePath == sys.stdout:
                sys.stdout.write(formattedMessage)
            elif self.filePath == sys.stderr:
                sys.stderr.write(formattedMessage)
            else:
                with open(self.filePath, self.fileMode) as outFile:
                    outFile.write(formattedMessage)

# static class
class LoggerCollection:
    loggerStack = []
    loggers = {}
    default = None

    @staticmethod
    def addLogger(name, logger):
        if name in LoggerCollection.loggers:
            raise ValueError("name {} already exits as a logger".format(name))
        LoggerCollection.loggers[name] = logger
        LoggerCollection.setDefault(name)
        LoggerCollection.loggerStack.append(name)

    @staticmethod
    def pop():
        lastLogger = LoggerCollection.loggerStack.pop()
        LoggerCollection.loggers.pop(lastLogger)
        if len(LoggerCollection.loggerStack) == 0:
            raise RuntimeError("Logger stack is empty!")
        lastLogger = LoggerCollection.loggerStack[-1]
        LoggerCollection.setDefault(lastLogger)

    @staticmethod
    def setDefault(name):
        if name not in LoggerCollection.loggers:
            raise RuntimeError("Logger {} does not exist".format(name))
        LoggerCollection.default = name

    @staticmethod
    def info(msg):
        if LoggerCollection.default is None:
            basicConfig()
        LoggerCollection.loggers[LoggerCollection.default].log(INFO, msg)

    @staticmethod
    def warn(msg):
        if LoggerCollection.default is None:
            basicConfig()
        LoggerCollection.loggers[LoggerCollection.default].log(WARN, msg)

    @staticmethod
    def error(msg):
        if LoggerCollection.default is None:
            basicConfig()
        LoggerCollection.loggers[LoggerCollection.default].log(ERROR, msg)
    
    @staticmethod
    def getDefault():
        return LoggerCollection.loggers[LoggerCollection.default]

def pop():
    LoggerCollection.pop()

def setDefault(name):
    LoggerCollection.setDefault(name)

# Provide similar interface as logging module
def info(msg):
    LoggerCollection.info(msg)

def warn(msg):
    LoggerCollection.warn(msg)

def warning(msg):
    LoggerCollection.warn(msg)

def error(msg):
    LoggerCollection.error(msg)

def basicConfig(name="default", level=INFO, format="%(asctime)s %(name)-12s %(levelname)-8s %(msg)s", datefmt="%Y-%m-%d %H:%M:%S", filename=sys.stdout, filemode="a+"):
    assert(name is not None)
    assert(level is not None)
    assert(format is not None)
    assert(datefmt is not None)
    assert(filename is not None)
    assert(filemode is not None)
    LoggerCollection.addLogger(name, Logger(name, level, format, datefmt, filename, filemode))

def get():
    return LoggerCollection.getDefault()