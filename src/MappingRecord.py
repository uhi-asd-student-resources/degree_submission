
class MappingRecord:
    def createFromCsv(row):
        record = MappingRecord()
        record.portfolioWorthy = (row[0].strip().upper() == 'TRUE')
        record.learningOutcomeCode = row[1].upper().strip()
        record.evidenceType = row[2].upper().strip()
        record.author = row[3].strip()
        record.repository = row[4].strip()
        record.digest = row[5].strip()
        return record

    def __init__(self):
        self.portfolioWorthy = None
        self.learningOutcomeCode = None
        self.evidenceType = None
        self.author = None
        self.repository = None
        self.digest = None
        self.commits = None
        self.repositoryDirectory = None

    def __repr__(self):
        return "{{{},{},{},{},{},{},{},{}}}".format(self.portfolioWorthy, self.learningOutcomeCode, self.evidenceType, self.author, self.repository, self.digest, self.commits, self.repositoryDirectory)