import csv
from MappingRecord import MappingRecord
import os    
import PrettyOutput
from DirectoryUtils import isGitRoot
import git
import Logger

class MappingRecordCollection:
    def __init__(self):
        self.mappingRecords = []

    def add(self, record):
        self.mappingRecords.append(record)

    def getAuthors(self):
        authors = []
        for m in self.mappingRecords:
            authors.append(m.author)
        return list(set(authors))

    def throwIfEmpty(self):
        if not self.mappingRecords or len(self.mappingRecords) == 0:
            PrettyOutput.prettyFixException("No mapping records in collection.")


    def filter_portfolio_worthy(self):
        newCollection = MappingRecordCollection()
        newCollection.mappingRecords = self.mappingRecords
        newCollection.mappingRecords = [ record for record in newCollection.mappingRecords if record.portfolioWorthy ]
        return newCollection

    def filter_by_learning_outcomes(self, loCollection):
        newCollection = MappingRecordCollection()
        for m in self.mappingRecords:
            if loCollection.contains(m.learningOutcomeCode):
                newCollection.add(m)
        return newCollection

    def size(self):
        return len(self.mappingRecords)

    def hasPortfolioWorthyValues(self):
        for item in self.mappingRecords:
            if item.portfolioWorthy == True:
                return True
        return False

    def getPortfolioWorthyRowCount(self):
        count =0
        for item in self.mappingRecords:
            if item.portfolioWorthy == True:
                count+=1
        return count

    def checkThatEachMappingRecordHasAMatchedRepository(self, repositoryList):
        """
        Check if the mappingFile repositories are actually in the current submission
        Expects repositoryList to be a list of strings.
        """
        recordsToWarnList = []
        for mr in self.mappingRecords:
            name = os.path.basename(mr.repository)
            nameWithoutDotGit = name
            if name.lower().endswith(".git"):
                nameWithoutDotGit, _ = os.path.splitext(name)
            found = False
            for r in repositoryList:
                if isinstance(r, dict):
                    if r["directory"].endswith(name) or r["directory"].endswith(nameWithoutDotGit):
                        found = True
                else:
                    if r.endswith(name) or r.endswith(nameWithoutDotGit):
                        found = True
            if not found:
                recordsToWarnList.append(mr)        
        return recordsToWarnList

    def matchDirectoriesToRepositories(self, buildDirectory):
        for root, _, _ in os.walk(buildDirectory):
            if isGitRoot(root):
                repo = git.Repo(root)
                repoUrl = repo.remotes.origin.url
                body = os.path.basename(repoUrl)
                bodyWithoutExt, tail = os.path.splitext(body)
                Logger.info("Matched directory {} to remote {} extension: {}".format(root, bodyWithoutExt, tail))
                for m in self.mappingRecords:
                    m_body = os.path.basename(m.repository)
                    m_bodyWithoutExt, _ = os.path.splitext(m_body)
                    if m_bodyWithoutExt == bodyWithoutExt:
                        Logger.info("Matching record {} Repo:{} Repo-name:{} dir-name:{}".format(m.digest, m.repository, bodyWithoutExt, m_bodyWithoutExt))
                        m.repositoryDirectory = root

def createMappingRecordCollection(mappingFile):
    mappingRecordCollection = MappingRecordCollection()
    with open(mappingFile, 'r', encoding="utf8") as inFile:
        data = csv.reader(inFile, delimiter=',', quotechar='"')
        for index, row in enumerate(data):
            if len(row) == 0:
                continue
            row[0] = row[0].strip()
            if row[0][0] == '#':
                continue
            if len(row) != 6:
                raise Exception("expected 6 columns in active rows of mapping file found {} on line {} of {}".format(len(row), index, mappingFile))
            for index, x in enumerate(row):
                x = x.replace("\"", "")
            record = MappingRecord.createFromCsv(row)
            mappingRecordCollection.add(record)
    return mappingRecordCollection

    