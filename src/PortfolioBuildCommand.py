import os
import PortfolioChecks
import MappingRecordCollection
import termcolor
# from PortfolioExtractCommand import PortfolioExtractCommand
import Logger
import PrettyOutput
from ProgramCommand import ProgramCommand
from PortfolioTree import PortfolioTree 
from BuildPlugins.CoversheetChecksPlugin import CoversheetChecksPlugin
from BuildPlugins.PreflightChecksPlugin import PreflightChecksPlugin
from BuildPlugins.CreateCoversheetsPlugin import CreateCoversheetsPlugin
from BuildPlugins.CompressMediaPlugin import CompressMediaPlugin
from BuildPlugins.SingleDirectoryPlugin import SingleDirectoryPlugin
from BuildPlugins.MediaFileConstraintsPlugin import MediaFileConstraintsPlugin
from BuildPlugins.CodeReviewPlugin import CodeReviewPlugin
from BuildPlugins.DotGitDirectoryExistsPlugin import DotGitDirectoryExistsPlugin
from BuildPlugins.GuidanceCheckerPlugin import GuidanceCheckerPlugin
from BuildPlugins.TeamRepositoriesPlugin import TeamRepositoriesPlugin
import ZipStudentPortfolio
from ExtractEntryPoints import runExtractPerStudent
from ProgramArguments import ProgramArguments




# def getAllCommitsInRepositories(config):
#     repositoryDetailsList = []
#     for r in config.repositoryList:
#         Logger.info("Repository: {}".format(r['repository']))
#         authors = Repositories.get_authors_for_repository(config.mappingRecords, r['repository'])
#         if len(authors) == 0:
#             Logger.info("Authors: all")
#         else:
#             Logger.info("Authors: {}".format(','.join(authors)))
#         commits = Repositories.read_in_commits(r['directory'], authors)
#         Logger.info("Commit Count: {}".format(len(commits)))
#         repo = { 
#             "repository": r['repository'],
#             "directory": r['directory'],
#             "authors": authors,
#             "commits": commits
#         }
#         repositoryDetailsList.append(repo)

# def doAllPortfolioChecks(config):
#     result = PortfolioChecks.check_for_git_hidden_directory(config.buildDirectory)
#     if not result:
#         raise RuntimeError("Could not find any git repositories in your build directory - this is weird, have you deleted the .git directories perhaps?  If so, please don't.")
#     for r in result:
#         Logger.info("Found validated git repository: {}".format(r))
    
#     result = PortfolioChecks.check_for_assets_directory(config.assetsDirectory)
#     if not result:
#         raise RuntimeError("Assets directory {} was not found".format(config.assetsDirectory))

#     result = PortfolioChecks.check_repository_list_against_mapping_csv(config.repositoryList, config.mappingRecords)
#     if len(result) > 0:
#         for r in result:
#             Logger.info("Repository {} was mentioned in the mapping records requested for marking, but was not in the list of repositories.".format(r.repository))
#         raise RuntimeError("Mismatch between mapping.csv and repositories.txt, please check these issues and resolve.")

#     failedAssets = PortfolioChecks.check_all_assets_are_less_than_threshold(config.assetsDirectory)
#     if len(failedAssets) > 0:        
#         Logger.info("""
#     You still have large files in your assets file, please correct and then try again.

#     The best solution is to cut down the file to a short 2-5 minute clip that demonstrates
#     what you are trying to exemplify.

#     Do not chop up the file into lots of small pieces and submit them all!
#     """)
#         sys.exit(1)






# def build_portfolio(config:SubmissionConfiguration):
#     """
#     This function is the main dispatch for when the student is building their portfolio.
#     I have factored out most of the functionality to make it a little easier to read and test.
#     """
#     preflightChecks(config)    
#     prepareDirectoryNotFullBuild = createSubmissionDirectoryIfNotExists(config.buildDirectory)
#     createAssetsDirectoryIfNotExists(config.assetsDirectory)
#     checkAndCopyConfigFileIfExists(config)
#     readRepositoriesListAndClone(config)    
#     PortfolioChecks.check_for_mapping_file(config.mappingFilePath, [config.basePath])
#     createCoversheetsForSigning(config)
#     makeCommentsForMarkersTextFileIfNotExists(config.buildDirectory)
#     if prepareDirectoryNotFullBuild:
#         printIntermediateMessage(config)
#         return

#     # compare their work with the template
#     templatePath = Repositories.clonePersonalRepositoryTemplate()
#     Repositories.checkDirectoryForSameFilesAsTemplate(templatePath, config.buildDirectory)

#     checkCoversheets(config)
#     readMappingFileAndFilterList(config)
#     getAllCommitsInRepositories(config)    
#     checkAllMediaFilesToEnsureSizeConstraints(config)
#     doAllPortfolioChecks(config)
#     createZipFileForSubmission(config)
#     checkForAnyMediaFilesAndWarn(config)
#     SingleDirectoryCheck.checkForSingleDirectory(config.buildDirectory)
#     GuidanceChecker.guidanceChecker(config.buildDirectory)
#     printSuccessMessage(config)

#     # automatically go on to do an extraction as most students won't do this and will be
#     # surprised
#     extractor = PortfolioExtractCommand(config)
#     extractor.run()

#     printFinalSuccessMessage(config)


class PortfolioBuildCommand(ProgramCommand):
    def __init__(self, config):
        super().__init__(config)
    
    def match(self, command):
        return command.lower() == "build"

    def _stage1(self):
        preflightChecks = PreflightChecksPlugin(self.config)
        prepareDirectoryNotFullBuild = preflightChecks.start()
        createCoversheetsPlugin = CreateCoversheetsPlugin(self.config)
        createCoversheetsPlugin.start()

        if not prepareDirectoryNotFullBuild:
            self.printIntermediateMessage()
        return prepareDirectoryNotFullBuild

    def printIntermediateMessage(self):
        """
        We arrive here on the first run of the script when the user has not got a submission directory
        created yet.
        """
        msg = """
        The submission directory {} has now been prepared for you.
        Add media assets to the 'assets' directory making sure you crop them to 15MB or less.
        Add coversheets for each module you are submitting for.
        When you are ready, just rerun this application and it will carry out further checks
        and create a zip file you can then upload for each module.

        CHECKLIST:
            Have you checked that you have the MUST HAVEs in the rubric documentation?
            Have you linked the knowledge from your learning outcomes to your projects in your reflections?
            Have you completed all parts of the learning outcomes?
            Where it says "some" do you have provided multiple instances as requested?

        If you have any questions do ask the delivery staff.
        
        """.format(self.config.buildDirectory)
        Logger.info(msg)
        print(termcolor.colored(msg, "green"))


    def _readMappingFileAndFilterList(self):
        mappingRecords = MappingRecordCollection.createMappingRecordCollection(self.config.mappingFilePath)

        if self.config.markPortfolioWorthyOnly:
            if not mappingRecords.hasPortfolioWorthyValues():
                raise RuntimeError("You need to select what work you wish us to mark by using the 'Portfolio Worthy' button in the Evidence list of the portal.")
            mappingRecords = mappingRecords.filter_portfolio_worthy()

        self.config.mappingRecords = mappingRecords
        if self.config.mappingRecords is None:
            PrettyOutput.prettyFixException("There are no mapping records ready for marking in the file {}.".format(self.config.mappingFilePath))

    def _stage2_preconditions(self):
        self._readMappingFileAndFilterList()
        result = PortfolioChecks.check_for_assets_directory(self.config.assetsDirectory)
        if not result:
            PrettyOutput.prettyFixException("Assets directory {} was not found".format(self.config.assetsDirectory))

        result = self.config.mappingRecords.checkThatEachMappingRecordHasAMatchedRepository(self.config.repositoryList)
        if len(result) > 0:
            for r in result:
                Logger.info("Repository {} was mentioned in the mapping records requested for marking, but was not in the list of repositories.".format(r.repository))
            PrettyOutput.prettyFixException("Mismatch between mapping.csv and repositories.txt, please check these issues and resolve.")
        return True

        # failedAssets = PortfolioChecks.check_all_assets_are_less_than_threshold(self.config.assetsDirectory)
        # if len(failedAssets) > 0:        
        #     Logger.info("""
        # You still have large files in your assets file, please correct and then try again.

        # The best solution is to cut down the file to a short 2-5 minute clip that demonstrates
        # what you are trying to exemplify.

        # Do not chop up the file into lots of small pieces and submit them all!
        # """)
        #     return False
        # return True

    def _stage2(self):
        portfolioTree = PortfolioTree(self.config, self.config.buildDirectory)
        portfolioTree.accept(DotGitDirectoryExistsPlugin(self.config))
        portfolioTree.accept(CoversheetChecksPlugin(self.config))
        portfolioTree.accept(CompressMediaPlugin(self.config))
        portfolioTree.accept(SingleDirectoryPlugin(self.config))
        portfolioTree.accept(MediaFileConstraintsPlugin(self.config))
        portfolioTree.accept(CodeReviewPlugin(self.config))
        portfolioTree.accept(GuidanceCheckerPlugin(self.config))
        portfolioTree.accept(TeamRepositoriesPlugin(self.config))
        self._createZipFileForSubmission()
        return True

    def _createZipFileForSubmission(self):
        if os.path.isfile(self.config.zipFileName):
            Logger.info("Removing old zipfile {}".format(self.config.zipFileName))
            os.remove(self.config.zipFileName)
        ZipStudentPortfolio.compress_student_portfolio_into_zip_file(self.config.buildDirectory, self.config.zipFilePrefix)
        if os.path.isfile(self.config.zipFileName):
            Logger.info("Created zip file {}".format(self.config.zipFileName))
        else:
            PrettyOutput.prettyRuntimeException("Unexpected error, could not locate final zip file '{}'".format(self.config.zipFileName))
    


    def _printFinalSuccessMessage(self):
        """
        On successful creation of the zip file and extraction the student can submit.
        """
        msg = """

        Success!
        You now need to upload {} for each module.
        If you have a really bad upload speed problems please do not upload different sets of files
        for each module.
        Yes, these files will be quite large if you have done lots of work.  Make sure you leave
        plenty of time to upload.

        We have done a test extraction to {} for you so you can see what the marker sees. 
        Check this over and make sure everything is there as expected.

        CHECKLIST:
            Have you checked that you have the MUST HAVEs in the rubric documentation?
            Have you linked the knowledge from your learning outcomes to your projects in your reflections?
            Have you completed all parts of the learning outcomes?
            Where it says "some" do you have provided multiple instances as requested?
            
        """.format(self.config.zipFileName, self.config.extractDirectory)
        Logger.info(msg)
        print(termcolor.colored(msg, "green"))

    def _stage3(self):
        if self.config.doExtractionAfterBuild:
            # we want to create this as if we were called from the command line
            programArguments = ProgramArguments.createFromConfig(self.config, allowTestStudent=True)
            programArguments.action = "extract"
            runExtractPerStudent(programArguments)
            
        return True

    def run(self):
        """
        This is split into 3 phases
        1. The first time we set up the portfolio area for the student.
        2. The second we start to check the contents of the portfolio.
        3. The third phase is a test extraction
        """
        if self._stage1():
            if self._stage2_preconditions() and self._stage2():
                if self._stage3():
                    self._printFinalSuccessMessage()