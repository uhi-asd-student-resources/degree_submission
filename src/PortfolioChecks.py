
import os
import shutil
import fnmatch
import re
import Logger

# We have told the students 15MB, I have set it as 20 to give a wee bit of wiggle room
# as the students can sometimes not compress the files to this level.
FILESIZE_MAX_LIMIT = 20 * 1024 * 1024




# def check_for_cover_sheets(buildDirectory):
#     """
#     We want to look for any file with the string "coversheet" or "cover_sheet" in.
#     We are only interested in the top level directory.
#     """
#     expr = "ui\d\d\d\d\d\d_coversheet"
#     result = []
#     for root, dirs, files in os.walk(buildDirectory, topdown=True):
#         for file in files:
#             if re.match(expr, file, re.IGNORECASE):
#                 result.append(os.path.join(root, file))
#     if len(result) == 0:
#         return False
#     return result

def check_for_cover_sheets_that_must_be_signed(buildDirectory):
    expr = "ui\d\d\d\d\d\d_coversheet_must_sign_before_submission.docx"
    result = []
    for root, dirs, files in os.walk(buildDirectory, topdown=True):
        for file in files:
            if re.match(expr, file, re.IGNORECASE):
                result.append(os.path.join(root, file))
    return result

def check_for_cover_sheets_that_have_been_signed(buildDirectory):
    expr = "ui\d\d\d\d\d\d_coversheet_signed.docx"
    result = []
    for root, dirs, files in os.walk(buildDirectory, topdown=True):
        for file in files:
            if re.match(expr, file, re.IGNORECASE):
                result.append(os.path.join(root, file))
    return result


def check_for_git_hidden_directory_aux(subdirectory):
    expr = ".git"
    result = []
    m = fnmatch.translate(expr)
    for root, dirs, files in os.walk(subdirectory, topdown=True):
        for dir in dirs:
            if re.match(m, dir, re.IGNORECASE):
                result.append(os.path.join(root, dir))
                continue
            result += check_for_git_hidden_directory_aux(
                os.path.join(root, dir))
    return list(set(result))


def check_for_git_hidden_directory(buildDirectory):
    result = check_for_git_hidden_directory_aux(buildDirectory)
    cleaned_results = []
    for d in result:
        cleaned_results.append(os.path.dirname(os.path.abspath(d)))
    if len(result) == 0:
        return False
    return cleaned_results


def check_for_assets_directory(assetsDirectory):
    """
    Check this still exists in case user decides to delete it
    """
    return os.path.isdir(assetsDirectory)


def check_for_true_values_in_mapping_file(mappingRecordList: list):
    count = 0
    for item in mappingRecordList:
        if item.portfolioWorthy == True:
            count += 1
    return count


def check_all_assets_are_less_than_threshold(assetsDirectory):
    files_which_are_too_large = []
    for root, dirs, files in os.walk(assetsDirectory, topdown=True):
        for file in files:
            fileSize = os.path.getsize(os.path.join(root, file))
            if fileSize > FILESIZE_MAX_LIMIT:
                files_which_are_too_large.append(os.path.join(root, file))
        for dir in dirs:
            files_which_are_too_large += check_all_assets_are_less_than_threshold(
                os.path.join(root, dir))
    return files_which_are_too_large

# FIX_OR_REMOVE:Not sure this required
# def check_for_learning_outcomes_in_git_commits(mappingRecordList):
#     loCommits = 0
#     for mr in mappingRecordList:
#         for commit in mr.commits:
#             if re.match("LO\d\.\d\.\d\.\d", commit, re.IGNORECASE):
#                 loCommits += 1
#     return loCommits


def check_repository_list_against_mapping_csv(repositoryList, mappingRecordList):
    """
    Check if the mappingFile repositories are actually in the current submission
    """
    recordsToWarnList = []
    for mr in mappingRecordList:
        name = os.path.basename(mr.repository)
        nameWithoutDotGit, _ = os.path.splitext(name)
        found = False
        for r in repositoryList:
            if r['repository'].endswith(name) or r['repository'].endswith(nameWithoutDotGit):
                found = True
        if not found:
            recordsToWarnList.append(mr)        
    return recordsToWarnList


