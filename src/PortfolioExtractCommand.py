from ExtractionPlugins.CitationCounterPlugin import CitationCounterPlugin
from ExtractionPlugins.ReferenceCountPlugin import ReferenceCountPlugin
from ExtractionPlugins.CommitLogCapturePlugin import CommitLogCapturePlugin
from ExtractionPlugins.CoversheetExtractionPlugin import CoversheetExtractionPlugin
from ExtractionPlugins.GrepPlugin import GrepPlugin
from ExtractionPlugins.LearningOutcomeCodeCheckerPlugin import LearningOutcomeCodeCheckerPlugin
from ExtractionPlugins.ObsidianScannerPlugin import ObsidianScannerPlugin
from ExtractionPlugins.SpellCheckerPlugin import SpellCheckerPlugin
from ExtractionPlugins.UrlExtractorPlugin import createUrlExtractor
from ExtractionPlugins.TranscribeMediaPlugin import TranscribeMediaPlugin
from ExtractionPlugins.WordCountPlugin import WordCountPlugin

from PortfolioTree import PortfolioTree
from ExtractEvidenceFromPortfolio import extractEvidenceFromStudentPortfolio
from SubmissionConfiguration import createSubmissionConfigurationFromFile
from UnzipStudentPortfolio import unzip_file
import zipfile
import os
import shutil
import SubmissionConfiguration
import MappingRecordCollection
import git
import Logger
import LearningOutcomesCollection
from DirectoryUtils import isDotGitDirectory, findFileInRepository, extractCopyOfFile, isLearningOutcomeEvidenceDirectory, sha256sum
from ProgramCommand import ProgramCommand
from Utilities import bytes_as_string
import PrettyOutput


def logWithExtractTag(msg):
    Logger.info("[EXTRACT] {}".format(msg))


class PortfolioExtractCommand(ProgramCommand):
    """
    Looks inside the mapping.csv file and uses git to pull
    out the evidence into directories for each learning outcome.
    Creates a new directory called ${student_number}_extract
    """

    def __init__(self, config):
        super().__init__(config)

    def match(self, command):
        return command.lower() == "extract"

    def _filterMappingRecordsThatArePorfolioWorthy(self):
        """
        Get a new mapping file and filter it for portfolio worthy work if appropriate
        Saves the result back to self.config
        """
        mappingRecords = MappingRecordCollection.createMappingRecordCollection(
            self.config.mappingFilePath)
        if self.config.markPortfolioWorthyOnly:
            filteredMappingRecords = mappingRecords.filter_portfolio_worthy()
            if filteredMappingRecords.size() == 0:
                # this is a fatal error unless we have allowed this
                if self.config.markPortfolioWorthyOnly:
                    PrettyOutput.prettyRuntimeException(
                        "No work was marked as portfolio worthy, there is nothing that the student has marked good enough to be marked.  The student should check that they have used the portal tool in the appropriate way to mark evidence as being 'Portfolio Worthy'. This is a fatal error as this is an immediate F.")
                else:
                    PrettyOutput.prettyWarning(
                        "No work was marked as portfolio worthy, there is nothing that the student has marked good enough to be marked.  The student should check that they have used the portal tool in the appropriate way to mark evidence as being 'Portfolio Worthy'. Continuing to extract.")
            self.config.mappingRecords = filteredMappingRecords
        else:
            self.config.mappingRecords = mappingRecords
        logWithExtractTag("{} records have been extracted from the mapping.csv file".format(
            self.config.mappingRecords.size()))

    def _filterMappingRecordsThatMatchLearningOutcomes(self):
        filteredMappingRecords = self.config.mappingRecords.filter_by_learning_outcomes(
            self.config.relevantLearningOutcomes)
        if filteredMappingRecords.size() == 0:
            # there is a situation where the first year has put the year as 1 and the module as 1 in Semester 1.
            # if there are no learning outcomes then we try setting the module to 6.
            oldModuleList = self.config.moduleList
            self.config.moduleList = [6]
            self.config.filterYearAndModuleLists()
            newMappingRecords = MappingRecordCollection.createMappingRecordCollection(
                self.config.mappingFilePath)
            filteredMappingRecords = newMappingRecords.filter_portfolio_worthy()
            filteredMappingRecords = filteredMappingRecords.filter_by_learning_outcomes(
                self.config.relevantLearningOutcomes)
            if filteredMappingRecords.size() > 0:
                PrettyOutput.prettyWarning(
                    "Detected no evidence for combination (yr=1,module=1). Tested to see if student meant (yr=1,module=6) and found work. Continuing as if the student intended this combination.")
                self.config.relevantModuleCollection.log()
                self.config.relevantLearningOutcomes.log()
            else:
                # replace as it was originally and throw an exception
                self.config.moduleList = oldModuleList
                self.config.filterYearAndModuleLists()
                warning = "No work was marked in the mapping.csv file for relevant learning outcomes"
                PrettyOutput.prettyFixException(warning)
        self.config.mappingRecords = filteredMappingRecords
        logWithExtractTag("{} records have been extracted from the mapping.csv file that match the year and modules specified in submission_configuration file.".format(
            self.config.mappingRecords.size()))

    def _makeExtractionDirectory(self):
        if os.path.isdir(self.config.extractDirectory):
            logWithExtractTag("Removing existing extraction directory: {}".format(
                self.config.extractDirectory))
            shutil.rmtree(self.config.extractDirectory)
        logWithExtractTag("Creating extraction directory: {}".format(
            self.config.extractDirectory))
        os.mkdir(self.config.extractDirectory)

    def _copyAssetsDirectoryFromPortfolioToExtractionDirectory(self):
        # we just copy the whole assets directory as we assume that this will be referred
        # to in the learning outcome files somewhere...
        if os.path.isdir(self.config.assetsDirectory):
            extractedAssetsDirectory = os.path.join(
                self.config.extractDirectory, SubmissionConfiguration.DEFAULT_ASSETS_DIRECTORY_NAME)
            shutil.copytree(self.config.assetsDirectory,
                            extractedAssetsDirectory)
        else:
            PrettyOutput.prettyWarning(
                "Assets directory was not found in extracted portfolio, please confirm before starting marking.")

    def _makeCoversheetsExtractionDirectory(self):
        # copy the CoverSheets in first
        self.coversheetDirectory = os.path.join(
            self.config.extractDirectory, "coversheets")
        if not os.path.isdir(self.coversheetDirectory):
            logWithExtractTag("Created coversheets extraction directory: {}".format(
                self.coversheetDirectory))
            os.mkdir(self.coversheetDirectory)

    def _makeCommitLogExtractionDirectory(self):
        # dump commit logs from all repositories
        commitLogPath = os.path.join(self.config.extractDirectory, "commits")
        if not os.path.isdir(commitLogPath):
            os.mkdir(commitLogPath)

    def _dumpFileListingOfPortfolioToFile(self):
        # dump all files from the repository for reference, exclude .git directories
        with open(os.path.join(self.config.extractDirectory, "files_list.txt"), "w", encoding="utf8") as fileLogOut:
            for root, dirs, files in os.walk(self.config.buildDirectory):
                if isDotGitDirectory(root):
                    continue
                fileLogOut.write("[root] {}\n".format(root))
                for d in dirs:
                    fileLogOut.write("[dir ] {}\n".format(d))
                for f in files:
                    fileLogOut.write("[file] {}\n".format(f))

    def _saveMappingFromExtractedFilesToPortfolioToFile(self):
        saveFilePath = os.path.join(
            self.config.extractDirectory, "extractedFileMapping.txt")
        with open(saveFilePath, "w") as outFile:
            for k in self.config.mapExtractedFilesToOriginalFiles.keys():
                dest = self.config.mapExtractedFilesToOriginalFiles[k]
                outFile.write("{} => {}\n".format(k, dest))

        saveFilePath = os.path.join(
            self.config.extractDirectory, "redirectedFileMapping.txt")
        with open(saveFilePath, "w") as outFile:
            for k in self.config.redirectedFiles.keys():
                dest = self.config.redirectedFiles[k]
                outFile.write("{} => {}\n".format(k, dest))

    def _welcome(self):
        PrettyOutput.prettyInfo("Starting portfolio extraction\nGenerating student extract in {}".format(
            self.config.extractDirectory))

    def _stage1_unzip(self):
        assert(self.config.studentNumber is not None)
        assert(self.config.buildDirectory is not None)

        # if coming from the student build phase then we will have a full config with buildDirectory and student number
        # if we are coming in at extraction phase then we will have an expected buildDirectory based on the student number/zipFileName and current directory
        if os.path.isdir(self.config.buildDirectory):
            portfolioPath = self.config.buildDirectory
        else:
            directoryToExtractZipFileInto = os.path.dirname(
                os.path.abspath(self.config.buildDirectory))
            assert(os.path.isdir(directoryToExtractZipFileInto))
            portfolioPath = unzip_file(
                self.config.zipFileName, directoryToExtractZipFileInto, self.config.studentNumber)

        # here we want to read in the submission directory file as we will only have
        # our extraction information.
        oldConfig = self.config
        configFilePath = os.path.join(
            portfolioPath, "submission_configuration.json")
        self.config = createSubmissionConfigurationFromFile(configFilePath)

        # TODO(tm) extract this and make a comparison function between 2 configuration files and also to see if a configuration is valid
        if oldConfig.studentNumber != self.config.studentNumber:
            PrettyOutput.prettyFixException(
                "Student number in the configuration file did not match the student number given.")
        if not os.path.isdir(self.config.buildDirectory):
            PrettyOutput.prettyFixException(
                "No buildDirectory found at {}.".format(self.config.buildDirectory))
        if not os.path.isfile(self.config.mappingFilePath):
            PrettyOutput.prettyFixException(
                "No mapping.csv file found at {}.".format(self.config.mappingFilePath))

    def _stage2_preparation(self):
        self.config.readRepositories()
        self._filterMappingRecordsThatArePorfolioWorthy()
        self._filterMappingRecordsThatMatchLearningOutcomes()
        self.config.mappingRecords.throwIfEmpty()
        self.config.mappingRecords.matchDirectoriesToRepositories(
            self.config.buildDirectory)
        self.config.mappingRecords.checkThatEachMappingRecordHasAMatchedRepository(
            self.config.repositoryList)
        self._makeExtractionDirectory()

    def _stage3_extract(self):
        self._copyAssetsDirectoryFromPortfolioToExtractionDirectory()
        self._makeCoversheetsExtractionDirectory()
        self._makeCommitLogExtractionDirectory()
        self._dumpFileListingOfPortfolioToFile()
        extractEvidenceFromStudentPortfolio(self.config)
        self._saveMappingFromExtractedFilesToPortfolioToFile()

    def _stage4_processPortfolioFiles(self):
        assert(self.config.extractDirectory is not None)
        logWithExtractTag(self.config.extractDirectory)
        pt = PortfolioTree(self.config, self.config.buildDirectory)
        pt.accept(CoversheetExtractionPlugin(
            self.config, self.coversheetDirectory))
        pt.accept(CommitLogCapturePlugin(self.config))
        pt.accept(GrepPlugin(self.config))

    def _stage5_processExtractedFiles(self):
        pt = PortfolioTree(self.config, self.config.extractDirectory)
        pt.accept(CitationCounterPlugin(self.config))
        pt.accept(ReferenceCountPlugin(self.config))
        pt.accept(LearningOutcomeCodeCheckerPlugin(self.config))
        pt.accept(SpellCheckerPlugin(self.config))
        pt.accept(ObsidianScannerPlugin(self.config))
        pt.accept(TranscribeMediaPlugin(
            self.config, self.config.doTranscriptions))
        urlExtractor = createUrlExtractor(self.config)
        pt.accept(WordCountPlugin(self.config))
        pt.accept(urlExtractor)

    def _finished(self):
        PrettyOutput.prettyInfo(
            "Extraction is complete. Check over any warnings and make sure you are happy with the work that has been extracted.  Any issues or queries, please contact the delivery staff immediately.")

    def run(self):
        self._welcome()
        logWithExtractTag("Starting Stage 1")
        self._stage1_unzip()
        logWithExtractTag("Starting Stage 2")
        self._stage2_preparation()
        logWithExtractTag("Starting Stage 3")
        self._stage3_extract()
        logWithExtractTag("Starting Stage 4")
        self._stage4_processPortfolioFiles()
        logWithExtractTag("Starting Stage 5")
        self._stage5_processExtractedFiles()
        logWithExtractTag("Completed.")
        self._finished()
