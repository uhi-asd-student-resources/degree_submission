from DirectoryUtils import isDotGitDirectory
import os

class PortfolioTree:
    """
    Creates a list of all the paths in the students extraction directory or portfolio
    We can then run a particular plugin function over each.
    We don't want to run over any files that we create so its important this is done
    at the start just after the files in the mapping.csv have been extracted from the various repositories.
    """

    def __init__(self, config, directory):
        self.config = config
        self.directory = directory
        self.filesAndDirectories = []
        self.create()

    def create(self):
        if not self.directory:
            raise ValueError("create: directory is {}".format(self.directory))
            
        for root, _, files in os.walk(self.directory):
            if isDotGitDirectory(root):
                continue
            # while this should be adding all unique paths for some 
            # reason there is duplication so I put in checks and we 
            # only add paths for files and directories once.
            if root not in self.filesAndDirectories:
                self.filesAndDirectories.append(root)
            for fileName in files:
                path = os.path.join(root, fileName)
                if path not in self.filesAndDirectories:
                    self.filesAndDirectories.append(path)

    def accept(self, visitor):
        if visitor.start():
            for path in self.filesAndDirectories:
                if os.path.isdir(path):
                    if visitor.matchDirectory(path):
                        if not visitor.run(path):
                            break
                elif os.path.isfile(path):
                    _, ext = os.path.splitext(path)
                    if visitor.matchFile(path, ext):
                        if not visitor.run(path):
                            break
            # The visitor is expected to throw an exception if they do 
            # not want us to continue after finish is called.
            visitor.finish()

