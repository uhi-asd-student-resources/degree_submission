"""
Abstact class for a portfolio tree visitor
"""
class PortfolioTreeVisitor:

    def __init__(self,config):
        self.config = config

    def matchDirectory(self, path):
        return False

    def matchFile(self, path, ext):
        return False

    def start(self):
        """
        Called before the PorfolioTree starts its inner for loop
        Return False to stop it from proceeding, run nor finish will be called
        """
        return True

    def run(self, path):
        """
        Called for each matching file or directory
        If we return false then we don't continue.
        """
        return True

    def finish(self):
        """
        Called at the very end of the tree, throw an exception using PrettyOutput if you do not
        want to continue.
        """
        pass
