import termcolor
import Logger

"""
We repeat the guidance again at the end of the program so that
the student does not miss the screen moving on.
"""
GUIDANCE_LOG = []
G_INTERPRET_ERRORS_AS_WARNINGS = False

def prettyFixException(stuff, saveMessage=False, writeToLog=True):
  if writeToLog:
    Logger.error(stuff)
  msg = termcolor.colored("\n\nThere is a problem with your portfolio that must be fixed:\n\n\t", "red")
  msg += termcolor.colored("{}".format(stuff), "cyan")
  msg += termcolor.colored("\n\nIf you are unsure of what to do to correct it, check with the delivery staff.", "red")
  msg += "\n\n"

  global G_INTERPRET_ERRORS_AS_WARNINGS
  if G_INTERPRET_ERRORS_AS_WARNINGS:
    print(msg)
  else:
    raise ValueError(msg)

def prettyWarning(stuff, saveMessage=False, writeToLog=True):
  global GUIDANCE_LOG
  if saveMessage:
    GUIDANCE_LOG.append( { "errorLevel": "warning", "message": stuff } )
  if writeToLog:
    Logger.warning(stuff)  
  
  msg = termcolor.colored("\n\nThe following warning has been raised:\n\n\t", "red")
  msg += termcolor.colored("{}".format(stuff), "cyan")
  msg += termcolor.colored("\n\nThis may be a false alarm, but you might want to address it before you submit.", "red")
  msg += "\n\n"
  print(msg)

def prettyValueException(stuff, saveMessage=False):
  Logger.error(stuff)
  msg = termcolor.colored("\n\nThis exception has been raised by the submission application:\n\n\t", "red")
  msg += termcolor.colored("{}".format(stuff), "cyan")
  msg += "\n\n"

  global G_INTERPRET_ERRORS_AS_WARNINGS
  if G_INTERPRET_ERRORS_AS_WARNINGS:
    print(msg)
  else:
    raise ValueError(msg)

def prettyRuntimeException(stuff, saveMessage=False):
  Logger.error(stuff)
  msg = termcolor.colored("\n\nThis exception has been raised by the submission application:\n\n\t", "red")
  msg += termcolor.colored("{}".format(stuff), "cyan")
  msg += "\n\n"

  global G_INTERPRET_ERRORS_AS_WARNINGS
  if G_INTERPRET_ERRORS_AS_WARNINGS:
    print(msg)
  else:
    raise RuntimeError(msg)

def prettyInfo(stuff, saveMessage=False, writeToLog=True):
  global GUIDANCE_LOG
  if saveMessage:
    GUIDANCE_LOG.append( { "errorLevel": "warning", "message": stuff } )
  if writeToLog:
    Logger.info(stuff)
  msg = termcolor.colored("{}".format(stuff), "green")
  print(msg)

def prettyGuidance(stuff, saveMessage=False, writeToLog=True):
  global GUIDANCE_LOG
  if saveMessage:
    GUIDANCE_LOG.append( { "errorLevel": "warning", "message": stuff } )
  if writeToLog:
    Logger.warning(stuff)
  msg = termcolor.colored("\n\nThe following guidance has been offered:\n\n\t", "blue")
  msg += termcolor.colored("{}".format(stuff), "yellow")
  msg += termcolor.colored("\n\nThis may be a false alarm, but you might want to address it before you submit.", "blue")
  msg += "\n\n"
  print(msg)
  
def showGuidance(level, message, saveMessage=True, writeToLog=True):
  global GUIDANCE_LOG
  
  if level == "guidance":
    prettyGuidance(message, saveMessage = saveMessage, writeToLog=writeToLog)
  elif level == "info":
    prettyInfo(message, saveMessage = saveMessage, writeToLog=writeToLog)
  elif level == "error":
    prettyFixException(message, saveMessage = saveMessage, writeToLog=writeToLog)
  elif level == "warning":
    prettyWarning(message, saveMessage = saveMessage, writeToLog=writeToLog)
  else:
    msg = "invalid guidance level '{}'".format(level)
    global G_INTERPRET_ERRORS_AS_WARNINGS
    if G_INTERPRET_ERRORS_AS_WARNINGS:
      print(msg)
    else:
      raise RuntimeError(msg)

def onProgramExit(configuration):
  global GUIDANCE_LOG
  if len(GUIDANCE_LOG) == 0:
    print("\n\nGood job! There are no guidance, warning or error messages to report.")
  else:
    print("\n\nYou have some guidance, warning or error messages that you should check and decide if you wish to address.")
  print("If you cannot scroll back you can also check the log file: {}".format(configuration.LoggerFilePath))