import datetime
from Constants import *
import Logger
import PrettyOutput
import os
import sys
import termcolor
from Constants import GLOBAL_PROGRAM_NAME

class ProgramArguments:
    def __init__(self):
        self.appName = "The BSc Applied Software Development Degree Submission Tool"
        self.nowTimestamp = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
        self.action = None                           # either extract, compress or build
        self.ignoreErrors=[]                        # which checks do we want to ignore
        self.markPortfolioWorthyOnly = True          # should we use all evidence in mapping.csv or just that which is marked as worthy
        self.configFile = DEFAULT_SUBMISSION_CONFIG_FILENAME
        self.compressMediaFiles = True               # compress all media files
        self.portfolioPath = None                    # the path to the portfolio
        self.zipPath = None                          # the path and filename to the student's zip file
        self.studentNumber = None                    # the students matriculation number
        self.unzipPath = None                        # where you want the zip file unzipped to
        self.allowTestStudent = False                # allow the test student 123456789 to be used, useful for tests
        self.extractPath = None                      # where do we want to extract the portfolio to
        self.today = datetime.date.today()
        self.skipTranscription = False
        self.createAllCoversheets = False
        self.classZipFile = None

        # This is required as some students have forgotten to say which modules they are submitting for.  
        # In the future make default the "Principles of Programming" module and then hopefully it will be
        # obvious to them to change this.
        self.enforceModuleList=None
        self.logFilePath = None                        
    
    def checkZipFile(self):
        if self.zipPath:
            _, ext = os.path.splitext(self.zipPath)
            if not ext:
                Logger.info("Zip file '{}' missing .zip extension, adding.".format(self.zipPath))
                self.zipPath = self.zipPath + ".zip"
                if not os.path.isfile(self.zipPath):
                    raise RuntimeError("Zip file '{}' was not found".format(self.zipPath))

    def setAction(self, action):
        if action == "build" or action == "extract" or action == "extractclass":
            self.action = action
        else:
            PrettyOutput.prettyValueException("'{}' is an invalid value for action.  Required one from {build, extract, extractlass}.".format(action))

    def setLogFileName(self):
        if self.action == "extractclass":
            # we are going to be extracting the class so we want to use this one.
            self.logFilePath = os.path.basename(self.classZipFile) + self.nowTimestamp + ".log"
        elif self.studentNumber:
            # if a student number exists we are probably batch processing so we want to 
            # ensure each log is unique.
            self.logFilePath = self.studentNumber+"_submission_"+self.action+"_"+self.nowTimestamp+".log"
        else:
            self.logFilePath = "submission_"+self.action+"_"+self.nowTimestamp+".log"
    
    def printToLog(self):
        # for auditing purposes we will output a log of what we did
        Logger.info("Program Start Date: {}".format(self.today.strftime("%d/%m/%Y")))
        Logger.info("Option - Action: {}".format(self.action))
        Logger.info("Option - Configuration File: {}".format(self.configFile))
        Logger.info("Option - Filter for Portfolio Worthy: {}".format(self.markPortfolioWorthyOnly))
        Logger.info("Option - Compress media: {}".format(self.compressMediaFiles))
        
        if self.classZipFile is not None:
            Logger.info("Option - Class zip file: {}".format(self.classZipFile))

        if self.action == "extract":
            # Extraction can happen by giving a zip file or pointing to a portfolio directory
            Logger.info("Option - Student Number: {}".format(self.studentNumber))
            Logger.info("Option - Zip Path: {}".format(self.zipPath))
            Logger.info("Option - Unzip Path: {}".format(self.unzipPath))
            Logger.info("Option - Portfolio Path: {}".format(self.portfolioPath))
            Logger.info("Option - Extract Path: {}".format(self.extractPath))

    @staticmethod
    def showHelp():
        msg = termcolor.colored("\nWelcome to the UHI Applied Software Development Degree Submission tool\n\n", "blue")
        msg += "You can use this tool to build your portfolio for submission.\n"
        msg += "You can also use this tool to check for common errors and antipatterns in your evidence.\n"
        msg += "You can also view the same output as the markers will see, this will help you"
        msg += "ensure that everything you want assessed can be found efficiently.\n"
        msg += "The extract process is carried out immediately after a successful build for the students benefit."
        msg += "\n\n> Options\n"
        msg += " --help\t\tThis help screen.\n"
        msg += "\n"
        msg += "> Build Options\n"
        msg += "  On first run, creates initial portfolio. On following runs it will try to build an updated portfolio.\n"
        msg += " --build\t\tSelect build mode, added when running build_submission.(bat|sh) script.\n"
        msg += " --no-compress\t\tDo not compress all video and audio in your portfolio (automatically done during build).\n"
        msg += " --enforce-module\t\tBackward compatibility if module not given in configuration file.\n"
        msg += " --config\t\tUse a particular configuration file.\n"
        msg += "\n"
        msg += "> Extract Options\n"
        msg += "  Extracts the students work into a directory structure more conducive to marking.\n"
        msg += " --extract\t\tSelect extract mode, added when running extract_submission.(bat|sh) script.\n"
        msg += " --zip-file\t\tSpecify which zip file to uncompress.\n"
        msg += " --student-number\tThe student number that is associated with the zip file.\n"
        msg += " --ignore-portfolio-worthy\t\tDo not filter mapping records on portfolio worth flag.\n"
        msg += " --portfolio-path\t\tWhere the portfolio path is located.\n"
        msg += " --extract-to\t\tWhere the portfolio will be extracted to.\n"
        msg += " --skip-transcriptions\t\tSkip transcriptions.\n"
        msg += "\n"
        msg += "> Brightspace Options\n"
        msg += " --extractclass\t\tExtracts a class zip file from Brightspace, for use by markers.\n"
        msg += " --class-zip[-file]\t\tZip file to extract. Zip file can hold one or more students submissions.\n"
        msg += "\n"
        msg += "> Development and Testing Options\n"
        msg += " --test\t\t\tAllow the tool to run with the test configuration.\n"
        msg += "\n"
        msg += "Any problems with the tool please contact @staff in #tool-issues in Slack."
        print(msg)
        sys.exit(0)

    @staticmethod
    def createFromExisting(pA):
        programArguments = ProgramArguments()
        programArguments.appName = pA.appName
        programArguments.nowTimestamp = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
        programArguments.action = pA.action
        programArguments.ignoreErrors= pA.ignoreErrors
        programArguments.markPortfolioWorthyOnly = pA.markPortfolioWorthyOnly
        programArguments.configFile = pA.configFile
        programArguments.compressMediaFiles = pA.compressMediaFiles
        programArguments.portfolioPath = pA.portfolioPath
        programArguments.zipPath = pA.zipPath
        programArguments.studentNumber = pA.studentNumber
        programArguments.unzipPath = pA.unzipPath
        programArguments.allowTestStudent = pA.allowTestStudent
        programArguments.extractPath = pA.extractPath
        programArguments.today = datetime.date.today()
        programArguments.skipTranscription = pA.skipTranscription
        programArguments.createAllCoversheets = pA.createAllCoversheets
        programArguments.classZipFile = pA.classZipFile
        programArguments.enforceModuleList=pA.enforceModuleList
        programArguments.logFilePath = pA.logFilePath
        return programArguments

    @staticmethod
    def createFromConfig(config, allowTestStudent=True):
        programArguments = ProgramArguments()
        programArguments.nowTimestamp = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
        programArguments.action = config.action
        programArguments.ignoreErrors= config.ignoreErrors
        programArguments.markPortfolioWorthyOnly = config.markPortfolioWorthyOnly
        programArguments.configFile = config.configFilePath
        programArguments.compressMediaFiles = config.compressMediaFiles
        programArguments.portfolioPath = config.buildDirectory
        programArguments.zipPath = config.zipFileName
        programArguments.studentNumber = config.studentNumber
        programArguments.unzipPath = config.buildDirectory
        programArguments.allowTestStudent = allowTestStudent
        programArguments.extractPath = config.extractDirectory
        programArguments.today = datetime.date.today()
        programArguments.skipTranscription = not config.doTranscriptions
        programArguments.createAllCoversheets = False
        programArguments.classZipFile = None
        programArguments.enforceModuleList=True
        programArguments.setLogFileName()
        return programArguments

    @staticmethod
    def createFromScriptArguments(version):
        programArguments = ProgramArguments()

        if len(sys.argv) == 1:
            # if there are no arguments then we show the help
            ProgramArguments.showHelp()

        # try our best to parse the arguments
        argumentIndex = 1
        while argumentIndex < len(sys.argv):
            if sys.argv[argumentIndex] == "--help" or sys.argv[argumentIndex] == "-h":
                ProgramArguments.showHelp()
            elif sys.argv[argumentIndex] == "--version" or sys.argv[argumentIndex] == "-V":
                print("UHI Degree Submission Tool Version {}".format(version))
                sys.exit(0)
            elif sys.argv[argumentIndex] == "--class-zip-file" or sys.argv[argumentIndex] == "--class-zip":
                programArguments.classZipFile = sys.argv[argumentIndex+1]
                argumentIndex += 1
            elif sys.argv[argumentIndex] == "--enforce-module":
                programArguments.enforceModuleList = sys.argv[argumentIndex+1]
                programArguments.enforceModuleList = map(int, programArguments.enforceModuleList.split(","))
                argumentIndex += 1
            if sys.argv[argumentIndex] == "--build":
                programArguments.action = "build"
            elif sys.argv[argumentIndex] == "--extract":
                programArguments.action = "extract"
            elif sys.argv[argumentIndex] == "--extractclass":
                programArguments.action = "extractclass"
                if not sys.argv[argumentIndex+1].startswith("--"):
                    programArguments.classZipFile = sys.argv[argumentIndex+1]
                    argumentIndex+=1
            elif sys.argv[argumentIndex] == "--ignore-portfolio-worthy":
                programArguments.markPortfolioWorthyOnly = False
            elif sys.argv[argumentIndex] == "--config":
                programArguments.configFile = sys.argv[argumentIndex+1]
                argumentIndex += 1
            elif sys.argv[argumentIndex] == "--no-compress":
                programArguments.compressMediaFiles = False
            elif sys.argv[argumentIndex] == "--portfolio-path":
                programArguments.portfolioPath = sys.argv[argumentIndex+1]
                argumentIndex += 1
            elif sys.argv[argumentIndex] == "--student-number" or sys.argv[argumentIndex] == "--student":
                programArguments.studentNumber = sys.argv[argumentIndex+1]
                argumentIndex += 1
            elif sys.argv[argumentIndex] == "--zip-path" or sys.argv[argumentIndex] == "--zip-file" or sys.argv[argumentIndex] == "--zip":
                # this is a common typo so we make it also do the same as zip-path
                programArguments.zipPath = sys.argv[argumentIndex+1]
                programArguments.checkZipFile()
                argumentIndex += 1
            elif sys.argv[argumentIndex] == "--unzip-path":
                programArguments.portfolioPath = sys.argv[argumentIndex+1]
                argumentIndex += 1
            elif sys.argv[argumentIndex] == "--test":
                programArguments.allowTestStudent = True
            elif sys.argv[argumentIndex] == "--extract-to":
                programArguments.extractPath = sys.argv[argumentIndex+1]
                argumentIndex += 1
            elif sys.argv[argumentIndex] == "--skip-transcription" or sys.argv[argumentIndex] == "--skip-transcriptions":
                programArguments.skipTranscription = True
            else:
                raise ValueError(termcolor.colored("Thanks for using {}. {} is an invalid argument. Use --help or -h to get options".format(GLOBAL_PROGRAM_NAME,sys.argv[argumentIndex]), "red"))
            argumentIndex += 1

        programArguments.setLogFileName()

        return programArguments


