import os
import git
import re
import Logger
import DirectoryUtils
import PrettyOutput
import time

GIT_PERSONAL_REPOSITORY_TEMPLATE = "git@bitbucket.org:uhi-asd-student-resources/personal_repository_2021.git"

def clonePersonalRepositoryTemplate(dirPath=None):
    repository = GIT_PERSONAL_REPOSITORY_TEMPLATE
    if not dirPath:
        dirPath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "personal_repository_template")
    if os.path.isdir(dirPath):
        repo = git.Repo(dirPath)
        origin = repo.remotes.origin
        origin.pull()
    else:
        git.Repo.clone_from(repository, dirPath)
    return dirPath

def checkDirectoryForSameFilesAsTemplate(templatePath, buildDirectory):
    for root, _, files in os.walk(templatePath):
        if DirectoryUtils.isDotGitDirectory(root):
            continue        
        for f in files:
            for buildRoot, _, buildFiles in os.walk(buildDirectory):
                if DirectoryUtils.isDotGitDirectory(buildRoot):
                    continue
                for bf in buildFiles:
                    if bf == ".gitignore":
                        continue
                    if bf == f:
                        if os.path.getsize(os.path.join(buildRoot,bf)) == os.path.getsize(os.path.join(root,f)):
                            fullPath = os.path.join(buildRoot, f)
                            warningText = None
                            if "mentor" in bf.lower() or "mentee" in bf.lower():
                                warningText = "If you have not taken part in the Mentor programme then you should delete {}.".format(fullPath)
                            elif "readme.md" in bf.lower():
                                warningText = "You should have updated the README file with your own information - {}.".format(fullPath)
                            else:
                                warningText = "Detected potential template file exists in your portfolio. You should have deleted {} and/or replaced it with your own.".format(fullPath)
                            if warningText:
                                PrettyOutput.prettyWarning(warningText)
            

def read_repositories_from_file(repoFile):
    Logger.info("Reading repositories from {}".format(repoFile))
    repositoryList = []
    if not os.path.isfile(repoFile):
        PrettyOutput.prettyFixException("The file {} does not exist in your portfolio.".format(repoFile))
    with open(repoFile, "r", encoding="utf8") as inFile:
        lines = inFile.readlines()
    for line in lines:
        line = line.strip()
        if len(line) > 0 and line[0] != '#':
            repositoryList.append(line)
            Logger.info("Registered repository: {}".format(line))
    if len(repositoryList) == 0:
        PrettyOutput.prettyFixException("No repositories found in {}".format(repoFile))
    return repositoryList


def clone_repository_list(repositoryList, buildDirectory):
    """
    Clones repositories throw errors when they occur.
    Private repositories should work if the SSH key is loaded in the ssh-agent.
    """
    Logger.info("""

    Attempting to clone/pull repositories if you are asked for 
    credentials and you are expecting a key to be used then press CTRL+C 
    and check the ssh key is loaded using 'ssh-add -l'.
    If you are expecting to be asked for credentials then please enter
    them.

            """)
    repositoryResults = []
    repositoryList = list(set(repositoryList))
    for repository in repositoryList:
        downloadLocation = os.path.basename(repository)
        downloadLocation2 = None
        if downloadLocation.endswith(".git"):
            downloadLocation2 = downloadLocation[:(len(downloadLocation)-4)]
            Logger.info("Alternative download location: {}".format(downloadLocation2))
        if downloadLocation2:
            repoDirectory2 = os.path.join(buildDirectory, downloadLocation2)
            if os.path.isdir(repoDirectory2):
                downloadLocation = downloadLocation2
                
        repoDirectory = os.path.join(buildDirectory, downloadLocation)
        if repository.startswith("https://") and "@" not in repository:
            repository = re.sub("(https://)(\w)", r"\1:@\2", repository)
            Logger.info("Refactored repository path: {}".format(repository))

        try:
            if not os.path.isdir(repoDirectory):
                Logger.info("Cloning {} to {}".format(repository,repoDirectory))
                git.Repo.clone_from(repository, repoDirectory)
                repoItem = {
                    "repository": repository,
                    "directory": repoDirectory
                }
            else:
                Logger.info("Fetching {}".format(repository))
                repo = git.Repo(repoDirectory)
                origin = repo.remotes.origin
                origin.pull()
                repoItem = {
                    "repository": repository,
                    "directory": repoDirectory
                }
            repositoryResults.append(repoItem)
        except Exception as ex:
            errorText = "ERROR: {}".format(str(ex))
            errorText = errorText + " You received an error pulling repository {} to {}.  If this is empty, then comment it out of your repositories.txt to stop getting this error message.".format(repository, repoDirectory)
            errorText = errorText + " If the repository is NOT empty, then check the url is correct, if it is, then contact a staff member."
            PrettyOutput.prettyRuntimeException(errorText)
        # Added this when we seemed to be timing out when downloading multiple bitbucket repos
        # time.sleep(10)
    return repositoryResults

def read_in_commits(localRepositoryLocation, authors=[]):
    repo = git.Repo(localRepositoryLocation)
    logs = []
    try: 
        logs = list(repo.iter_commits())
        if len(authors) > 0:
            result = []
            for log in logs:
                # search for a matching author and break on first so we 
                # won't get duplicates if multiple authors
                for a in authors:
                    if str(log.author).upper() == a.upper():
                        result.append(log)
                        break
            return result
    except Exception as ex:
        #Logger.error(traceback.format_exc())
        if str(ex) != "Reference at 'refs/heads/master' does not exist":
            raise
    return logs

def write_commits(commits, outputFile):
    """
    Write the information we want to check to a csv compatible with excel
    """
    with open(outputFile, "w", encoding="utf8") as outFile:
        for commit in commits:
            multilineString = commit.message.strip()
            multilineString = multilineString.replace("\n", "\r\f")
            outFile.write("\"{}\",\"{}\",\"{}\",\"{}\"\n".format(commit.committed_datetime, commit.author, commit.hexsha, multilineString))
    
def is_team_project(localRepositoryLocation, authors = []):
    if len(authors) == 0:
        PrettyOutput.prettyRuntimeException("No authors passed to is_team_project, check with delivery staff.")
    authors.append("TOM MCCALLUM") # remove delivery staff
    repo = git.Repo(localRepositoryLocation)
    count = 0
    total_records = 0
    other_authors = []
    try: 
        logs = list(repo.iter_commits())
        for log in logs:
            total_records += 1
            normalised_author = str(log.author).upper()
            # search for a matching author and break on first so we 
            # won't get duplicates if multiple authors
            isSelf = False
            for a in authors:
                if normalised_author == a.upper():
                    isSelf = True
            if not isSelf:
                count += 1
                if not normalised_author in other_authors:
                    other_authors.append(normalised_author)
    except Exception as ex:
        #Logger.error(traceback.format_exc())
        if str(ex) != "Reference at 'refs/heads/master' does not exist":
            raise
    if len(other_authors) == 1:
        return False
    if total_records > 0:
        if count / total_records < 0.1:     # if less than 10% of work is done by others then its effectively a solo project
            return False
        else:
            return True
    else:
        return False

def get_authors_for_repository(mappingRecords, repository):
    authors = []
    for m in mappingRecords:
        if m.repository == repository:
            authors.append(m.author)
    return authors

def matchRepositoryToLocalDirectory(repository, buildDirectory):
        downloadLocation = os.path.basename(repository)
        downloadLocation2 = None
        if downloadLocation.endswith(".git"):
            downloadLocation2 = downloadLocation[:(len(downloadLocation)-4)]
            if downloadLocation2:
                repoDirectory2 = os.path.join(
                    buildDirectory, downloadLocation2)
                if os.path.isdir(repoDirectory2):
                    downloadLocation = downloadLocation2

        repoDirectory = os.path.join(buildDirectory, downloadLocation)
        if repository.startswith("https://") and "@" not in repository:
            repository = re.sub("(https://)(\w)", r"\1:@\2", repository)

        if not os.path.isdir(repoDirectory):
            PrettyOutput.prettyRuntimeException(
                "Failed to find expected repository: {}".format(repoDirectory))
        else:
            repoItem = {
                "repository": repository,
                "directory": repoDirectory
            }
            return repoItem
        return None
