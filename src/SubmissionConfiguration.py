import os
import json
from LearningOutcomesCollection import LearningOutcomesCollection, createDefaultLearningOutcomesCollection
from Repositories import read_repositories_from_file
import Logger
import PrettyOutput
import DegreeModulesCollection
from Constants import *

class SubmissionConfiguration:
    """
    This will be our main container for our application.
    The student_number will come from the submission_config.json file
    and the rest will be calculated relative to this.

    Called in a couple of ways:

    1. Prepare phase - build directory does not exist so we are creating a new directory
    2. Build phase   - build directory exists
    3. Extract phase - we are pointing at an unzipped location somewhere
    """
    def __init__(self, 
                studentNumber,
                relativePathToCreateBuildDirectory = None, 
                configFile = None, 
                absolutePathToBuildDirectory=None,
                extractDirectory=None
        ):
        self.action = None
        self.ignoreErrors = []
        self.prepareDirectoryNotFullBuild = False
        
        # flags for turning off and on capabilities
        self.doTranscriptions = False

        self.markPortfolioWorthyOnly = True
        self.repositoryList = []
        self.yearList = None
        self.unique_years = None
        self.moduleList = None
        self.debug = False
        self.mappingRecords = None
        self.compressMediaFiles = True
        self.loggerFilePath = None
        self.studentNumber = studentNumber
        self.defaultMappingFileName = DEFAULT_MAPPING_FILENAME
        self.defaultRepositoryFileName = DEFAULT_REPOSITORY_LIST_FILENAME
        self.doExtractionAfterBuild = True
        self.mapExtractedFilesToOriginalFiles = {}          # map relative path of the extracted file to original so we can go back and forth
        self.redirectedFiles = {}                           # files they have moved about so that the older ones are no longer there which messes us up later on.

        self.relevantLearningOutcomes = []                  # list all relevant learning outcomes for this run
        self.expectedWordCountLimits = [0,0]                # expected word count limits only calculated once relevant modules selected

        if absolutePathToBuildDirectory:
            pathToBuildDirectory = absolutePathToBuildDirectory
            pathToBuildParentDirectory = os.path.dirname(absolutePathToBuildDirectory)
        elif relativePathToCreateBuildDirectory:
            pathToBuildDirectory = os.path.join(relativePathToCreateBuildDirectory, studentNumber)
            pathToBuildParentDirectory = relativePathToCreateBuildDirectory
        else:
            if configFile and os.path.isfile(configFile):
                configDir = os.path.dirname(os.path.abspath(configFile))
                dirName = os.path.basename(os.path.dirname(os.path.abspath(configFile)))
                if dirName == self.studentNumber:
                    pathToBuildParentDirectory = os.path.dirname(configDir)
                    pathToBuildDirectory = configDir
                else:
                    pathToBuildParentDirectory = configDir
                    pathToBuildDirectory = os.path.join(configDir, studentNumber)
            else:
                pathToBuildParentDirectory = os.path.abspath(".")
                pathToBuildDirectory = os.path.join(pathToBuildParentDirectory, studentNumber)
                

        self.buildDirectory = pathToBuildDirectory

        if configFile:
            # if this is specified then we 
            self.basePath = os.path.dirname(os.path.abspath(configFile))
            self.configFileName = os.path.basename(configFile)
            self.configFilePath = os.path.join(self.basePath, self.configFileName)
            self.configRepositoryListFilePath = os.path.join(self.basePath, DEFAULT_REPOSITORY_LIST_FILENAME)
        else:
            self.basePath = os.path.dirname(os.path.abspath("."))
            self.configFileName = DEFAULT_SUBMISSION_CONFIG_FILENAME
            self.configFilePath = os.path.join(self.basePath, self.configFileName)
            self.configRepositoryListFilePath = os.path.join(self.basePath, DEFAULT_REPOSITORY_LIST_FILENAME)

        # set the location of where the config file will go during the build phase
        self.buildConfigFilePath = os.path.join(self.buildDirectory, self.configFileName)
        
        if not extractDirectory:
            self.extractDirectory = os.path.join(pathToBuildParentDirectory,self.studentNumber+"_"+DEFAULT_EXTRACT_DIRECTORY_SUFFIX)
        else:
            self.extractDirectory = extractDirectory

        self.zipFilePrefix = self.studentNumber
        self.zipFileName = self.zipFilePrefix + ZIP_EXTENSION
        self.assetsDirectory = os.path.join(self.buildDirectory,DEFAULT_ASSETS_DIRECTORY_NAME)
        self.mappingFilePath = os.path.join(self.buildDirectory, DEFAULT_MAPPING_FILENAME)
        self.repositoryListFilePath = os.path.join(self.buildDirectory, DEFAULT_REPOSITORY_LIST_FILENAME)
        
    def getOriginalPath(self, path):
        if path in self.mapExtractedFilesToOriginalFiles:
            return self.mapExtractedFilesToOriginalFiles[path]
        return None

    def readRepositories(self, repoListFilePath=None):
        if not repoListFilePath:
            repoListFilePath = self.repositoryListFilePath
        repositoryList = read_repositories_from_file(repoListFilePath)
        self.repositoryList = repositoryList
        return self.repositoryList

    def setYearList(self, year):
        if isinstance(year, int):
            year = [year]
        for yr in year:
            if not yr or yr not in [1,2,3,4]:
                raise ValueError("invalid year value ({}) specified, valid values are numbers in the range 1-4".format(yr))
        self.yearList = year                           # this should be the same length as modules, one year for each module
        self.unique_years = list(set(year))         # this is the set of unique years
        self.unique_years.sort()

    def setModuleList(self, modules):
        if not modules:
            raise ValueError("modules key cannot be set empty or null")
        # for m in modules:
        #     if m not in [1,2,3,4,5,6]:
        #         raise ValueError("invalid module value ({}) in modules array".format(m))
        self.moduleList = modules

    def filterYearAndModuleLists(self):
        self.learningOutcomeCollection = createDefaultLearningOutcomesCollection()
        self.moduleCollection = DegreeModulesCollection.createDegreeModuleCollection()
        
        self.relevantModuleCollection = self.moduleCollection.filter(self.yearList, self.moduleList)
        if self.relevantModuleCollection.size() == 0:
            PrettyOutput.prettyFixException("No modules were relevant for years {} and module numbers {}".format(self.yearList, self.moduleList))
        
        self.relevantLearningOutcomes = self.learningOutcomeCollection.filter(self.yearList, self.moduleList)
        if self.relevantLearningOutcomes.size() == 0:
            PrettyOutput.prettyFixException("No learning outcomes were relevant for years {} and module numbers {}".format(self.yearList, self.moduleList))
        self.expectedWordCountLimits = self.relevantModuleCollection.getWordCount()


def readConfigurationFromJsonFile(filePath):
    if not os.path.isfile(filePath):
        PrettyOutput.prettyFixException("The file {} does not exist. To fix this you should copy one of the submission_configuration templates to submission_configuration.json, and then try again.".format(filePath))
    with open(filePath, "r", encoding="utf8") as inFile:
        jsonData = json.load(inFile)
        jsonData["__configFilePath"] = filePath
        return jsonData

def validateConfigurationJsonObject(jsonData):
    if FIELD_STUDENT_NUMBER not in jsonData:
        PrettyOutput.prettyFixException("{} key not found in {}".format(FIELD_STUDENT_NUMBER, jsonData["__configFilePath"]))

    if "year" not in jsonData:
        PrettyOutput.prettyFixException("No year key found in submission_configuration.json.\n\tEdit this file and add the following: \"year\": [1] within the curly brackets.")

    if "year" in jsonData:
        if isinstance(jsonData["year"], int):
            x = jsonData["year"]
            if x > 0 and x < 5:
                jsonData["year"] = [x]
            else:
                PrettyOutput.prettyFixException("Year is {}, which is not in the range 1-4 in the file submission_configuration.json.\n\tEdit this file and make sure the value is in the correct range.".format(x))
    
    if "modules" in jsonData:
        if isinstance(jsonData["modules"], int):
            x = jsonData["modules"]
            if x > 0 and x < 12:
                jsonData["modules"] = [x]
            else:
                PrettyOutput.prettyFixException("Modules is {}, which is not in the range 1-6 in the file submission_configuration.json.\n\tEdit this file and make sure the value is in the correct range.".format(x))

    if "year" in jsonData and "modules" in jsonData:
        if len(jsonData["year"]) != len(jsonData["modules"]):
            if len(jsonData["year"]) == 1:
                if len(jsonData["modules"]) > 1:
                    PrettyOutput.prettyWarning("Only 1 year given for multiple modules. Assuming all modules are for year {}.".format(jsonData["year"][0]))
                    jsonData["year"] = [ jsonData["year"][0] for x in jsonData["modules"] ]
            else:
                PrettyOutput.prettyFixException("Year and modules list in submission configuration are not the same size.\n\tIf all the modules are in the same year, you can just write 1.\n\tIf modules comes from different years then you need to make the year a list and put the appropriate year number for each module.")
    
def createSubmissionConfigurationFromStudentNumber(studentNumber=None,
                                        relativePathToCreateBuildDirectory = None,
                                        absolutePathToBuildDirectory=None,
                                        extractDirectory=None
                                        ):
    config = SubmissionConfiguration(studentNumber=studentNumber, 
                                        relativePathToCreateBuildDirectory=relativePathToCreateBuildDirectory,
                                        configFile=None, 
                                        absolutePathToBuildDirectory=absolutePathToBuildDirectory,
                                        extractDirectory=extractDirectory)
    return config

def createSubmissionConfigurationFromFile(configFilePath=None,
                                        studentNumber=None,
                                        relativePathToCreateBuildDirectory = None,
                                        absolutePathToBuildDirectory=None,
                                        extractDirectory=None
                                        ):
    jsonData = None
    if configFilePath:
        jsonData = readConfigurationFromJsonFile(configFilePath)
        validateConfigurationJsonObject(jsonData)
        if not studentNumber:
            studentNumber = jsonData[FIELD_STUDENT_NUMBER]
    
    config = SubmissionConfiguration(studentNumber=studentNumber, 
                                        relativePathToCreateBuildDirectory=relativePathToCreateBuildDirectory,
                                        configFile=configFilePath, 
                                        absolutePathToBuildDirectory=absolutePathToBuildDirectory,
                                        extractDirectory=extractDirectory)

    try:
        if "year" in jsonData:
            config.setYearList(jsonData["year"])
    except ValueError as err:
        PrettyOutput.prettyFixException(err + "in {}".format(configFilePath))

    try:
        if "modules" in jsonData:
            config.setModuleList(jsonData["modules"])
    except ValueError as err:
        PrettyOutput.prettyFixException(err + "in {}".format(configFilePath))

    ## set up configuration appropriately
    config.filterYearAndModuleLists()
    return config




    
