"""
Handle the situation where a marker has selected all items for download from Brightspace and 
they have a zip file.

    ClassSubmissionName.zip
        index.html
        (DIR) XXXXX-XXXXX - FIRSTNAME LASTNAME - DD MMM YYYY Hmm PM
            XXXXXXXX.zip
"""
import zipfile
import os
import Logger
import PrettyOutput
import WindowsWorkarounds
from BrightspaceSubmission import BrightspaceSubmission


def unzip_class_zipfile(zipFileWithExt, targetDirectory):
    """
    We expect the zip file to be something like 123456789.zip
    We expect the directoryToExtractInto to not exist and be something like /tmp
    """
    Logger.info("Unzipping class file {}".format(zipFileWithExt))
    if not os.path.isfile(zipFileWithExt):
        PrettyOutput.prettyRuntimeException(
            "Zip file {} does not exist.".format(zipFileWithExt))
    if os.path.isdir(targetDirectory):
        allowRemove = input(
            "{} already exists, do you want to delete it? [Y/n]".format(targetDirectory))
        if allowRemove.lower() == "n":
            PrettyOutput.prettyRuntimeException(
                "{} already exists, you declined to remove it. Please name a non-existing directory to unzip the class file into.".format(targetDirectory))
        WindowsWorkarounds.shutil_rmtree(targetDirectory)

    Logger.info("Creating directory: {}".format(targetDirectory))
    os.mkdir(targetDirectory)
    with zipfile.ZipFile(zipFileWithExt) as zip:
        zip.extractall(targetDirectory)

    # check that we expect what we have
    for root, dirs, files in os.walk(targetDirectory):
        if len(files) != 1:
            PrettyOutput.prettyWarning(
                "Unexpected number of files found in root of target directory.  Expected 1, found {}".format(len(files)))
        submissions = []
        for d in dirs:
            submission = BrightspaceSubmission.create(d, path=root)
            if submission.isValid():
                submission.loadZipFiles()
                if submission.hasNoZipFiles():
                    PrettyOutput.prettyWarning(
                        "Submission {} has no zip files".format(submission.getDirectory()))
                else:
                    submissions.append(submission)
        break  # we only want the top directory

    return submissions


def unzip_class_directory(targetDirectory):
    for root, dirs, files in os.walk(targetDirectory):
        if len(files) != 1:  # there should be an index.html file in the root of the brightspace download
            PrettyOutput.prettyWarning(
                "Unexpected number of files found in root of target directory.  Expected 1, found {}".format(len(files)))
        submissions = []
        for d in dirs:
            submission = BrightspaceSubmission.create(d, path=root)
            if submission.isValid():
                submission.loadZipFiles()
                if submission.hasNoZipFiles():
                    PrettyOutput.prettyWarning(
                        "Submission {} has no zip files".format(submission.getDirectory()))
                else:
                    submissions.append(submission)
        break  # we only want the top directory
    return submissions
