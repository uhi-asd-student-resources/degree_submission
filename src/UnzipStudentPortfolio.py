from configparser import NoOptionError
import shutil
import zipfile
import os
import Logger
import PrettyOutput
import re


def unzip_file(zipFileWithExt, directoryToExtractInto, studentNumber):
    """
    We expect the zip file to be something like 123456789.zip
    We expect the directoryToExtractInto to not exist and be something like /tmp
    """
    if directoryToExtractInto is not None and not os.path.isdir(directoryToExtractInto):
        PrettyOutput.prettyFixException(
            "Directory to extract zip file into does not exist: {}".format(directoryToExtractInto))

    # if we are not given a directory then we assume that we are unzipping
    # relative to where we are running and the directory will be
    # called after the zip file. E.g. if the zip file is 123456789.zip the directory
    # will be called 123456789.
    zipFileNameWithoutExt, _ = os.path.splitext(
        os.path.basename(zipFileWithExt))
    if not directoryToExtractInto:
        directoryToExtractInto = "."
    directoryToExtractInto = os.path.abspath(directoryToExtractInto)

    lastDir = os.path.basename(directoryToExtractInto)
    if lastDir != studentNumber:
        portfolioPath = os.path.join(directoryToExtractInto, studentNumber)
    else:
        portfolioPath = directoryToExtractInto

    Logger.info("[UNZIP] portfolioPath set to {}".format(portfolioPath))
    if os.path.isdir(portfolioPath):
        PrettyOutput.prettyWarning(
            "[UNZIP] PortfolioPath {} already exists, using this. If this is not intended, remove the portfolio and this will let the zip file be re-extracted.".format(portfolioPath))
        return portfolioPath

    with zipfile.ZipFile(zipFileWithExt, 'r') as zip:
        members = zip.namelist()
        firstMember = members[0]
        lastChar = firstMember[-1]

        # first member is a directory, as long as we can find mapping.csv somewhere
        # then we will proceed.
        mappingFileLocationInArchive = None
        for m in members:
            if re.search("mapping.csv", m):
                mappingFileLocationInArchive = m
                break
        if not mappingFileLocationInArchive:
            PrettyOutput.prettyRuntimeException(
                "Could not find mapping.csv anywhere in the members of the zip file {}. This required manual inspection, no idea what they did.".format(zipFileWithExt))

        Logger.info("Mapping file was located in archive at: {}".format(
            mappingFileLocationInArchive))
        if mappingFileLocationInArchive == "mapping.csv":
            # mapping file is in the root directory this is not good
            Logger.info(
                "[UNZIP] Found mapping.csv in the root of the zip file, will make directory to unzip files into.")
            directoryToExtractInto = os.path.join(
                directoryToExtractInto, zipFileNameWithoutExt)
            Logger.info("[UNZIP] Creating directory {}".format(
                directoryToExtractInto))
            os.mkdir(directoryToExtractInto)
            unzipRootPath = directoryToExtractInto
        else:
            # Assume mapping.csv is in a location like XXXXX/mapping.csv in which case
            # take the directory name of this path
            unzipRootPath = os.path.dirname(mappingFileLocationInArchive)
            Logger.info("Assuming first entry in zip file is a location we can extract to: {}".format(
                unzipRootPath))
            unzipRootPath = os.path.join(directoryToExtractInto, unzipRootPath)
            # There is no need to create a directory here as the unzipping will do that 
            # for us in this case.

        Logger.info("[UNZIP] Directory extracted to: {}".format(
            directoryToExtractInto))
        zip.extractall(directoryToExtractInto)
        if not os.path.isdir(unzipRootPath):
            PrettyOutput.prettyRuntimeException(
                "Zip file {} did not uncompress where we expected it to at {}. Manual inspection required.".format(zipFileWithExt, unzipRootPath))

    Logger.info(
        "[UNZIP] portfolioPath has been set to: {}".format(portfolioPath))
    Logger.info("[UNZIP] unzipRootPath set to: {}".format(unzipRootPath))

    if not os.path.isdir(portfolioPath):
        Logger.info(
            "[UNZIP] Creating portfolio path: {}".format(portfolioPath))
        os.mkdir(portfolioPath)

    unzippedIntoSubdirectory = None
    for root, dirs, files in os.walk(unzipRootPath):
        if len(files) > 0 and len(dirs) > 0:
            # Here we can handle both the student submitting the degree submission directory
            # and creating umpteen subdirectories by looking for our core file for evidence
            # and making sure we cannot see the degree_submission scripts.
            if os.path.isfile(os.path.join(root, "mapping.csv")) and not os.path.isfile(os.path.join(root, "build_submission.sh")):
                unzippedIntoSubdirectory = root
                break

    if unzippedIntoSubdirectory is not None and unzippedIntoSubdirectory != portfolioPath:
        # copy the files from the subdirectory into our intended portfolioPath
        backupDirectoryName = os.path.basename(os.path.abspath(portfolioPath))
        backupDirectory = os.path.dirname(os.path.abspath(portfolioPath))
        backupDirectory = os.path.join(
            backupDirectory, "{}_zipcontents.tmp".format(backupDirectoryName))
        Logger.info("Copying single directory ({}) contents to parent".format(
            unzippedIntoSubdirectory))
        Logger.info(
            "Copying to temporary directory: {}".format(backupDirectory))
        shutil.copytree(unzippedIntoSubdirectory,
                        backupDirectory, dirs_exist_ok=True)
        Logger.info("Deleting original directory: {}".format(
            unzippedIntoSubdirectory))
        shutil.rmtree(unzippedIntoSubdirectory)
        Logger.info("Copying temporary directory to extraction directory")
        shutil.copytree(backupDirectory, portfolioPath, dirs_exist_ok=True)
        shutil.rmtree(backupDirectory)

    return portfolioPath
