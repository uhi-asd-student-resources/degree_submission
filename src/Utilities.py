import os
import Logger
from Constants import *
import PrettyOutput
import re

def bytes_as_string(size):
    oSize = size
    if size < 1024:
        return "{} bytes".format(round(size,2))
    size /= 1024
    if size < 1024:
        return "{} KB".format(round(size,2))
    size /= 1024
    if size < 1024:
        return "{} MB".format(round(size,2))
    if size / 1024:
        return "{} GB".format(round(size,2))
    return "Too big ({} bytes)".format(round(size,2))
    
# Return true if file A is newer than file B
def isNewerFile(fileA, fileB ):
    if os.path.isfile(fileA) and not os.path.isfile(fileB):
        return True
    if not os.path.isfile(fileA) and os.path.isfile(fileB):
        return False
    return os.path.getmtime(fileA) >= os.path.getmtime(fileB)

def isStudentNumberValid(x):
    # 8 is a normal student and 9 is a test student
    if len(x) != 8 and len(x) != 9:
        return False
    return re.match("^\d+$", x)

def fixModuleListIfMissing(programArguments, configuration):
    # this is a hack to work around mistakes from the students.
    # here we force the modules list to be something
    if programArguments.enforceModuleList is not None:
        configuration.moduleList = programArguments.enforceModuleList
        Logger.info("Forcing modules list to be {}".format(configuration.moduleList))

def checkIfTestStudent(programArguments, configuration):
    if not programArguments.allowTestStudent and \
        configuration.studentNumber == DEFAULT_STUDENT_NUMBER:
        PrettyOutput.prettyFixException("You have not updated {} with your student number!".format(programArguments.configFile))
