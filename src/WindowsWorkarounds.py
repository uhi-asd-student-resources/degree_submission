import os, stat
import shutil

import errno, os, stat, shutil

# https://stackoverflow.com/questions/1213706/what-user-do-python-scripts-run-as-in-windows
def handleRemoveReadonly(func, path, exc):
    excvalue = exc[1]
    if func in (os.rmdir, os.remove) and excvalue.errno == errno.EACCES:
        os.chmod(path, stat.S_IRWXU| stat.S_IRWXG| stat.S_IRWXO) # 0777
        func(path)
    else:
        raise

def remove_readonly(func, path, _):
    """
    Clear the readonly bit and reattempt the removal
    """
    os.chmod(path, stat.S_IWRITE)
    func(path)

def shutil_rmtree(directory):
    if os.path.isdir(directory):
        shutil.rmtree(directory, onerror=handleRemoveReadonly, ignore_errors=False)