import shutil
import zipfile
import os
import Logger
import sys
import PrettyOutput

BRIGHTSPACE_MAX_FILE_UPLOAD_SIZE=2 * 1024 * 1024 * 1024
COMPRESSION_FILE_EXTENSION=".zip"

def compress_student_portfolio_into_zip_file(buildDirectory, zipFileWithoutExt):
    if zipFileWithoutExt.endswith(COMPRESSION_FILE_EXTENSION):
        zipFileWithoutExt = zipFileWithoutExt[:len(zipFileWithoutExt)-4]
    absBuildDirectory = os.path.abspath(buildDirectory)
    baseDir = os.path.basename(absBuildDirectory)
    rootDir = os.path.dirname(absBuildDirectory)
    shutil.make_archive(zipFileWithoutExt, COMPRESSION_FILE_EXTENSION[1:], root_dir=rootDir, base_dir=baseDir)
    fileSize = os.path.getsize(zipFileWithoutExt + COMPRESSION_FILE_EXTENSION)
    Logger.info("Zip file is {} bytes".format(fileSize))
    if fileSize >= BRIGHTSPACE_MAX_FILE_UPLOAD_SIZE:
        PrettyOutput.prettyFixException("Zip file is {} bytes which is too large for Brightspace. The zip file must be less than 2GB in size.  Please seek guidance from the delivery staff.".format(fileSize))


