"""
Entry point into the Degree Submission code

This file looks after reading the command line arguments, creating a SubmissionConfiguration
object which will then be used to carry out the users action.
"""
import os
import sys
import SubmissionConfiguration
from PortfolioBuildCommand import PortfolioBuildCommand
from PortfolioExtractCommand import PortfolioExtractCommand
import Logger
import colorama
from ProgramArguments import ProgramArguments
import UnzipClassDownload
from Utilities import checkIfTestStudent, fixModuleListIfMissing
from ExtractEntryPoints import *
import PrettyOutput

colorama.init()             # activate terminal colouring for Windows

VERSION="1.0.1"


def runPerStudent(programArguments, configuration=None):
    # we store this in the portfolio for a build action
    # we store this in the extracted for an extracted action
    Logger.basicConfig(name=programArguments.action,
                       level=Logger.INFO,
                       format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                       filename=sys.stdout,
                       filemode='a+')

    if programArguments.action == "extract":
        configuration = setupForExtraction(programArguments, configuration)
    else:
        configuration = SubmissionConfiguration.createSubmissionConfigurationFromFile(
            programArguments.configFile)

    configuration.loggerFilePath = programArguments.logFilePath
    configuration.action = programArguments.action
    configuration.markPortfolioWorthyOnly = programArguments.markPortfolioWorthyOnly
    configuration.ignoreErrors = programArguments.ignoreErrors
    configuration.compressMediaFiles = programArguments.compressMediaFiles
    configuration.portfolioPath = programArguments.portfolioPath
    configuration.doTranscriptions = not programArguments.skipTranscription

    if configuration.action.lower() == "build":
        parentOfBuildDirectory = os.path.dirname(configuration.buildDirectory)
        Logger.get().filePath = os.path.join(
            parentOfBuildDirectory, programArguments.logFilePath)
    elif configuration.action.lower() == "extract":
        # at this point the extract directory does not exist so we have to create the log outside of it.
        parentOfExtractDirectory = os.path.dirname(
            configuration.extractDirectory)
        Logger.get().filePath = os.path.join(
            parentOfExtractDirectory, programArguments.logFilePath)

    if configuration:
        checkIfTestStudent(programArguments, configuration)
        fixModuleListIfMissing(programArguments, configuration)

    programArguments.printToLog()
    configuration.relevantModuleCollection.log()
    configuration.relevantLearningOutcomes.log()

    handlers = []
    handlers.append(PortfolioBuildCommand(configuration))
    handlers.append(PortfolioExtractCommand(configuration))

    for handler in handlers:
        if handler.match(configuration.action):
            handler.run()
    Logger.pop()


def runProgram(programArguments, configuration):
    # Extraction can happen by giving a zip file or pointing to a portfolio directory
    if programArguments.action == "extractclass":
        PrettyOutput.G_INTERPRET_ERRORS_AS_WARNINGS = True
        submissions = []
        if os.path.isdir(programArguments.classZipFile):
            # On a Mac apparently you can set it to unzip zip files automatically so we need to add a workaround for that.
            submissions = UnzipClassDownload.unzip_class_directory(
                programArguments.classZipFile)
        else:
            baseName = os.path.basename(programArguments.classZipFile)
            fileName, _ = os.path.splitext(baseName)
            submissions = UnzipClassDownload.unzip_class_zipfile(
                programArguments.classZipFile, os.path.join(os.getcwd(), fileName))
        for sub in submissions:
            sub.extract(programArguments)
    else:
        runPerStudent(programArguments, configuration)


def runMainProgram():
    Logger.basicConfig(name="main",
                       level=Logger.INFO,
                       format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                       filename=sys.stdout,
                       filemode='a')
    programArguments = ProgramArguments.createFromScriptArguments(VERSION)
    runProgram(programArguments, None)


if __name__ == "__main__":
    runMainProgram()
