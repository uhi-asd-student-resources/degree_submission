import unittest
from BrightspaceSubmission import BrightspaceSubmission
import datetime

class BrightspaceSubmissionTest(unittest.TestCase):

    def test_create_pm(self):
        submission = BrightspaceSubmission.create("39854-91512 - GOLF SIERRA - 9 April 2021 201 PM")
        self.assertEqual(submission.firstNumber,"39854")
        self.assertEqual(submission.secondNumber,"91512")
        self.assertEqual(submission.firstName,"GOLF")
        self.assertEqual(submission.lastName,"SIERRA")
        self.assertEqual(submission.submissionDate,"9 April 2021")
        self.assertEqual(submission.submissionTime,"201 PM")
        self.assertEqual(submission.submissionTimestamp,datetime.datetime(2021, 4, 9, 14, 1))

    def test_create_am(self):
        submission = BrightspaceSubmission.create("39854-91512 - GOLF SIERRA - 9 April 2021 201 AM")
        self.assertEqual(submission.firstNumber,"39854")
        self.assertEqual(submission.secondNumber,"91512")
        self.assertEqual(submission.firstName,"GOLF")
        self.assertEqual(submission.lastName,"SIERRA")
        self.assertEqual(submission.submissionDate,"9 April 2021")
        self.assertEqual(submission.submissionTime,"201 AM")
        self.assertEqual(submission.submissionTimestamp,datetime.datetime(2021, 4, 9, 2, 1))