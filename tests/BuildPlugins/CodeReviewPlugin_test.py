import unittest
from BuildPlugins.CodeReviewPlugin import CodeReviewPlugin 
import MockScenario
import os
from PortfolioTree import PortfolioTree

class CodeReviewPluginTest(unittest.TestCase):

    def test_CodeReviewPlugin(self):
        testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        MockScenario.mock_completed_directory("123456789")
        pt = PortfolioTree(None, testPath)
        pt.create()
        visitor = CodeReviewPlugin(None)
        pt.accept(visitor)
        self.assertEqual(visitor.videoFileCount, 1)
        MockScenario.mock_destroy("123456789")
        