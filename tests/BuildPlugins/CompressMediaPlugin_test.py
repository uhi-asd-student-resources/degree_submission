import unittest
from BuildPlugins.CompressMediaPlugin import CompressMediaPlugin 
import MockScenario
import os
from PortfolioTree import PortfolioTree
import shutil

class CompressMediaPluginTest(unittest.TestCase):

    def test_CompressMediaPlugin_noCompressRequired(self):
        testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        MockScenario.mock_completed_directory("123456789")
        pt = PortfolioTree(None, testPath)
        pt.create()
        visitor = CompressMediaPlugin(None)
        pt.accept(visitor)
        # by itself the only video there is too small to require
        # compressing
        self.assertEqual(visitor.compressed, 0)
        self.assertEqual(visitor.uncompressable, 0)
        MockScenario.mock_destroy("123456789")
        
    def test_CompressMediaPlugin_shrinkable(self):
        testPortfolioDirectory = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        testFilePath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "test_files", "test_shrinkable.mp4")
        MockScenario.mock_completed_directory("123456789")
        shutil.copyfile(testFilePath, os.path.join(testPortfolioDirectory, "assets", "test_shrinkable.mp4"))
        pt = PortfolioTree(None, testPortfolioDirectory)
        pt.create()
        with self.assertRaises(ValueError):
            visitor = CompressMediaPlugin(None)
            pt.accept(visitor)
            # by itself the only video there is too small to require
            # compressing
            self.assertEqual(visitor.compressed, 1)
            self.assertEqual(visitor.uncompressable, 0)
        MockScenario.mock_destroy("123456789")
    
    def test_CompressMediaPlugin_unshrinkable(self):
        testPortfolioDirectory = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        testFilePath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "test_files", "test_video.mp4")
        MockScenario.mock_completed_directory("123456789")
        shutil.copyfile(testFilePath, os.path.join(testPortfolioDirectory, "assets", "test_unshrinkable.mp4"))
        pt = PortfolioTree(None, testPortfolioDirectory)
        pt.create()
        visitor = CompressMediaPlugin(None)
        with self.assertRaises(ValueError):
            pt.accept(visitor)
            # by itself the only video there is too small to require
            # compressing
            self.assertEqual(visitor.compressed, 0)
            self.assertEqual(visitor.uncompressable, 1)
        MockScenario.mock_destroy("123456789")
        