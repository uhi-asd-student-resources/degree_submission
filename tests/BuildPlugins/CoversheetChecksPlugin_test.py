import unittest
from BuildPlugins.CoversheetChecksPlugin import CoversheetChecksPlugin 
import MockScenario
import os
from PortfolioTree import PortfolioTree
import docx
from DegreeModulesCollection import DegreeModulesCollection, createDegreeModuleCollection
import CoversheetUtilities
import shutil
from SubmissionConfiguration import createSubmissionConfigurationFromFile
import WriteCoversheet

class CoversheetChecksPluginTest(unittest.TestCase):

    def test_CoversheetChecksPlugin_notSigned(self):
        testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        degreeModulesCollection = createDegreeModuleCollection()
        mustBeSignedName = degreeModulesCollection.getCoversheetNeedsSigningFileName(1,6)
        coversheetFilePath = os.path.join(testPath, mustBeSignedName)
        
        MockScenario.mock_completed_directory("123456789")
        shutil.copyfile(CoversheetUtilities.COVERSHEET_TEMPLATE, coversheetFilePath)
        degreeModulesCollection.fillInCoversheet(coversheetFilePath, coversheetFilePath,1, 6)
        assert(os.path.isfile(coversheetFilePath))
        config = createSubmissionConfigurationFromFile(os.path.join(testPath, "submission_configuration.json"))
        pt = PortfolioTree(None, testPath)
        pt.create()
        visitor = CoversheetChecksPlugin(config)
        with self.assertRaises(ValueError):
            pt.accept(visitor)
            self.assertEqual(visitor.unsignedCoversheetCount, 1)
            self.assertEqual(visitor.signedCoversheetCount, 0)
        MockScenario.mock_destroy("123456789")
        
    def test_CoversheetChecksPlugin_signed_but_not_renamed(self):
        testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        degreeModulesCollection = createDegreeModuleCollection()
        mustBeSignedName = degreeModulesCollection.getCoversheetNeedsSigningFileName(1,6)
        coversheetFilePath = os.path.join(testPath, mustBeSignedName)
        
        
        MockScenario.mock_completed_directory("123456789")
        shutil.copyfile(CoversheetUtilities.COVERSHEET_TEMPLATE, coversheetFilePath)
        degreeModulesCollection.fillInCoversheet(coversheetFilePath, coversheetFilePath,1, 6)
        WriteCoversheet.writeCoversheet(coversheetFilePath)
        assert(os.path.isfile(coversheetFilePath))
        config = createSubmissionConfigurationFromFile(os.path.join(testPath, "submission_configuration.json"))
        pt = PortfolioTree(None, testPath)
        pt.create()
        visitor = CoversheetChecksPlugin(config)
        with self.assertRaises(ValueError):
            pt.accept(visitor)
            self.assertEqual(visitor.unsignedCoversheetCount, 1)
            self.assertEqual(visitor.signedCoversheetCount, 0)
        MockScenario.mock_destroy("123456789")
    
    def test_CoversheetChecksPlugin_signed_before(self):
        testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        degreeModulesCollection = createDegreeModuleCollection()
        mustBeSignedName = degreeModulesCollection.getCoversheetNeedsSigningFileName(1,6)
        coversheetFilePath = os.path.join(testPath, mustBeSignedName)
        signedName = degreeModulesCollection.getCoversheetSignedFileName(1,6)
        signedFilePath = os.path.join(testPath, signedName)
        
        MockScenario.mock_completed_directory("123456789")
        shutil.copyfile(CoversheetUtilities.COVERSHEET_TEMPLATE, coversheetFilePath)
        degreeModulesCollection.fillInCoversheet(coversheetFilePath, coversheetFilePath,1, 6)
        WriteCoversheet.writeCoversheet(coversheetFilePath)
        assert(os.path.isfile(coversheetFilePath))

        shutil.move(coversheetFilePath, signedFilePath)
        config = createSubmissionConfigurationFromFile(os.path.join(testPath, "submission_configuration.json"))
        pt = PortfolioTree(None, testPath)
        pt.create()
        visitor = CoversheetChecksPlugin(config)
        pt.accept(visitor)
        self.assertEqual(visitor.unsignedCoversheetCount, 0)
        self.assertEqual(visitor.signedCoversheetCount, 1)
        MockScenario.mock_destroy("123456789")
    