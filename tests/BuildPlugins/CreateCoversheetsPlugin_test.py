import unittest
from BuildPlugins.CreateCoversheetsPlugin import CreateCoversheetsPlugin 
import MockScenario
import os
from PortfolioTree import PortfolioTree
import docx
from DegreeModulesCollection import DegreeModulesCollection, createDegreeModuleCollection
import CoversheetUtilities
import shutil
from SubmissionConfiguration import createSubmissionConfigurationFromFile

def writeCoversheet(path):    
    doc = docx.Document(path)

    tags = [
        "Your student number",
        "Your college",
        "The date of submission here",
        "Type your first and last name here or put a signature",
        "Date Submitted"
    ]
    
    for tableIndex, t in enumerate(doc.tables):
        for rowIndex, row in enumerate(t.rows):
            for cellIndex, cell in enumerate(row.cells):
                # print("CELL {}".format(cell.text))
                for tag in tags:
                    if "<<{}>>".format(tag.lower()) in str(cell.text).strip().lower():
                        cell.text = "Something that is not empty"
    doc.save(path)

class CreateCoversheetsPluginTest(unittest.TestCase):

    def test_CreateCoversheetsPlugin_noCoversheet(self):
        testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        
        MockScenario.mock_completed_directory("123456789")
        config = createSubmissionConfigurationFromFile(os.path.join(testPath, "submission_configuration.json"))
        pt = PortfolioTree(None, testPath)
        pt.create()
        visitor = CreateCoversheetsPlugin(config)
        visitor.start()
        self.assertEqual(visitor.createdCoversheetCount, 1)
        self.assertEqual(visitor.signedCoversheetCount, 0)
        MockScenario.mock_destroy("123456789")
        
    def test_CreateCoversheetsPlugin_completed(self):
        testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        degreeModulesCollection = createDegreeModuleCollection()
        mustBeSignedName = degreeModulesCollection.getCoversheetNeedsSigningFileName(1,6)
        coversheetFilePath = os.path.join(testPath, mustBeSignedName)
        
        
        MockScenario.mock_completed_directory("123456789")
        shutil.copyfile(CoversheetUtilities.COVERSHEET_TEMPLATE, coversheetFilePath)
        degreeModulesCollection.fillInCoversheet(coversheetFilePath, coversheetFilePath,1, 6)
        writeCoversheet(coversheetFilePath)
        assert(os.path.isfile(coversheetFilePath))

        config = createSubmissionConfigurationFromFile(os.path.join(testPath, "submission_configuration.json"))
        pt = PortfolioTree(None, testPath)
        pt.create()
        visitor = CreateCoversheetsPlugin(config)
        pt.accept(visitor)
        self.assertEqual(visitor.createdCoversheetCount, 0)
        self.assertEqual(visitor.signedCoversheetCount, 1)
        MockScenario.mock_destroy("123456789")
    
    