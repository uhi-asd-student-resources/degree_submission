import unittest
from BuildPlugins.DotGitDirectoryExistsPlugin import DotGitDirectoryExistsPlugin 
import MockScenario
import os
from PortfolioTree import PortfolioTree

class DotGitDirectoryExistsPluginTest(unittest.TestCase):

    def test_DotGitDirectoryExistsPlugin(self):
        testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        MockScenario.mock_completed_directory("123456789")
        pt = PortfolioTree(None, testPath)
        pt.create()
        visitor = DotGitDirectoryExistsPlugin(None)
        pt.accept(visitor)
        self.assertEqual(visitor.dotGitDirectoryCount, 4)
        MockScenario.mock_destroy("123456789")
        