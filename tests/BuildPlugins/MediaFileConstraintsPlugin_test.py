import unittest
from BuildPlugins.MediaFileConstraintsPlugin import MediaFileConstraintsPlugin 
import MockScenario
import os
from PortfolioTree import PortfolioTree
import shutil

class MediaConstraintsPluginTest(unittest.TestCase):

    def test_MediaFileConstraintsPlugin(self):
        testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        MockScenario.mock_completed_directory("123456789")
        testFilePath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "test_files", "test_shrinkable.mp4")
        MockScenario.mock_completed_directory("123456789")
        shutil.copyfile(testFilePath, os.path.join(testPath, "assets", "test_shrinkable.mp4"))
        shutil.copyfile(testFilePath, os.path.join(testPath, "assets", "test_unshrinkable.mp4"))

        pt = PortfolioTree(None, testPath)
        pt.create()
        visitor = MediaFileConstraintsPlugin(None)
        with self.assertRaises(ValueError):
            pt.accept(visitor)
            self.assertEqual(len(visitor.largeFiles), 2)
        MockScenario.mock_destroy("123456789")
        