import unittest
from BuildPlugins.PreflightChecksPlugin import PreflightChecksPlugin
import MockScenario
import os
from PortfolioTree import PortfolioTree
import SubmissionConfiguration
import WindowsWorkarounds
import shutil

class PreflightChecksPluginTest(unittest.TestCase):

    def setUp(self):
        self.degreeSubmissionRoot = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

    def test_PreflightChecksPlugin_noConfig(self):
        with self.assertRaises(RuntimeError):
            visitor = PreflightChecksPlugin(None)
            visitor.start()
        
    def test_PreflightChecksPlugin(self):
        testPath = os.path.join(self.degreeSubmissionRoot, "123456789")
        if os.path.isdir(testPath):
            WindowsWorkarounds.shutil_rmtree(testPath)
        assetsPath = os.path.join(testPath, "assets")
        mappingFile = os.path.join(testPath, "mapping.csv")
        commentsForMarkersFile = os.path.join(testPath, "Comments for markers.txt")

        testConfigPath = os.path.join(self.degreeSubmissionRoot, "submission_configuration.json")
        config = SubmissionConfiguration.createSubmissionConfigurationFromFile(testConfigPath)
        
        visitor = PreflightChecksPlugin(config)
        with self.assertRaises(ValueError):
            result = visitor.start()
            self.assertEqual(result, False)
            self.assertTrue(os.path.isdir(testPath))
            self.assertTrue(os.path.isdir(assetsPath))
            self.assertFalse(os.path.isfile(mappingFile))
            self.assertTrue(os.path.isfile(commentsForMarkersFile))
            self.assertTrue(os.path.isfile(os.path.join(testPath, "submission_configuration.json")))
        WindowsWorkarounds.shutil_rmtree(testPath)

    def test_PreflightChecksPlugin_mapping_file_exists(self):
        targetPath = os.path.join(self.degreeSubmissionRoot, "123456789")
        if os.path.isdir(targetPath):
            WindowsWorkarounds.shutil_rmtree(targetPath)
        assetsPath = os.path.join(targetPath, "assets")
        mappingFile = os.path.join(targetPath, "mapping.csv")
        commentsForMarkersFile = os.path.join(targetPath, "Comments for markers.txt")
        repositoriesFile = os.path.join(targetPath, "repositories.txt")

        testFilesPath = os.path.join(self.degreeSubmissionRoot, "test_files")        
        testMappingPath = os.path.join(testFilesPath, "mapping.csv")

        assert(os.path.isfile(testMappingPath))
        assert(not os.path.isfile(mappingFile))
        testConfigPath = os.path.join(self.degreeSubmissionRoot, "submission_configuration.json")
        config = SubmissionConfiguration.createSubmissionConfigurationFromFile(testConfigPath)
        
        # do it the first knowing that it will raise an exception
        visitor = PreflightChecksPlugin(config)
        with self.assertRaises(ValueError):
            result = visitor.start()
        
        shutil.copyfile(testMappingPath, mappingFile)
        assert(os.path.isfile(mappingFile))
        
        # now try again and see if it picks it up
        with self.assertRaises(ValueError):
            visitor = PreflightChecksPlugin(config)
            result = visitor.start()
            self.assertEqual(result, False)
            self.assertTrue(os.path.isdir(targetPath))
            self.assertTrue(os.path.isdir(assetsPath))
            self.assertTrue(os.path.isfile(mappingFile))
            self.assertTrue(os.path.isfile(commentsForMarkersFile))
            self.assertTrue(os.path.isfile(os.path.join(targetPath, "submission_configuration.json")))
            self.assertFalse(os.path.isfile(repositoriesFile))
        WindowsWorkarounds.shutil_rmtree(targetPath)

    def test_PreflightChecksPlugin_repositories_file_exists(self):
        targetPath = os.path.join(self.degreeSubmissionRoot, "123456789")
        if os.path.isdir(targetPath):
            WindowsWorkarounds.shutil_rmtree(targetPath)
        
        assetsPath = os.path.join(targetPath, "assets")
        mappingFile = os.path.join(targetPath, "mapping.csv")
        commentsForMarkersFile = os.path.join(targetPath, "Comments for markers.txt")
        repositoriesFile = os.path.join(targetPath, "repositories.txt")
        
        testFilesPath = os.path.join(self.degreeSubmissionRoot, "test_files")        
        testMappingPath = os.path.join(testFilesPath, "mapping.csv")
        testRepositoriesPath = os.path.join(testFilesPath, "RepositoryListTests", "repositories.txt")

        assert(os.path.isfile(testMappingPath))
        assert(os.path.isfile(testRepositoriesPath))
        assert(not os.path.isfile(mappingFile))
        assert(not os.path.isfile(repositoriesFile))

        testConfigPath = os.path.join(self.degreeSubmissionRoot, "submission_configuration.json")
        config = SubmissionConfiguration.createSubmissionConfigurationFromFile(testConfigPath)
        
        # do it the first knowing that it will raise an exception
        visitor = PreflightChecksPlugin(config)
        with self.assertRaises(ValueError):
            result = visitor.start()
        
        shutil.copyfile(testMappingPath, mappingFile)
        assert(os.path.isfile(mappingFile))
        shutil.copyfile(testRepositoriesPath, repositoriesFile)
        assert(os.path.isfile(repositoriesFile))
        
        # now try again and see if it picks it up
        visitor = PreflightChecksPlugin(config)
        result = visitor.start()
        self.assertEqual(result, True)
        self.assertTrue(os.path.isdir(targetPath))
        self.assertTrue(os.path.isdir(assetsPath))
        self.assertTrue(os.path.isfile(mappingFile))
        self.assertTrue(os.path.isfile(commentsForMarkersFile))
        self.assertTrue(os.path.isfile(os.path.join(targetPath, "submission_configuration.json")))
        self.assertTrue(os.path.isfile(repositoriesFile))
        self.assertEqual(len(config.repositoryList), 4)
        WindowsWorkarounds.shutil_rmtree(targetPath)