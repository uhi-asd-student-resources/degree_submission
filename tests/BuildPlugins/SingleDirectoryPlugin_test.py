import unittest
from BuildPlugins.SingleDirectoryPlugin import SingleDirectoryPlugin 
import MockScenario
import os
from PortfolioTree import PortfolioTree

class SingleDirectoryPluginTest(unittest.TestCase):

    def test_SingleDirectoryPlugin(self):
        testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        MockScenario.mock_completed_directory("123456789")
        pt = PortfolioTree(None, testPath)
        pt.create()
        visitor = SingleDirectoryPlugin(None)
        pt.accept(visitor)
        self.assertEqual(visitor.singleDirectoryCount, 1)
        MockScenario.mock_destroy("123456789")
        