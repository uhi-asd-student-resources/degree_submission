import unittest
from BuildPlugins.TeamRepositoriesPlugin import TeamRepositoriesPlugin 
import MockScenario
import os
from PortfolioTree import PortfolioTree

class TeamRepositoriesPluginTest(unittest.TestCase):

    def test_TeamRepositoriesPlugin(self):
        testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        MockScenario.mock_completed_directory("123456789")
        authors = [ "Tom McCallum" ]
        pt = PortfolioTree(None, testPath)
        pt.create()
        visitor = TeamRepositoriesPlugin(None)
        visitor.authors = authors
        pt.accept(visitor)
        self.assertEqual(len(visitor.teamRepositories), 0)
        self.assertEqual(len(visitor.soloRepositories), 4)
        self.assertEqual(len(visitor.warnings), 1)
        MockScenario.mock_destroy("123456789")

    def test_TeamRepositoriesPlugin_none(self):
        testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        MockScenario.mock_completed_directory("123456789")
        pt = PortfolioTree(None, testPath)
        pt.create()
        visitor = TeamRepositoriesPlugin(None)
        visitor.authors = []
        pt.accept(visitor)
        self.assertEqual(len(visitor.teamRepositories), 0)
        self.assertEqual(len(visitor.soloRepositories), 0)
        self.assertEqual(len(visitor.warnings), 1)
        MockScenario.mock_destroy("123456789")

    # TODO(tm) we don't have a test for an actual team repository, but have tested it with real portfolios...
    # def test_TeamRepositoriesPlugin_moreThanOne(self):
    #     testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
    #     MockScenario.mock_completed_directory("123456789")
    #     pt = PortfolioTree(None, testPath)
    #     pt.create()
    #     visitor = TeamRepositoriesPlugin(None)
    #     visitor.authors = []
    #     pt.accept(visitor)
    #     self.assertEqual(len(visitor.teamRepositories), 0)
    #     self.assertEqual(len(visitor.soloRepositories), 0)
    #     self.assertEqual(len(visitor.warnings), 1)
    #     MockScenario.mock_destroy("123456789")
        