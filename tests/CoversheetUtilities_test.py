import unittest
import CoversheetUtilities
import os

class CoversheetUtilitiesTest(unittest.TestCase):

    TESTPATH=os.path.join(os.path.dirname(os.path.dirname(__file__)), "test_files", "test_files", "CoversheetTests")

    def test_is_coversheet_completed_fail(self):
        for root, _, files in os.walk(os.path.join(CoversheetUtilitiesTest.TESTPATH, "Fails")):
            for f in files:
                testCoversheetFilePath = os.path.join(root, f)
                result = CoversheetUtilities.isCoversheetCompleted(testCoversheetFilePath)
                self.assertEqual(result, False)

    def test_is_coversheet_completed_pass(self):
        for root, _, files in os.walk(os.path.join(CoversheetUtilitiesTest.TESTPATH, "Pass")):
            for f in files:
                testCoversheetFilePath = os.path.join(root, f)
                result = CoversheetUtilities.isCoversheetCompleted(testCoversheetFilePath)
                self.assertEqual(result, True)

    def test_whyIsCoversheetNotCompleted_isCompleted(self):
        for root, _, files in os.walk(os.path.join(CoversheetUtilitiesTest.TESTPATH, "Pass")):
            for f in files:
                testCoversheetFilePath = os.path.join(root, f)
                result = CoversheetUtilities.whyIsCoversheetNotComplete(testCoversheetFilePath)
                self.assertEqual(result, [])

    def test_whyIsCoversheetNotCompleted_notCompleted(self):
        failPath = os.path.join(CoversheetUtilitiesTest.TESTPATH, "Fails")
        failPath = os.path.join(failPath, "UI107006_must_be_signed_before_submission 1.docx")
        result = CoversheetUtilities.whyIsCoversheetNotComplete(failPath)
        self.assertEqual(len(result), 5)