import unittest
from DegreeModule import DegreeModule
import csv

class DegreeModuleTest(unittest.TestCase):

    def test_DegreeModule(self):
        row = "1,\"UI107006\",\"Principles of programming\",\"S1\",\"01 Sept 2021\",\"17 Dec 2022\",2500,\"Karen MacKay\",\"LO 1,2,3,4,5\""
        data = csv.reader([row], delimiter=',', quotechar='"')
        module = DegreeModule.createFromCsv(list(data)[0])
        self.assertEqual(module.year,1)
        self.assertEqual(module.code, "UI107006")
        self.assertEqual(module.name, "Principles of programming")
        self.assertEqual(module.semester, "S1")
        self.assertEqual(module.dateDue, "17 Dec 2022")
        self.assertEqual(module.dateIssued, "01 Sept 2021")
        self.assertEqual(module.lecturer, "Karen MacKay")
        self.assertEqual(module.learningOutcomes, "LO 1,2,3,4,5")
        