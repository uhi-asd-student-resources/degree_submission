import unittest
import DegreeModulesCollection
import os
import CoversheetUtilities
from DirectoryUtils import sha256sum

class DegreeModulesCollectionTest(unittest.TestCase):

    def setUp(self):
        self.moduleCollection = DegreeModulesCollection.createDegreeModuleCollection()

    def test_modules(self):
        self.assertEqual(self.moduleCollection.size(), 22)

    def test_find_caseinsensitive(self):
        result = self.moduleCollection.find("principles of programming")
        self.assertIsNotNone(result)

    def test_findByNumber_asInt(self):
        result = self.moduleCollection.findByNumber(1, 6)
        self.assertIsNotNone(result)

    def test_findByNumber_asString(self):
        result = self.moduleCollection.findByNumber(1, "6")
        self.assertIsNotNone(result)

    def test_getCoversheetSignedFileName(self):
        result = self.moduleCollection.getCoversheetSignedFileName(1, 6)
        self.assertEqual(result, "UI107006_coversheet_signed.docx")

    def test_getCoversheetNeedsSigningFileName(self):
        result = self.moduleCollection.getCoversheetNeedsSigningFileName(1, 6)
        self.assertEqual(result, "UI107006_coversheet_must_sign_before_submission.docx")

    def test_createAllCoversheets(self):
        targetPath = os.path.dirname(__file__)
        created = self.moduleCollection.createAllCoversheets(targetPath)
        self.assertEqual(len(created), 22)
        for s in created:
            os.remove(s)

    def test_fillInCoversheet(self):
        targetPath = os.path.join(os.path.dirname(__file__), "coversheet_test.docx")
        sourcePath = CoversheetUtilities.COVERSHEET_TEMPLATE
        self.moduleCollection.fillInCoversheet(sourcePath, targetPath, 1, 6)
        self.assertTrue(os.path.isfile(targetPath))
        # NOTE(TM) Unfortunately cannot test they are the same as there appears to be metadata that changes on saving new version.
        # expectedFilePath = os.path.join(os.path.dirname(os.path.dirname(__file__)), "test_files", "CoversheetTests", "coversheet_test.docx")
        # self.assertEqual(sha256sum(targetPath), sha256sum(expectedFilePath))
        os.remove(targetPath)