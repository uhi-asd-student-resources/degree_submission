import unittest
import DirectoryUtils
import os

class DirectoryUtils_test(unittest.TestCase):

    def test_isDotGitDirectory_true(self):
        # ths current degree_submission path is a git
        # repository and therefore will have a .git file
        thisPath = os.path.join(os.path.dirname(os.path.dirname(__file__)), ".git")
        result = DirectoryUtils.isDotGitDirectory(thisPath)
        self.assertEqual(result, True)

    def test_isDotGitDirectory_false(self):
        thisPath = os.path.dirname(__file__)
        result = DirectoryUtils.isDotGitDirectory(thisPath)
        self.assertEqual(result, False)

    def test_isGitRoot_true(self):
        # ths current degree_submission path is a git
        # repository and therefore will have a .git file
        thisPath = os.path.join(os.path.dirname(os.path.dirname(__file__)))
        result = DirectoryUtils.isGitRoot(thisPath)
        self.assertEqual(result, True)

    def test_isGitRoot_false(self):
        thisPath = os.path.dirname(__file__)
        result = DirectoryUtils.isDotGitDirectory(thisPath)
        self.assertEqual(result, False)

    def test_findFileInSubdirectories_found(self):
        thisPath = os.path.join(os.path.dirname(os.path.dirname(__file__)))
        thisFile = os.path.join(thisPath, "README.md")
        result = DirectoryUtils.findFileInSiblingDirectories(thisFile)
        theActualFileNameFound = os.path.basename(result)
        theActualParentDirectoryFound = os.path.basename(os.path.dirname(result))
        self.assertEqual(theActualFileNameFound, "README.md")
        self.assertEqual(theActualParentDirectoryFound, "degree_submission")

    def test_findFileInSubdirectories_notfound(self):
        thisPath = os.path.join(os.path.dirname(os.path.dirname(__file__)))
        thisFile = os.path.join(thisPath, "README2.md")
        result = DirectoryUtils.findFileInSiblingDirectories(thisFile)
        self.assertEqual(result, None)

    def test_findFileInRepository_notFound(self):
        thisPath = os.path.join(os.path.dirname(os.path.dirname(__file__)))
        thisFile = os.path.join(thisPath, "README2.md")
        result = DirectoryUtils.findFileInRepository(thisFile)
        self.assertEqual(result, None)

    def test_findFileInRepository_found(self):
        thisPath = os.path.join(os.path.dirname(os.path.dirname(__file__)))
        thisFile = os.path.join(thisPath, "README.md")
        result = DirectoryUtils.findFileInRepository(thisFile)
        print(result)
        self.assertIsNotNone(result)

    def test_findFileInSubdirectories_loop(self):
        thisPath = os.path.join(os.path.dirname(os.path.dirname(__file__)))
        # thisFile = os.path.join(thisPath, "README.md")
        with self.assertRaises(RuntimeError):
            DirectoryUtils.findFileInSiblingDirectories(thisPath)
    
    def test_isLatex_true(self):
        result = DirectoryUtils.isLatex("markdown.tex")
        self.assertEqual(result, True)

    def test_isLatex_false(self):
        result = DirectoryUtils.isLatex("markdown.texe")
        self.assertEqual(result, False)

    def test_isLearningOutcomeEvidenceDirectory_success(self):
        self.assertEqual(DirectoryUtils.isLearningOutcomeEvidenceDirectory("LO1.2.3.4"), True)

    def test_isLearningOutcomeEvidenceDirectory_fail(self):
        self.assertEqual(DirectoryUtils.isLearningOutcomeEvidenceDirectory("LO1.2.3.x"), False)

    def test_sha256sum(self):
        thisPath = os.path.join(os.path.dirname(os.path.dirname(__file__)))
        thisFile = os.path.join(thisPath, "README.md")
        result = DirectoryUtils.sha256sum(thisFile)
        expectedSha256Hash = "43db6f5c3e67f3ddea07b7306ebdf1bd93959657c73ce18ee4b4c68b00f3cffa"
        self.assertEqual(result, expectedSha256Hash)

        
