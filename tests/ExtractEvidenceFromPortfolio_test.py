import unittest
from ExtractEvidenceFromPortfolio import extractEvidenceFromStudentPortfolio
import os
import WindowsWorkarounds
import shutil
from SubmissionConfiguration import createSubmissionConfigurationFromFile
import MappingRecordCollection
import Repositories

class ExtractEvidenceFromPortfolioTest(unittest.TestCase):

    def setUp(self):
        self.degreeSubmissionPath = os.path.dirname(os.path.dirname(__file__))
        self.testPath = os.path.join(self.degreeSubmissionPath, "test_files", "test_files", "ExtractEvidenceFromPortfolioTests")
        self.testTarget = os.path.join(self.degreeSubmissionPath, "123456789")
        self.testExtractTarget = os.path.join(self.degreeSubmissionPath, "123456789_extracted")

    def test_pydriller_test(self):
        if os.path.isdir(self.testTarget):
            WindowsWorkarounds.shutil_rmtree(self.testTarget)
        if os.path.isdir(self.testExtractTarget):
            WindowsWorkarounds.shutil_rmtree(self.testExtractTarget)
        
        thisTestPath = os.path.join(self.testPath, "pydriller")
        assert(os.path.isdir(thisTestPath))
        shutil.copytree(thisTestPath, self.testTarget)

        
        configFilePath = os.path.join(self.testTarget, "submission_configuration.json")
        config = createSubmissionConfigurationFromFile(configFilePath)
        self.assertEqual(os.path.abspath(config.buildDirectory), self.testTarget)
        self.assertEqual(os.path.abspath(config.extractDirectory), self.testExtractTarget)
        os.mkdir(self.testExtractTarget)

        config.mappingRecords = MappingRecordCollection.createMappingRecordCollection(config.mappingFilePath)
        config.repositoryList = config.readRepositories()
        repositoryList = Repositories.clone_repository_list(config.repositoryList, config.buildDirectory)
        config.mappingRecords.matchDirectoriesToRepositories(config.buildDirectory)
        config.repositoryList = repositoryList

        extractEvidenceFromStudentPortfolio(config)

        isThere = os.path.join(self.testExtractTarget, "LO1.1.1.1", "a28c49c7bb5fab32a2f9a60437bc0cfa4a56b06a", "helloworld_20220405T110739_UTC_a28c49c_ADD_TAGGED.md")
        self.assertTrue(os.path.isfile(isThere))
        # todo(TM) complete the checks for the files that should be here and also the 2 _latest files that should not exist as they are deleted.
        
        if os.path.isdir(self.testTarget):
            WindowsWorkarounds.shutil_rmtree(self.testTarget)
        if os.path.isdir(self.testExtractTarget):
            WindowsWorkarounds.shutil_rmtree(self.testExtractTarget)
        
