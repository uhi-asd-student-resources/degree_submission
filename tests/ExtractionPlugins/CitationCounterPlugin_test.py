import unittest
from ExtractionPlugins.CitationCounterPlugin import CitationCounterPlugin

class CitationCounterPluginTest(unittest.TestCase):

    def test_citationcount_date_middle(self):
        text = "this is a citation (1996)"
        counter = CitationCounterPlugin(None)
        value = counter._countCitationsInText(text)
        self.assertEqual(value, 1)

    def test_citationcount_date_after_newline(self):
        text = "(1996) this is a citation\n(1996)"
        counter = CitationCounterPlugin(None)
        value = counter._countCitationsInText(text)
        self.assertEqual(value, 2)

    def test_citationcount_name_and_date_middle(self):
        text = "this is a citation (Shore 1996)"
        counter = CitationCounterPlugin(None)
        value = counter._countCitationsInText(text)
        self.assertEqual(value, 1)

    def test_citationcount_name_and_date_after_newline(self):
        text = "(Martock. 1996) this is a citation\n(Pims, 1996)"
        counter = CitationCounterPlugin(None)
        value = counter._countCitationsInText(text)
        self.assertEqual(value, 2)

    def test_citationcount_name_and_date_and_middle(self):
        text = "this is a citation (Shore & Mitchell 1996)"
        counter = CitationCounterPlugin(None)
        value = counter._countCitationsInText(text)
        self.assertEqual(value, 1)

    def test_citationcount_name_and_date_and_after_newline(self):
        text = "(Martock & Kirk. 1996) this is a citation\n(Pims & Leslie, 1996)"
        counter = CitationCounterPlugin(None)
        value = counter._countCitationsInText(text)
        self.assertEqual(value, 2)

    def test_viera(self):
        text = "delivery staff (Vieira, 2022c) I identified"
        counter = CitationCounterPlugin(None)
        value = counter._countCitationsInText(text)
        self.assertEqual(value, 1)