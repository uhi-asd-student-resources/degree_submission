import unittest
from ExtractionPlugins.CommitLogCapturePlugin import CommitLogCapturePlugin
from MockScenario import mock_completed_directory, mock_destroy
import os
import WindowsWorkarounds
from PortfolioTreeVisitor import PortfolioTreeVisitor

class CommitLogCapturePluginTest(unittest.TestCase):

    def setUp(self):
        self.degreeSubmissionRoot = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
        self.portfolioDir = os.path.join(self.degreeSubmissionRoot, "123456789")
        self.extractionDir = os.path.join(self.degreeSubmissionRoot, "123456789_extracted")
        self.commitDir = os.path.join(self.extractionDir, "commits")
        
    def test_matchFile(self):
        WindowsWorkarounds.shutil_rmtree(self.extractionDir)
        mock_completed_directory("123456789")
        plugin = CommitLogCapturePlugin(None)
        result = plugin.matchDirectory(os.path.join(self.portfolioDir, "demopersonal"))
        self.assertTrue(result)
        result = plugin.matchDirectory(os.path.join(self.portfolioDir, "demo_faulty_portal"))
        self.assertTrue(result)
        result = plugin.matchDirectory(os.path.join(self.portfolioDir, "assets"))
        self.assertFalse(result)
        result = plugin.matchDirectory(self.degreeSubmissionRoot)
        self.assertFalse(result)
        mock_destroy("123456789")
        WindowsWorkarounds.shutil_rmtree(self.extractionDir)
        
    def test_commits(self):
        WindowsWorkarounds.shutil_rmtree(self.extractionDir)
        os.mkdir(os.path.join(self.degreeSubmissionRoot, "123456789_extracted"))
        mock_completed_directory("123456789")
        plugin = CommitLogCapturePlugin(None, self.commitDir)
        plugin.run(os.path.join(self.portfolioDir, "demopersonal"))
        self.assertEqual(plugin.commitCount, 25)
        mock_destroy("123456789")
        WindowsWorkarounds.shutil_rmtree(self.extractionDir)
