import unittest
from ExtractionPlugins.CoversheetExtractionPlugin import CoversheetExtractionPlugin
from MockScenario import mock_completed_directory, mock_destroy
import os
import WindowsWorkarounds
from PortfolioTree import PortfolioTree
import SubmissionConfiguration
import shutil
import CoversheetUtilities
from DegreeModulesCollection import createDegreeModuleCollection
import WriteCoversheet

class CoversheetExtractionPluginTest(unittest.TestCase):

    def setUp(self):
        self.degreeSubmissionRoot = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
        self.portfolioDir = os.path.join(self.degreeSubmissionRoot, "123456789")
        self.extractionDir = os.path.join(self.degreeSubmissionRoot, "123456789_extracted")
        self.targetDirectory = os.path.join(self.extractionDir, "coversheets")
        
    def test_matchFile(self):
        WindowsWorkarounds.shutil_rmtree(self.extractionDir)
        mock_completed_directory("123456789")
        plugin = CoversheetExtractionPlugin(None)
        result = plugin.matchFile(os.path.join(self.portfolioDir, "UI107006_coversheet_signed.docx"), ".docx")
        self.assertTrue(result)
        result = plugin.matchFile(os.path.join(self.portfolioDir, "UI107006_CovErsHeet_signed.docx"), ".docx")
        self.assertTrue(result)
        result = plugin.matchFile(os.path.join(self.portfolioDir, "ui107006_CovErsHeet_signed.docx"), ".docx")
        self.assertTrue(result)
        result = plugin.matchFile(os.path.join(self.portfolioDir, "ui107006_CovErsHee_signed.docx"), ".docx")
        self.assertFalse(result)
        mock_destroy("123456789")
        WindowsWorkarounds.shutil_rmtree(self.extractionDir)
        
    def test_coversheet_count(self):
        WindowsWorkarounds.shutil_rmtree(self.extractionDir)
        os.mkdir(os.path.join(self.degreeSubmissionRoot, "123456789_extracted"))
        os.mkdir(os.path.join(self.degreeSubmissionRoot, "123456789_extracted", "coversheets"))
        mock_completed_directory("123456789")
        
        testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        degreeModulesCollection = createDegreeModuleCollection()
        mustBeSignedName = degreeModulesCollection.getCoversheetSignedFileName(1,6)
        coversheetFilePath = os.path.join(testPath, mustBeSignedName)
        shutil.copyfile(CoversheetUtilities.COVERSHEET_TEMPLATE, coversheetFilePath)
        degreeModulesCollection.fillInCoversheet(coversheetFilePath, coversheetFilePath,1, 6)
        WriteCoversheet.writeCoversheet(coversheetFilePath)
        assert(os.path.isfile(coversheetFilePath))

        config = SubmissionConfiguration.createSubmissionConfigurationFromFile(os.path.join(self.portfolioDir, "submission_configuration.json"))
        self.assertIsNotNone(config)
        self.assertIsNotNone(self.portfolioDir)
        tree = PortfolioTree(config, self.portfolioDir)
        tree.create()
        self.assertIsNotNone(self.targetDirectory)
        plugin = CoversheetExtractionPlugin(config, self.targetDirectory)
        tree.accept(plugin)
        self.assertEqual(plugin.coversheetCount, 1)
        mock_destroy("123456789")
        WindowsWorkarounds.shutil_rmtree(self.extractionDir)
