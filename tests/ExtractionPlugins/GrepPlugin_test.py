import unittest
from ExtractionPlugins.GrepPlugin import GrepPlugin
from MockScenario import mock_completed_directory, mock_destroy
import os
import WindowsWorkarounds
from PortfolioTree import PortfolioTree
import SubmissionConfiguration
import shutil
import CoversheetUtilities
from DegreeModulesCollection import createDegreeModuleCollection
import WriteCoversheet

class GrepPluginTest(unittest.TestCase):

    def setUp(self):
        self.degreeSubmissionRoot = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
        self.portfolioDir = os.path.join(self.degreeSubmissionRoot, "123456789")
        self.extractionDir = os.path.join(self.degreeSubmissionRoot, "123456789_extracted")
        
    def test_coversheet_count(self):
        WindowsWorkarounds.shutil_rmtree(self.extractionDir)
        os.mkdir(os.path.join(self.degreeSubmissionRoot, "123456789_extracted"))
        mock_completed_directory("123456789")
        
        testPath = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "123456789")
        degreeModulesCollection = createDegreeModuleCollection()
        mustBeSignedName = degreeModulesCollection.getCoversheetSignedFileName(1,6)
        coversheetFilePath = os.path.join(testPath, mustBeSignedName)
        shutil.copyfile(CoversheetUtilities.COVERSHEET_TEMPLATE, coversheetFilePath)
        degreeModulesCollection.fillInCoversheet(coversheetFilePath, coversheetFilePath,1, 6)
        WriteCoversheet.writeCoversheet(coversheetFilePath)
        assert(os.path.isfile(coversheetFilePath))

        config = SubmissionConfiguration.createSubmissionConfigurationFromFile(os.path.join(self.portfolioDir, "submission_configuration.json"))
        self.assertIsNotNone(config)
        self.assertIsNotNone(self.portfolioDir)
        tree = PortfolioTree(config, self.portfolioDir)
        tree.create()
        plugin = GrepPlugin(config)
        tree.accept(plugin)
        self.assertEqual(plugin.matchCount, 11)
        mock_destroy("123456789")
        #WindowsWorkarounds.shutil_rmtree(self.extractionDir)
