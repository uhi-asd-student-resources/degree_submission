import unittest
from ExtractionPlugins.LearningOutcomeCodeCheckerPlugin import LearningOutcomeCodeCheckerPlugin
from MockScenario import mock_completed_directory, mock_destroy
import os
import WindowsWorkarounds
from PortfolioTree import PortfolioTree
import SubmissionConfiguration
import shutil
import CoversheetUtilities
from DegreeModulesCollection import createDegreeModuleCollection
import WriteCoversheet

class LearningOutcomeCodeCheckerPluginTest(unittest.TestCase):

    def setUp(self):
        self.degreeSubmissionRoot = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
        self.testPath = os.path.join(self.degreeSubmissionRoot, "test_files", "LearningOutcomeCodeCheckerTests")
        assert(os.path.isdir(self.testPath))

    def test_matchFile(self):
        notDetectedFile = os.path.join(self.testPath, "not_detected.txt")
        assert(os.path.isfile(notDetectedFile))
        plugin = LearningOutcomeCodeCheckerPlugin(None)
        self.assertFalse(plugin.matchFile(notDetectedFile, ".txt"))

        detectedFile = os.path.join(self.testPath, "LO1.1.1.1", "no_lo_latest.txt")
        assert(os.path.isfile(detectedFile))
        plugin = LearningOutcomeCodeCheckerPlugin(None)
        self.assertTrue(plugin.matchFile(detectedFile, ".txt"))

    def test_run_none(self):
        path = os.path.join(self.testPath, "LO1.1.1.1", "no_lo_latest.txt")
        plugin = LearningOutcomeCodeCheckerPlugin(None)
        plugin.run(path)
        self.assertEqual(len(plugin.warnings), 1)

    def test_run_single(self):
        path = os.path.join(self.testPath, "LO1.1.1.1", "lo_latest.txt")
        plugin = LearningOutcomeCodeCheckerPlugin(None)
        plugin.run(path)
        self.assertEqual(len(plugin.warnings), 0)

    def test_run_conflict(self):
        path = os.path.join(self.testPath, "LO1.1.1.1", "lo_conflict_latest.txt")
        plugin = LearningOutcomeCodeCheckerPlugin( None)
        plugin.run(path)
        self.assertEqual(len(plugin.warnings), 1)

    def test_run_no_conflict(self):
        """
        Has 2 learning outcomes from 2 modules but they are both
        meta skills.
        """
        path = os.path.join(self.testPath, "LO1.1.1.1", "lo_no_conflict_latest.txt")
        plugin = LearningOutcomeCodeCheckerPlugin(None)
        plugin.run(path)
        self.assertEqual(len(plugin.warnings), 0)

    def test_run_dot_in_lo_code(self):
        """
        Has a dot in the learning code e.g. LO.1.2.3.4
        """
        path = os.path.join(self.testPath, "LO1.1.1.1", "lo_no_conflict_dot_latest.txt")
        plugin = LearningOutcomeCodeCheckerPlugin(None)
        plugin.run(path)
        self.assertEqual(len(plugin.warnings), 0)
