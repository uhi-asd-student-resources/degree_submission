import unittest
from ExtractionPlugins.ObsidianMarkdown import *

class ObsidianMarkdownTest(unittest.TestCase):

    def setUp(self):
        self.degreeSubmittionRoot = os.path.dirname(os.path.dirname(__file__))
        self.testPath = os.path.join(self.degreeSubmittionRoot, "test_files", "ObsidianTests")

    def test_obsidianIsBlock(self):
        text = "[[filename#^dcf64c]]"
        result = obsidianIsBlock(text)
        self.assertTrue(result)

    def test_obsidianGetBlockFromLink(self):
        text = "[[filename#^dcf64c]]"
        result = obsidianGetBlockFromLink(text)
        self.assertEqual(result, "dcf64c")

    def test_obsidianIsUrl_https(self):
        text = "https://bbc.co.uk"
        result = obsidianIsUrl(text)
        self.assertTrue(result)

    def test_obsidianIsUrl_http(self):
        text = "http://bbc.co.uk"
        result = obsidianIsUrl(text)
        self.assertTrue(result)

    def test_obsidianGetFileFromLink(self):
        text = "filename#section"
        result = obsidianGetFileFromLink(text)
        self.assertEqual(result, "filename")

    def test_obsidianGetSectionFromLink(self):
        text = "filename#section"
        result = obsidianGetSectionFromLink(text)
        self.assertEqual(result, "section")

    def test_obsidianGetBlockText(self):
        linkPath = os.path.join(self.testPath, "blockExample.md")
        block = "dcf64c"
        result = obsidianGetBlockText(linkPath, block)
        expected = "This is the block.\nexpected to be returned."
        self.assertEqual(result, expected)

    def test_obsidianGetBlockText_startsWithCaret(self):
        linkPath = os.path.join(self.testPath, "blockExample.md")
        block = "^dcf64c"
        result = obsidianGetBlockText(linkPath, block)
        expected = "This is the block.\nexpected to be returned."
        self.assertEqual(result, expected)

    def test_obsidianGetBlockText_inline(self):
        linkPath = os.path.join(self.testPath, "blockExample.md")
        block = "dcf64c"
        result = obsidianGetBlockText(linkPath, block)
        expected = "This is the block.\nexpected to be returned."
        self.assertEqual(result, expected)

    def test_obsidianGetBlockText_atStart(self):
        linkPath = os.path.join(self.testPath, "blockExampleAtStart.md")
        block = "dcf64c"
        result = obsidianGetBlockText(linkPath, block)
        expected = ""
        self.assertEqual(result, expected)

    def test_obsidianGetBlockText_atStart(self):
        linkPath = os.path.join(self.testPath, "blockExampleNotFound.md")
        block = "dcf64c"
        result = obsidianGetBlockText(linkPath, block)
        self.assertIsNone(result)

    def test_obsidianGetSectionText(self):
        linkPath = os.path.join(self.testPath, "sectionExample.md")
        section = "Hello World"
        result = obsidianGetSectionText(linkPath, section)
        expected = "## Hello World\n\nThis is some text in the block.\n\nAnd we continue\n\nuntil \n\nwe\n\nhit \n\nthe next section"
        self.assertEqual(result, expected)

    def test_obsidianGetSectionText(self):
        linkPath = os.path.join(self.testPath, "sectionExample.md")
        section = "new sub section"
        result = obsidianGetSectionText(linkPath, section)
        expected = "## new sub section\n\nSome text here to find me.\n\nHere."
        self.assertEqual(result, expected)

    def test_obsidianGetSectionText(self):
        linkPath = os.path.join(self.testPath, "sectionExample.md")
        section = "another subsection"
        result = obsidianGetSectionText(linkPath, section)
        expected = "## another subsection\n\n### with a subsubsubsection\n\nsome text"
        self.assertEqual(result, expected)

    # def test_internalLinks1(self):
    #   links = getObsidianInternalLinks("[[test.txt#^a4b43]]")
    #   self.assertEqual(len(links), 1)
    #   self.assertEqual(links[0], "test.txt#^a4b43")

    # def test_internalLinks0(self):
    #   links = getObsidianInternalLinks("This is not an image.")
    #   self.assertEqual(len(links), 0)

    # def test_internalLinks2(self):
    #   links = getObsidianInternalLinks("[[test.txt#^568ef]] and\nthis on a newline\n\n[[after.txt#^5698ed]]")
    #   self.assertEqual(len(links), 2)
    #   self.assertEqual(links[0], "test.txt#^568ef")
    #   self.assertEqual(links[1], "after.txt#^5698ed")

    # def test_internalLinks3_1(self):
    #   links = getObsidianInternalLinks("\n[[file:///C:\\test%20me again.txt#^568ef]]")
    #   # printLinks(links)
    #   self.assertEqual(len(links), 1)
    #   self.assertEqual(links[0], "C:\\test me again.txt#^568ef")
      
    # def test_internalLinks3_2(self):
    #   links = getObsidianInternalLinks("\n[[after.txt#^5698ed]]")
    #   # printLinks(links)
    #   self.assertEqual(len(links), 1)
    #   self.assertEqual(links[0], "after.txt#^5698ed")

    # def test_internalLinks3(self):
    #   links = getObsidianInternalLinks("[[file:///C:\\test%20me again.txt#^568ef]] and\nthis on a newline\n\n[[after.txt#^5698ed]]")
    #   # printLinks(links)
    #   self.assertEqual(len(links), 2)
    #   self.assertEqual(links[0], "C:\\test me again.txt#^568ef")
    #   self.assertEqual(links[1], "after.txt#^5698ed")
      
    # def test_embedding(self):
    #   links = getObsidianEmbeddingLinks("![[file:///C:\\test%20me again.txt#What is this?]] and\nthis on a newline\n\n![[after.txt#why me]]")
    #   # printLinks(links)
    #   self.assertEqual(len(links), 2)
    #   self.assertEqual(links[0], "C:\\test me again.txt#What is this?")
    #   self.assertEqual(links[1], "after.txt#why me")

    # def test_image(self):
    #   links = getObsidianExternalLinks("![Engelbart](https://history-computer.com/ModernComputer/Basis/images/Engelbart.jpg) and ![Engelbart|100](https://history-computer.com/ModernComputer/Basis/images/Engelbart2.jpg)")
    #   # printLinks(links)
    #   self.assertEqual(len(links), 2)
    #   self.assertEqual(links[0], "https://history-computer.com/ModernComputer/Basis/images/Engelbart.jpg")
    #   self.assertEqual(links[1], "https://history-computer.com/ModernComputer/Basis/images/Engelbart2.jpg")
