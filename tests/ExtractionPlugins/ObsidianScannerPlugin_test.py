import unittest
from ExtractionPlugins.ObsidianMarkdown import REGEX_GITHUB_DIRECT_LINK_LOCATION
from ExtractionPlugins.ObsidianScannerPlugin import ObsidianScannerPlugin
import os


class ObsidianScannerPluginTest(unittest.TestCase):

    def setUp(self):
        self.degreeSubmissionRoot = os.path.dirname(
            os.path.dirname(os.path.dirname(__file__)))
        self.testPath = os.path.join(
            self.degreeSubmissionRoot, "test_files", "ObsidianTests")
        self.assertTrue(os.path.isdir(self.testPath))

    def test_replaceTagsWithValues(self):
        plugin = ObsidianScannerPlugin(None)
        expectedFile = os.path.join(
            self.testPath, "replaceTagsWithValues_test.html")
        assert(os.path.isfile(expectedFile))
        with open(expectedFile, "r", encoding="utf-8") as inFile:
            expected = inFile.read()
            html = plugin.html_markdown_template_code
            kv = {
                "title": "Hello World",
                "file_date": "22/07/2022",
                "extraction_path": "/tmp/portfolio_extracted/test.md",
                "original_path": "/tmp/portfolio/test.md",
                "main": "more html"
            }
            actual = plugin._replaceTagsWithValues(html, kv)
            self.assertEqual(actual, expected)

    def test_findImageInBuildDirectory_samedir(self):
        plugin = ObsidianScannerPlugin(None)
        sourceDirectoryInBuildDir = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2")
        result = plugin._findImageInBuildDirectory(
            "image1.png", sourceDirectoryInBuildDir)
        self.assertTrue(result.endswith(
            "test_files/ObsidianTests/example_obsidian_library/sublevel1/sublevel2/image1.png"))

    def test_findImageInBuildDirectory_siblingdir(self):
        plugin = ObsidianScannerPlugin(None)
        sourceDirectoryInBuildDir = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2")
        result = plugin._findImageInBuildDirectory(
            "image2.png", sourceDirectoryInBuildDir)
        self.assertTrue(result.endswith(
            "test_files/ObsidianTests/example_obsidian_library/sublevel1/sibling/image2.png"))

    def test_findImageInBuildDirectory_parentdir(self):
        plugin = ObsidianScannerPlugin(None)
        sourceDirectoryInBuildDir = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2")
        result = plugin._findImageInBuildDirectory(
            "image3.png", sourceDirectoryInBuildDir)
        self.assertTrue(result.endswith(
            "test_files/ObsidianTests/example_obsidian_library/sublevel1/image3.png"))

    def test_findImageInBuildDirectory_parentparentdir(self):
        plugin = ObsidianScannerPlugin(None)
        sourceDirectoryInBuildDir = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2")
        result = plugin._findImageInBuildDirectory(
            "image4.png", sourceDirectoryInBuildDir)
        self.assertTrue(result.endswith(
            "test_files/ObsidianTests/example_obsidian_library/image4.png"))

    def test_findImageInBuildDirectory_childdir(self):
        plugin = ObsidianScannerPlugin(None)
        sourceDirectoryInBuildDir = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2")
        result = plugin._findImageInBuildDirectory(
            "image5.png", sourceDirectoryInBuildDir)
        self.assertTrue(result.endswith(
            "test_files/ObsidianTests/example_obsidian_library/sublevel1/sublevel2/child/image5.png"))

    def test_resolveEmbeddedImages(self):
        content = "![[image1.png]]"
        relativePathFromDestinationToOriginal = "."
        sourceDirectoryInBuildDir = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2")
        filePathInBuildDirectory = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2", "example.md")
        destinationDirectoryInExtractedDir = os.path.join(
            self.testPath, "fake_extracted", "LO1.1.1.1")
        plugin = ObsidianScannerPlugin(None)
        result = plugin._resolveEmbeddedImages(content, relativePathFromDestinationToOriginal,
                                               sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        expected = "<div class='embed'><div class='quote'><a href='../../example_obsidian_library/sublevel1/sublevel2/image1.png' target='_blank'><p><img alt=\"image1.png\" src=\"../../example_obsidian_library/sublevel1/sublevel2/image1.png\"></p></a></div><p>Embedded image reference: image1.png</p></div>"
        self.assertEqual(result, expected)

    def test_resolveInternalImages(self):
        content = "[[image1.png]]"
        relativePathFromDestinationToOriginal = "."
        sourceDirectoryInBuildDir = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2")
        filePathInBuildDirectory = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2", "example.md")
        destinationDirectoryInExtractedDir = os.path.join(
            self.testPath, "fake_extracted", "LO1.1.1.1")
        plugin = ObsidianScannerPlugin(None)
        result = plugin._resolveInternalImages(content, relativePathFromDestinationToOriginal,
                                               sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        expected = """<div class='embed'><div class='quote'><a href='../../example_obsidian_library/sublevel1/sublevel2/image1.png' target='_blank'><p><img alt="image1.png" src="../../example_obsidian_library/sublevel1/sublevel2/image1.png"></p></a></div><p>Linked image reference: image1.png</p></div>"""
        self.assertEqual(result, expected)

    def test_resolveInternalMarkdownLinks(self):
        content = "[[child/example2]]"
        relativePathFromDestinationToOriginal = "."
        sourceDirectoryInBuildDir = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2")
        filePathInBuildDirectory = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2", "example.md")
        destinationDirectoryInExtractedDir = os.path.join(
            self.testPath, "fake_extracted", "LO1.1.1.1")
        plugin = ObsidianScannerPlugin(None)
        result = plugin._resolveInternalMarkdownLinks(
            content, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        expected = """[child/example2](./child/example2.md)"""
        self.assertEqual(result, expected)

    def test_resolveEmbeddedBlockLinks(self):
        content = "![[child/example2.md#^1a2e3d]]"
        relativePathFromDestinationToOriginal = "."
        sourceDirectoryInBuildDir = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2")
        filePathInBuildDirectory = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2", "example.md")
        destinationDirectoryInExtractedDir = os.path.join(
            self.testPath, "fake_extracted", "LO1.1.1.1")
        plugin = ObsidianScannerPlugin(None)
        result = plugin._resolveEmbeddedBlockLinks(content, relativePathFromDestinationToOriginal,
                                                   sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        expected = """<div class='embed'><div class='quote'><p>Block of text</p></div><p>Embedded block reference: child/example2.md#^1a2e3d</p></div>"""
        self.assertEqual(result, expected)

    def test_resolveEmbeddedSectionLinks(self):
        content = "![[child/example2.md#Heading1]]"
        relativePathFromDestinationToOriginal = "."
        sourceDirectoryInBuildDir = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2")
        filePathInBuildDirectory = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2", "example.md")
        destinationDirectoryInExtractedDir = os.path.join(
            self.testPath, "fake_extracted", "LO1.1.1.1")
        plugin = ObsidianScannerPlugin(None)
        result = plugin._resolveEmbeddedSectionLinks(
            content, relativePathFromDestinationToOriginal, sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        expected = """<div class='embed'><div class='quote'><h1>Heading1</h1>\n<h2>Heading2</h2>\n<p>Block of text ^1a2e3d</p>\n<p>This is some more text</p></div><p>Embedded block reference: child/example2.md#Heading1</p></div>"""
        self.assertEqual(result, expected)

    def test_resolveLinkedBlockLinks(self):
        content = "[[child/example2.md#^1a2e3d]]"
        relativePathFromDestinationToOriginal = "."
        sourceDirectoryInBuildDir = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2")
        filePathInBuildDirectory = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2", "example.md")
        destinationDirectoryInExtractedDir = os.path.join(
            self.testPath, "fake_extracted", "LO1.1.1.1")
        plugin = ObsidianScannerPlugin(None)
        result = plugin._resolveLinkedBlockLinks(content, relativePathFromDestinationToOriginal,
                                                 sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        expected = """<div class='embed'><div class='quote'><p>Block of text</p></div><p>Linked block reference: child/example2.md#^1a2e3d</p></div>"""
        self.assertEqual(result, expected)

    def test_resolveLinkedBlockLinks_studentProblem(self):
        filePath = os.path.join(self.testPath, "blockTest.md")
        with open(filePath, "r", encoding="utf-8") as inFile:
            content = inFile.read()
            relativePathFromDestinationToOriginal = "."
            sourceDirectoryInBuildDir = self.testPath
            filePathInBuildDirectory = filePath
            destinationDirectoryInExtractedDir = os.path.join(
                self.testPath, "fake_extracted", "LO1.1.1.1")
            plugin = ObsidianScannerPlugin(None)
            result = plugin._resolveLinkedBlockLinks(content, relativePathFromDestinationToOriginal,
                                                     sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
            expected = """This has been something I've remained aware of throughout the course, in these final weeks especially as I've been moving home for the last couple, which has been particularly stressful! <div class='embed'><div class='quote'><p>I've found it extremely difficult to create a stored procedure between the constraints of time and my health see asset_files/StoredProcedureProblems.mp4 and none of the materials I tried have worked with ibm</p></div><p>Linked block reference: StoredProcedure#^6c5218</p></div>"""
            self.assertEqual(result, expected)

    def test_resolveGitHubImageLinks(self):
        content = "![something to say](image1.png)"
        relativePathFromDestinationToOriginal = "."
        sourceDirectoryInBuildDir = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2")
        filePathInBuildDirectory = os.path.join(
            self.testPath, "example_obsidian_library", "sublevel1", "sublevel2", "example.md")
        destinationDirectoryInExtractedDir = os.path.join(
            self.testPath, "fake_extracted", "LO1.1.1.1")
        plugin = ObsidianScannerPlugin(None)
        result = plugin._resolveGitHubImageLinks(content, relativePathFromDestinationToOriginal,
                                                 sourceDirectoryInBuildDir, filePathInBuildDirectory, destinationDirectoryInExtractedDir)
        expected = "<div class='embed'><div class='quote'><a href='../../example_obsidian_library/sublevel1/sublevel2/image1.png' target='_blank'><p><img alt=\"something to say\" src=\"../../example_obsidian_library/sublevel1/sublevel2/image1.png\"></p></a></div><p>Embedded image reference: image1.png<br/>Alt text: something to say</p></div>"
        self.assertEqual(result, expected)

    def test_GitHubDirectImageLink(self):
        import regex
        content = "[Big dog placement presentation](../../assets/bd-placement-presentation.mp4)"
        content = "[abc](def)"
        print(regex.findall(REGEX_GITHUB_DIRECT_LINK_LOCATION, content,
              regex.UNICODE | regex.MULTILINE | regex.IGNORECASE))
