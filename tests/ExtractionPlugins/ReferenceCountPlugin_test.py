import unittest
from ExtractionPlugins.ReferenceCountPlugin import ReferenceCountPlugin
from MockScenario import mock_completed_directory, mock_destroy
import os
from PortfolioTree import PortfolioTree
from DegreeModulesCollection import createDegreeModuleCollection

class ReferenceCountPluginTest(unittest.TestCase):

    def setUp(self):
        self.degreeSubmissionRoot = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
        self.testPath = os.path.join(self.degreeSubmissionRoot, "test_files", "ReferenceCountTests")
        assert(os.path.isdir(self.testPath))

    def test_matchFile(self):
        notDetectedFile = os.path.join(self.testPath, "not_detected.txt")
        assert(os.path.isfile(notDetectedFile))
        plugin = ReferenceCountPlugin(None)
        self.assertFalse(plugin.matchFile(notDetectedFile, ".txt"))

        detectedFile = os.path.join(self.testPath, "LO1.1.1.1", "references_latest.txt")
        assert(os.path.isfile(detectedFile))
        plugin = ReferenceCountPlugin(None)
        self.assertTrue(plugin.matchFile(detectedFile, ".txt"))

    def test_run_none(self):
        path = os.path.join(self.testPath, "LO1.1.1.1", "no_references_latest.txt")
        plugin = ReferenceCountPlugin(None)
        plugin.run(path)
        plugin.finish()
        self.assertEqual(plugin.referenceCount, 0)
        self.assertEqual(len(plugin.warnings), 1)
        self.assertEqual(len(plugin.info), 0)

    def test_run_single(self):
        path = os.path.join(self.testPath, "LO1.1.1.1", "references_latest.txt")
        plugin = ReferenceCountPlugin(None)
        plugin.run(path)
        plugin.finish()
        # will not catch it if there is no date which will
        # be rare so as long as we are in the right
        # ballpark
        self.assertEqual(plugin.referenceCount, 77)
        self.assertEqual(len(plugin.warnings), 0)
        self.assertEqual(len(plugin.info), 1)

    