import unittest
from ExtractionPlugins.SpellCheckerPlugin import SpellCheckerPlugin
import os

class SpellCheckerTest(unittest.TestCase):

    def test_spellCheckText_correct(self):
      text = "This has no spelling mistakes."
      spellChecker = SpellCheckerPlugin()
      mistakes = spellChecker.spellCheckText(text)
      self.assertEqual(len(mistakes), 0)

    def test_spellCheckText_1mistake(self):
      text = "This has no spelling mistakesblah."
      spellChecker = SpellCheckerPlugin()
      mistakes = spellChecker.spellCheckText(text)
      self.assertEqual(len(mistakes), 1)

    def test_spellCheckFile_False(self):
      filePath = os.path.join(os.path.dirname(__file__), "spelling_noerror_latest.md")
      self.assertEqual(os.path.isfile(filePath), True)
      spellChecker = SpellCheckerPlugin()
      hasErrors = spellChecker.spellCheckFile(filePath)
      self.assertEqual(hasErrors, False)

    def test_spellCheckFile_True(self):
      filePath = os.path.join(os.path.dirname(__file__), "spelling_error_latest.md")
      self.assertEqual(os.path.isfile(filePath), True)
      spellChecker = SpellCheckerPlugin()
      hasErrors = spellChecker.spellCheckFile(filePath)
      self.assertEqual(hasErrors, True)