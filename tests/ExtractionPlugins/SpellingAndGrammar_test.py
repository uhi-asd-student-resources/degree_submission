import unittest
import ExtractionPlugins.SpellingAndGrammar as SpellingAndGrammar


class SpellingAndGrammarTest(unittest.TestCase):

    def test_removeMarkdownLinkFromString(self):
        str = "this is a [[file.png]] and [file2.png]"
        expected = "this is a   and  "
        actual = SpellingAndGrammar.removeMarkdownLinkFromString(str)
        self.assertEqual(expected, actual)

    def test_removeCitationsFromString(self):
        str = "this is a (Mark, 2008) or (Friar 1994)\n(Walker 2010)"
        expected = "this is a   or  \n "
        actual = SpellingAndGrammar.removeCitationsFromString(str)
        self.assertEqual(expected, actual)

    def test_removeReferencesFromString_format1(self):
        str = "this is some text\n\nMarker, T. (2002) The life and work of kippers. Here\nnot a reference"
        expected = "this is some text\n\n \nnot a reference"
        actual = SpellingAndGrammar.removeReferencesFromString(str)
        self.assertEqual(expected, actual)

    def test_removeReferencesFromString_format2(self):
        str = "this is some text\n\nMarker, T (2002) The life and work of kippers. Here\nnot a reference"
        expected = "this is some text\n\n \nnot a reference"
        actual = SpellingAndGrammar.removeReferencesFromString(str)
        self.assertEqual(expected, actual)

    def test_removeReferencesFromString_format3(self):
        str = "this is some text\n\nMarker, T 2002. The life and work of kippers. Here\nnot a reference"
        expected = "this is some text\n\n \nnot a reference"
        actual = SpellingAndGrammar.removeReferencesFromString(str)
        self.assertEqual(expected, actual)

    def test_removeLearningOutcomesFromString_format1(self):
        str = "This is a learning outcome (Lo1.2.3.4)"
        expected = "This is a learning outcome  "
        actual = SpellingAndGrammar.removeLearningOutcomesFromString(str)
        self.assertEqual(expected, actual)

    def test_removeLearningOutcomesFromString_format2(self):
        str = "This is a learning outcome (lo1.2.3.4)"
        expected = "This is a learning outcome  "
        actual = SpellingAndGrammar.removeLearningOutcomesFromString(str)
        self.assertEqual(expected, actual)

    def test_removeLearningOutcomesFromString_format3(self):
        str = "This is a learning outcome (1.2.3.4)"
        expected = "This is a learning outcome  "
        actual = SpellingAndGrammar.removeLearningOutcomesFromString(str)
        self.assertEqual(expected, actual)

    def test_removeLearningOutcomesFromString_format4(self):
        str = "This is a learning outcome lo1.2.3.4"
        expected = "This is a learning outcome  "
        actual = SpellingAndGrammar.removeLearningOutcomesFromString(str)
        self.assertEqual(expected, actual)

    def test_removeLearningOutcomesFromString_format5(self):
        str = "This is a learning outcome lo 1.2.3.4"
        expected = "This is a learning outcome  "
        result = SpellingAndGrammar.removeLearningOutcomesFromString(str)
        self.assertEqual(expected, result)

    def test_removeLikelyUrlsFromString(self):
      str = "This is a https://bbc.co.uk\nhttp://barnstorm.co url"
      expected = "This is a  \n  url"
      result = SpellingAndGrammar.removeLikelyUrlsFromString(str)
      self.assertEqual(expected, result)

    def test_removeFilePathsFromString(self):
      str = "This is a c:\dos\widnows.exe\nfile.png file paths"
      expected = "This is a  \n  file paths"
      result = SpellingAndGrammar.removeFilePathsFromString(str)
      self.assertEqual(expected, result)

    def test_removeObsidianBlocksFromString(self):
      str = "This is a ^#2d4a5b\n^#2d4a5b file paths"
      expected = "This is a  \n  file paths"
      result = SpellingAndGrammar.removeObsidianBlocksFromString(str)
      self.assertEqual(expected, result)

    def test_removeObsidianBlocksFromString_fail(self):
      str = "This is a ^#2d4a5b\n^#2d4a5bc file paths"
      expected = "This is a  \n^#2d4a5bc file paths"
      result = SpellingAndGrammar.removeObsidianBlocksFromString(str)
      self.assertEqual(expected, result)

    def test_removeApostropheSuffixesFromString(self):
      str = "This shouldn't couldn't ain't Golf's finest hour"
      expected = "This should  could  ai  Golf  finest hour"
      result = SpellingAndGrammar.removeApostropheSuffixesFromString(str)
      self.assertEqual(expected, result)

    def test_removeMultipleSpacesFromString(self):
      str = "This      is not"
      expected = "This is not"
      result = SpellingAndGrammar.removeMultipleSpacesFromString(str)
      self.assertEqual(expected, result)

    def test_isWordOfSignificantLength(self):
      word = [ "is", "a", "for", "strategy" ]
      expected = [ False, False, False, True ]
      for ind, w in enumerate(word):
        self.assertEqual(SpellingAndGrammar.isWordOfSignificantLength(w), expected[ind])

    def test_isNumber(self):
      numbers = [ "1", "1.0", "1e-5", "+4", "-4", "-1.2e+5.3", "e", "a" ]
      expected = [ True,True,True,True,True,True,False,False ]
      for ind, w in enumerate(numbers):
        result = SpellingAndGrammar.isNumber(w)
        self.assertEqual(result, expected[ind])


    def test_hasParagraphs_true(self):
        content = """

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque iaculis lobortis ante, id tincidunt nulla pellentesque sit amet. Maecenas elementum sagittis eros. Donec tincidunt auctor purus, vel laoreet nibh ullamcorper ac. Vivamus euismod tristique neque, ut malesuada turpis placerat efficitur. Morbi purus magna, blandit at maximus in, venenatis sed sapien. Sed molestie malesuada finibus. Ut accumsan varius nisi, et rhoncus dolor sollicitudin ac. Nam finibus, nunc in viverra gravida, urna urna vestibulum dolor, ut aliquam quam dolor et erat. Morbi vehicula finibus hendrerit.

Donec mattis mauris nibh, vitae consequat quam fermentum quis. Mauris quis mattis eros, quis euismod nunc. Nunc ut nisl est. Curabitur nec arcu elementum, pellentesque ligula id, elementum libero. Duis congue tortor ex. Nulla feugiat mauris id nunc aliquam ultricies. Integer elementum convallis luctus. Vestibulum maximus lobortis purus, eu tempus metus dapibus sed. Sed sagittis egestas lectus at cursus. Integer porta tristique orci, id laoreet est egestas a. Quisque pellentesque sem lorem, feugiat consequat nunc tincidunt vel. Quisque egestas tempor egestas. Mauris eu elit congue, semper tortor a, commodo metus. Curabitur malesuada orci id augue interdum, non lacinia ligula semper. Suspendisse elementum consectetur arcu, eget maximus odio ultrices eget. """
        success, _, _ = SpellingAndGrammar.hasParagraphs(content)
        self.assertEqual(success, True)

    def test_hasParagraphs_false(self):
        content = """

Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

"""
        success, _, _ = SpellingAndGrammar.hasParagraphs(content)
        self.assertEqual(success, False)

