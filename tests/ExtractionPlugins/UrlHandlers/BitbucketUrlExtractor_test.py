import unittest
from ExtractionPlugins.UrlHandlers.BitbucketUrlHandler import BitbucketUrlHandler


class BitbucketUrlHandlerTest(unittest.TestCase):

    def test_BitbucketUrlHandler_match_true(self):
        handler = BitbucketUrlHandler(None)
        result = handler.match(
            "https://bitbucket.org/TomMcCallum/demopersonal/src/master/README.md")
        self.assertEqual(result, True)
