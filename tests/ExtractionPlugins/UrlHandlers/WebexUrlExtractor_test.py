import unittest
from ExtractionPlugins.UrlHandlers.WebexUrlHandler import WebexUrlHandler


class WebexUrlHandlerTest(unittest.TestCase):

    def test_WebexUrlHandler_match_true(self):
        handler = WebexUrlHandler(None)
        result = handler.match(
            "https://uhi.webex.com/uhi/ldr.php?RCID=fa7b2d5f1efad6b94649606fd4de3ca3")
        self.assertEqual(result, True)
