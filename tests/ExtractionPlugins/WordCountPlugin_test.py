import unittest
from ExtractionPlugins.WordCountPlugin import WordCountPlugin
from MockScenario import mock_completed_directory, mock_destroy
import os
from PortfolioTree import PortfolioTree
from DegreeModulesCollection import createDegreeModuleCollection

class WordCountPluginTest(unittest.TestCase):

    def setUp(self):
        self.degreeSubmissionRoot = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
        self.testPath = os.path.join(self.degreeSubmissionRoot, "test_files", "WordCountTests")
        assert(os.path.isdir(self.testPath))

    def test_matchFile(self):
        notDetectedFile = os.path.join(self.testPath, "not_detected.txt")
        assert(os.path.isfile(notDetectedFile))
        plugin = WordCountPlugin(None)
        self.assertFalse(plugin.matchFile(notDetectedFile, ".txt"))

        detectedFile = os.path.join(self.testPath, "LO1.1.1.1", "word_count_2500_latest.txt")
        assert(os.path.isfile(detectedFile))
        plugin = WordCountPlugin(None)
        self.assertTrue(plugin.matchFile(detectedFile, ".txt"))

    def test_run_none(self):
        path = os.path.join(self.testPath, "LO1.1.1.1", "word_count_0_latest.txt")
        plugin = WordCountPlugin(None)
        plugin.run(path)
        plugin.finish()
        self.assertEqual(plugin.wordCount, 0)
        self.assertEqual(len(plugin.warnings), 1)

    def test_run_1000(self):
        path = os.path.join(self.testPath, "LO1.1.1.1", "word_count_1000_latest.txt")
        plugin = WordCountPlugin(None)
        plugin.run(path)
        plugin.finish()
        # will not catch it if there is no date which will
        # be rare so as long as we are in the right
        # ballpark
        self.assertEqual(plugin.wordCount, 1000)
        self.assertEqual(len(plugin.warnings), 1)
        
    def test_run_2000(self):
        path = os.path.join(self.testPath, "LO1.1.1.1", "word_count_2000_latest.txt")
        plugin = WordCountPlugin(None)
        plugin.run(path)
        plugin.finish()
        # will not catch it if there is no date which will
        # be rare so as long as we are in the right
        # ballpark
        self.assertEqual(plugin.wordCount, 2000)
        self.assertEqual(len(plugin.warnings), 1)
    
    def test_run_2500(self):
        path = os.path.join(self.testPath, "LO1.1.1.1", "word_count_2500_latest.txt")
        plugin = WordCountPlugin(None)
        plugin.run(path)
        plugin.finish()
        # will not catch it if there is no date which will
        # be rare so as long as we are in the right
        # ballpark
        self.assertEqual(plugin.wordCount, 2500)
        self.assertEqual(len(plugin.warnings), 0)

    def test_run_3500(self):
        path = os.path.join(self.testPath, "LO1.1.1.1", "word_count_3500_latest.txt")
        plugin = WordCountPlugin(None)
        plugin.run(path)
        plugin.finish()
        # will not catch it if there is no date which will
        # be rare so as long as we are in the right
        # ballpark
        self.assertEqual(plugin.wordCount, 3500)
        self.assertEqual(len(plugin.warnings), 1)