import unittest
from LearningOutcome import LearningOutcome


class LearningOutcomeTest(unittest.TestCase):

    def test_isSoftwareDevelopment(self):
        themes = [1,4,7,10]
        for a in themes :
            lo = LearningOutcome()
            lo.theme = a
            self.assertEqual(lo.isDataTheme(), False)
            self.assertEqual(lo.isSoftwareDevelopmentTheme(), True)
            self.assertEqual(lo.isOperationsTheme(), False)

    def test_isOperations(self):
        themes = [2,5,8,11]
        for a in themes :
            lo = LearningOutcome()
            lo.theme = a
            self.assertEqual(lo.isDataTheme(), False)
            self.assertEqual(lo.isSoftwareDevelopmentTheme(), False)
            self.assertEqual(lo.isOperationsTheme(), True)

    def test_isData(self):
        themes = [3,6,9,12]
        for a in themes :
            lo = LearningOutcome()
            lo.theme = a
            self.assertEqual(lo.isDataTheme(), True)
            self.assertEqual(lo.isSoftwareDevelopmentTheme(), False)
            self.assertEqual(lo.isOperationsTheme(), False)

    def test_createFromCsv(self):
        data = "1|Principles of programming|Agile|6|1|LO1.1.1.1|Explain the importance of delivering value early and often, iterating and continuously improving workflows where necessary.|1"
        data = data.split("|")
        lo = LearningOutcome.createFromCsvData(data)
        self.assertEqual(lo.theme, 1)
        self.assertEqual(lo.moduleName, "Principles of programming")
        self.assertEqual(lo.moduleNumber, 6)
        self.assertEqual(lo.loNumber, 1)
        self.assertEqual(lo.loCode, "LO1.1.1.1")
        self.assertEqual(lo.description, "Explain the importance of delivering value early and often, iterating and continuously improving workflows where necessary.")
