import unittest
import LearningOutcomesCollection

class LearningOutcomesCollectionTest(unittest.TestCase):

    def test_learningoutcomecollection_create(self):
        collection = LearningOutcomesCollection.createDefaultLearningOutcomesCollection()
        self.assertEqual(collection.size(), 120)