import unittest
import Logger

class LoggerTest(unittest.TestCase):

    def test_parseFormatString(self):
        text = "%(asctime)s %(name)-12s test %(levelname)-8s %(msg)s"
        tokens = Logger.parseFormatString(text)
        self.assertEqual(tokens[0]['label'], '%(asctime)')
        self.assertEqual(tokens[0]['type'], 'string')
        self.assertIsNone(tokens[0]['width'])
        self.assertEqual(tokens[1]['value'], ' ')
        self.assertEqual(tokens[1]['type'], 'text')
        self.assertEqual(tokens[2]['label'], '%(name)')
        self.assertEqual(tokens[2]['type'], 'string')
        self.assertEqual(tokens[2]['width'],'12')

    def test_compose(self):
        thisLogger = Logger.Logger("MYLOG")
        msg = thisLogger.compose(Logger.INFO, "test message")
        self.assertEqual(len(msg), 54)
        self.assertTrue(msg.endswith(" MYLOG        INFO     test message"))
        
        
        