import unittest
import MockScenario
import os
import MappingRecordCollection

class MappingRecordCollectionTest(unittest.TestCase):

    TESTPATH=os.path.join(os.path.dirname(os.path.dirname(__file__)), "test_files", "test_files", "MappingFileTests", "mapping 1.csv")
    def test_read_mapping_file(self):
        collection = MappingRecordCollection.createMappingRecordCollection(MappingRecordCollectionTest.TESTPATH)
        self.assertEqual(13,collection.size())

    def test_get_portfolio_worthy_rows(self):
        collection = MappingRecordCollection.createMappingRecordCollection(MappingRecordCollectionTest.TESTPATH)
        newCollection = collection.filter_portfolio_worthy()
        self.assertEqual(3,newCollection.size())