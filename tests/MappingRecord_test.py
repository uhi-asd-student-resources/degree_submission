import unittest
from MappingRecord import MappingRecord
import csv


class MappingRecordTest(unittest.TestCase):

    def test_createFromCsv(self):
        row = "\"TRUE\",\"LO1.1.1.1\",\"Reflection\",\"Tom McCallum\",\"git@bitbucket.org:TomMcCallum/demopersonal.git\",\"7d7442682f983b4e33c4885d5864b0d7f33c577a\""
        data = csv.reader([row], delimiter=",", quotechar = '"')
        mr = MappingRecord.createFromCsv(list(data)[0])
        expected = "{True,LO1.1.1.1,REFLECTION,Tom McCallum,git@bitbucket.org:TomMcCallum/demopersonal.git,7d7442682f983b4e33c4885d5864b0d7f33c577a,None,None}"
        self.assertEqual(str(mr), expected)
        