import os
import shutil
import WindowsWorkarounds
import Repositories

def mock_no_directory(mockDirectory):
    if os.path.isdir(mockDirectory):
        WindowsWorkarounds.shutil_rmtree(mockDirectory)
    os.mkdir(mockDirectory)

def mock_completed_directory(mockDirectory):
    if os.path.isdir(mockDirectory):
        WindowsWorkarounds.shutil_rmtree(mockDirectory)
    shutil.copytree("test_files/test_files/123456789_complete", mockDirectory)
    repositoryList = Repositories.read_repositories_from_file(os.path.join(mockDirectory, "repositories.txt"))
    Repositories.clone_repository_list( repositoryList, "123456789")

def mock_destroy(mockDirectory):
    if os.path.isdir(mockDirectory):
        WindowsWorkarounds.shutil_rmtree(mockDirectory)
