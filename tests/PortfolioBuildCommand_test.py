import unittest
import SubmissionConfiguration
import os
from PortfolioBuildCommand import PortfolioBuildCommand
import MockScenario
import shutil

class PortfolioBuildCommandTest(unittest.TestCase):

    def test_prepare_stage(self):
        degreeSubmissionRootPath = os.path.dirname(os.path.dirname(__file__))
        MockScenario.mock_destroy("123456789")
        assert(not os.path.isdir(os.path.join(degreeSubmissionRootPath, "123456789")))

        configJsonDestPath = os.path.join(degreeSubmissionRootPath, "submission_configuration.json")
        mappingCsvDestPath = os.path.join(degreeSubmissionRootPath,"mapping.csv")
        repositoriesTxtDestPath = os.path.join(degreeSubmissionRootPath,"repositories.txt")
        configFile = os.path.join(degreeSubmissionRootPath,"submission_configuration.json")

        # copy defaults from test_files
        testFileRootPath = os.path.join(degreeSubmissionRootPath,"test_files", "test_files","PortfolioBuildCommandTests", "prepare_stage")
        shutil.copyfile(os.path.join(testFileRootPath, "submission_configuration.json"), configJsonDestPath)
        shutil.copyfile(os.path.join(testFileRootPath, "mapping.csv"), mappingCsvDestPath)
        shutil.copyfile(os.path.join(testFileRootPath, "repositories.txt"), repositoriesTxtDestPath)
        
        configuration = SubmissionConfiguration.createSubmissionConfigurationFromFile(configFile)
        configuration.basePath = os.path.abspath(".")
        configuration.action = "build"
        configuration.markPortfolioWorthyOnly = True
        configuration.ignoreErrors = []
        cmd = PortfolioBuildCommand(configuration)
        result = cmd._stage1()
        self.assertFalse(result)
        self.assertTrue(os.path.isdir("123456789"))
        self.assertTrue(os.path.isdir("123456789/assets"))
        self.assertTrue(os.path.isfile("123456789/mapping.csv"))
        self.assertTrue(os.path.isfile("123456789/repositories.txt"))
        os.remove(mappingCsvDestPath)
        os.remove(repositoriesTxtDestPath)
        MockScenario.mock_destroy("123456789")

    def test_stage1_and_stage2_stops_on_unfilled_coversheets(self):
        degreeSubmissionRootPath = os.path.dirname(os.path.dirname(__file__))
        MockScenario.mock_destroy("123456789")
        assert(not os.path.isdir(os.path.join(degreeSubmissionRootPath, "123456789")))

        configJsonDestPath = os.path.join(degreeSubmissionRootPath, "submission_configuration.json")
        mappingCsvDestPath = os.path.join(degreeSubmissionRootPath,"mapping.csv")
        repositoriesTxtDestPath = os.path.join(degreeSubmissionRootPath,"repositories.txt")
        configFile = os.path.join(degreeSubmissionRootPath,"submission_configuration.json")

        # copy defaults from test_files
        testFileRootPath = os.path.join(degreeSubmissionRootPath,"test_files","test_files","PortfolioBuildCommandTests", "prepare_stage")
        shutil.copyfile(os.path.join(testFileRootPath, "submission_configuration.json"), configJsonDestPath)
        
        MockScenario.mock_completed_directory("123456789")
        configuration = SubmissionConfiguration.createSubmissionConfigurationFromFile(configJsonDestPath)
        configuration.basePath = os.path.abspath(".")
        configuration.action = "build"
        configuration.markPortfolioWorthyOnly = True
        configuration.ignoreErrors = []
        cmd = PortfolioBuildCommand(configuration)
        result = cmd._stage1()
        with self.assertRaises(ValueError):
            self.assertTrue(result)
            if result:
                result = cmd._stage2()
            self.assertFalse(result)
            self.assertTrue(os.path.isdir("123456789"))
            self.assertTrue(os.path.isdir("123456789/assets"))
            self.assertTrue(os.path.isfile("123456789/mapping.csv"))
            self.assertTrue(os.path.isfile("123456789/repositories.txt"))
            self.assertTrue(os.path.isfile("123456789.zip"))
        MockScenario.mock_destroy("123456789")

    def test_stage1_and_stage2_pass(self):
        degreeSubmissionRootPath = os.path.dirname(os.path.dirname(__file__))
        MockScenario.mock_destroy("123456789")
        assert(not os.path.isdir(os.path.join(degreeSubmissionRootPath, "123456789")))

        configJsonDestPath = os.path.join(degreeSubmissionRootPath, "submission_configuration.json")
        mappingCsvDestPath = os.path.join(degreeSubmissionRootPath,"mapping.csv")
        repositoriesTxtDestPath = os.path.join(degreeSubmissionRootPath,"repositories.txt")
        configFile = os.path.join(degreeSubmissionRootPath,"submission_configuration.json")

        # copy defaults from test_files
        testFileRootPath = os.path.join(degreeSubmissionRootPath,"test_files","test_files","PortfolioBuildCommandTests", "prepare_stage")
        shutil.copyfile(os.path.join(testFileRootPath, "submission_configuration.json"), configJsonDestPath)
        
        MockScenario.mock_completed_directory("123456789")

        
        configuration = SubmissionConfiguration.createSubmissionConfigurationFromFile(configJsonDestPath)
        configuration.basePath = os.path.abspath(".")
        configuration.action = "build"
        configuration.markPortfolioWorthyOnly = True
        configuration.ignoreErrors = []

        # copy the coversheets over
        coversheetPath = os.path.join(degreeSubmissionRootPath, "test_files", "test_files", "CoversheetTests")
        shutil.copyfile(os.path.join(coversheetPath, "UI107001_coversheet_signed.docx"), os.path.join(configuration.buildDirectory, "UI107001_coversheet_signed.docx"))
        shutil.copyfile(os.path.join(coversheetPath, "UI107002_coversheet_signed.docx"), os.path.join(configuration.buildDirectory, "UI107002_coversheet_signed.docx"))
        shutil.copyfile(os.path.join(coversheetPath, "UI107003_coversheet_signed.docx"), os.path.join(configuration.buildDirectory, "UI107003_coversheet_signed.docx"))
        shutil.copyfile(os.path.join(coversheetPath, "UI107004_coversheet_signed.docx"), os.path.join(configuration.buildDirectory, "UI107004_coversheet_signed.docx"))
        shutil.copyfile(os.path.join(coversheetPath, "UI107005_coversheet_signed.docx"), os.path.join(configuration.buildDirectory, "UI107005_coversheet_signed.docx"))
        shutil.copyfile(os.path.join(coversheetPath, "UI107006_coversheet_signed.docx"), os.path.join(configuration.buildDirectory, "UI107006_coversheet_signed.docx"))

        cmd = PortfolioBuildCommand(configuration)
        result = cmd._stage1()
        self.assertTrue(result)
        if result:
            result = cmd._stage2()
        self.assertTrue(result)
        self.assertTrue(os.path.isdir("123456789"))
        self.assertTrue(os.path.isdir("123456789/assets"))
        self.assertTrue(os.path.isfile("123456789/mapping.csv"))
        self.assertTrue(os.path.isfile("123456789/repositories.txt"))
        self.assertTrue(os.path.isfile(os.path.join(degreeSubmissionRootPath,"123456789.zip")))
        MockScenario.mock_destroy("123456789")

    def test_run(self):
        degreeSubmissionRootPath = os.path.dirname(os.path.dirname(__file__))
        MockScenario.mock_destroy("123456789")
        assert(not os.path.isdir(os.path.join(degreeSubmissionRootPath, "123456789")))

        configJsonDestPath = os.path.join(degreeSubmissionRootPath, "submission_configuration.json")
        mappingCsvDestPath = os.path.join(degreeSubmissionRootPath,"mapping.csv")
        repositoriesTxtDestPath = os.path.join(degreeSubmissionRootPath,"repositories.txt")
        configFile = os.path.join(degreeSubmissionRootPath,"submission_configuration.json")

        # copy defaults from test_files
        testFileRootPath = os.path.join(degreeSubmissionRootPath,"test_files", "test_files","PortfolioBuildCommandTests", "prepare_stage")
        shutil.copyfile(os.path.join(testFileRootPath, "submission_configuration.json"), configJsonDestPath)
        
        MockScenario.mock_completed_directory("123456789")

        
        configuration = SubmissionConfiguration.createSubmissionConfigurationFromFile(configJsonDestPath)
        configuration.basePath = os.path.abspath(".")
        configuration.action = "build"
        configuration.markPortfolioWorthyOnly = True
        configuration.ignoreErrors = []
        configuration.doExtractionAfterBuild = False
        # copy the coversheets over
        coversheetPath = os.path.join(degreeSubmissionRootPath, "test_files", "test_files", "CoversheetTests")
        shutil.copyfile(os.path.join(coversheetPath, "UI107001_coversheet_signed.docx"), os.path.join(configuration.buildDirectory, "UI107001_coversheet_signed.docx"))
        shutil.copyfile(os.path.join(coversheetPath, "UI107002_coversheet_signed.docx"), os.path.join(configuration.buildDirectory, "UI107002_coversheet_signed.docx"))
        shutil.copyfile(os.path.join(coversheetPath, "UI107003_coversheet_signed.docx"), os.path.join(configuration.buildDirectory, "UI107003_coversheet_signed.docx"))
        shutil.copyfile(os.path.join(coversheetPath, "UI107004_coversheet_signed.docx"), os.path.join(configuration.buildDirectory, "UI107004_coversheet_signed.docx"))
        shutil.copyfile(os.path.join(coversheetPath, "UI107005_coversheet_signed.docx"), os.path.join(configuration.buildDirectory, "UI107005_coversheet_signed.docx"))
        shutil.copyfile(os.path.join(coversheetPath, "UI107006_coversheet_signed.docx"), os.path.join(configuration.buildDirectory, "UI107006_coversheet_signed.docx"))

        cmd = PortfolioBuildCommand(configuration)
        result = cmd.run()
        self.assertTrue(os.path.isdir("123456789"))
        self.assertTrue(os.path.isdir("123456789/assets"))
        self.assertTrue(os.path.isfile("123456789/mapping.csv"))
        self.assertTrue(os.path.isfile("123456789/repositories.txt"))
        self.assertTrue(os.path.isfile(os.path.join(degreeSubmissionRootPath,"123456789.zip")))
        MockScenario.mock_destroy("123456789")