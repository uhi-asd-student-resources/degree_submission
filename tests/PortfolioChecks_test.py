import unittest
import PortfolioChecks
import os
import shutil
import MockScenario 

class PortfolioChecksTest(unittest.TestCase):

    def test_check_for_git_hidden_directory(self):
        MockScenario.mock_completed_directory("123456789")
        filePath = "123456789"
        result = PortfolioChecks.check_for_git_hidden_directory(filePath)
        self.assertTrue(result != False)
        self.assertTrue(4, len(result))
        MockScenario.mock_destroy("123456789")

    def test_check_for_git_hidden_directory_failed(self):
        MockScenario.mock_no_directory("123456789")
        filePath = "123456789"
        result = PortfolioChecks.check_for_git_hidden_directory(filePath)
        self.assertFalse(result)
        MockScenario.mock_destroy("123456789")

if __name__ == '__main__':
    unittest.main()