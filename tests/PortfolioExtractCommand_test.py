import unittest
from PortfolioExtractCommand import PortfolioExtractCommand
#import git
#from pydriller import Repository
import ZipStudentPortfolio
import UnzipStudentPortfolio
import os
import shutil
import MockScenario
from SubmissionConfiguration import createSubmissionConfigurationFromFile
# from PortfolioExtractCommand import PortfolioExtractCommand
import WindowsWorkarounds
import zipfile
import logging
import sys

root = logging.getLogger()
root.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

class ExtractPortfolioTest(unittest.TestCase):
        
    def setUp(self):
        self.degreeSubmissionRoot = os.path.dirname(os.path.dirname(__file__))
        self.testPath = os.path.join(self.degreeSubmissionRoot, "test_files", "test_files", "ExtractionTests")

    # def test_zip_and_extract(self):
    #     """
    #     Test zipping up our test directory and then using our unzip tools
    #     """
    #     zipFileName = "123456789.zip"
    #     MockScenario.mock_completed_directory("123456789")
    #     ZipStudentPortfolio.compress_student_portfolio_into_zip_file("123456789",zipFileName)
    #     self.assertTrue(os.path.isfile(zipFileName))
    #     with zipfile.ZipFile(zipFileName) as zip:
    #         members = zip.namelist()
    #         self.assertEqual(members[0], "123456789/")
    #     WindowsWorkarounds.shutil_rmtree("123456789")
    #     self.assertFalse(os.path.isdir("123456789"))
    #     UnzipStudentPortfolio.unzip_file(zipFileName, "/tmp", "123456789")
    #     self.assertTrue(os.path.isdir("/tmp/123456789"))
    #     self.assertTrue(os.path.isfile("/tmp/123456789/mapping.csv"))
    #     if os.path.isfile(zipFileName):
    #         os.remove(zipFileName)
    #     if os.path.isdir("/tmp/123456789"):
    #         WindowsWorkarounds.shutil_rmtree("/tmp/123456789")

    def test_unzip_and_extract(self):
        testFile = os.path.join(self.testPath, "123456789-good.zip")
        zipFileName = "123456789.zip"
        if os.path.isfile(zipFileName):
            os.remove(zipFileName)
        if os.path.isdir("123456789_extracted"):
            WindowsWorkarounds.shutil_rmtree("123456789_extracted")
        if os.path.isdir("123456789"):
            WindowsWorkarounds.shutil_rmtree("123456789")
        
        shutil.copyfile(testFile, zipFileName)
        UnzipStudentPortfolio.unzip_file(zipFileName, "/tmp", "123456789")
        configFilePath = os.path.join("/tmp", "123456789", "submission_configuration.json")
        self.assertTrue(os.path.isfile(configFilePath))

        config = createSubmissionConfigurationFromFile(configFilePath, studentNumber="123456789")
        extractCommand = PortfolioExtractCommand(config)
        extractCommand.run()
        
        self.assertTrue(os.path.isfile("/tmp/123456789_extracted/coversheets/UI107001_coversheet_signed.docx"))
        self.assertTrue(os.path.isfile("/tmp/123456789_extracted/coversheets/UI107006_coversheet_signed.docx"))
        self.assertTrue(os.path.isfile("/tmp/123456789_extracted/LO1.3.1.3/014f546fb1fb4be7b9602ef9a8ec831563d67d44/test_latest.md"))
        self.assertTrue(os.path.isfile("/tmp/123456789_extracted/LO1.1.1.1/e13c8597aceb2d480a1dd656816356d946535e44/another_file_latest.txt"))
        
        if os.path.isdir("/tmp/123456789_extracted"):
            WindowsWorkarounds.shutil_rmtree("/tmp/123456789_extracted")
        if os.path.isdir("/tmp/123456789"):
            WindowsWorkarounds.shutil_rmtree("/tmp/123456789")
        if os.path.isfile(zipFileName):
            os.remove(zipFileName)
