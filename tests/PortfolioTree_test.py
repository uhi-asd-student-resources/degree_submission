import unittest
from PortfolioTree import PortfolioTree
import os
from PortfolioTreeVisitor import PortfolioTreeVisitor

class MockVisitor(PortfolioTreeVisitor):
    
    def __init__(self):
        super().__init__(None)
        self.directoryCounter =0
        self.fileCounter =0
    
    def matchDirectory(self, path):
        return True

    def matchFile(self, path, ext):
        return True

    def start(self):
        """
        Called before the PorfolioTree starts its inner for loop
        Return False to stop it from proceeding, run nor finish will be called
        """
        self.directoryCounter = 0
        self.fileCounter =0
        return True

    def run(self, path):
        """
        Called for each matching file or directory
        If we return false then we don't continue.
        """
        print(path)
        if os.path.isfile(path):
            self.fileCounter += 1
        if os.path.isdir(path):
            self.directoryCounter += 1
        return True


    def finish(self):
        """
        Called at the very end of the tree, throw an exception using PrettyOutput if you do not
        want to continue.
        """
        pass

class MockVisitorDontStart(PortfolioTreeVisitor):
    
    def __init__(self):
        super().__init__(None)
        self.runCounter =0
        self.finishCounter =0
    
    def matchDirectory(self, path):
        return True

    def matchFile(self, path, ext):
        return True

    def start(self):
        """
        Called before the PorfolioTree starts its inner for loop
        Return False to stop it from proceeding, run nor finish will be called
        """
        return False

    def run(self, path):
        """
        Called for each matching file or directory
        If we return false then we don't continue.
        """
        self.runCounter += 1
        return True


    def finish(self):
        """
        Called at the very end of the tree, throw an exception using PrettyOutput if you do not
        want to continue.
        """
        self.finishCounter += 1

class MockVisitorBreakRun(PortfolioTreeVisitor):
    
    def __init__(self):
        super().__init__(None)
        self.runCounter =0
        self.finishCounter =0
    
    def matchDirectory(self, path):
        return True

    def matchFile(self, path, ext):
        return True

    def start(self):
        """
        Called before the PorfolioTree starts its inner for loop
        Return False to stop it from proceeding, run nor finish will be called
        """
        return True

    def run(self, path):
        """
        Called for each matching file or directory
        If we return false then we don't continue.
        """
        self.runCounter += 1
        if self.runCounter == 2:
            return False
        return True


    def finish(self):
        """
        Called at the very end of the tree, throw an exception using PrettyOutput if you do not
        want to continue.
        """
        self.finishCounter += 1

class PortfolioTreeTest(unittest.TestCase):

    def test_create(self):
        directory = os.path.join(os.path.dirname(os.path.dirname(__file__)), "test_files", "test_files", "PortfolioTreeTests", "demopersonal")
        pt = PortfolioTree(None, directory)
        pt.create()
        self.assertEqual(len(pt.filesAndDirectories), 61)

    def test_accept(self):
        directory = os.path.join(os.path.dirname(os.path.dirname(__file__)), "test_files", "test_files", "PortfolioTreeTests", "demopersonal")
        mockVisitor = MockVisitor()
        pt = PortfolioTree(None, directory)
        pt.create()
        pt.accept(mockVisitor)
        self.assertEqual(mockVisitor.fileCounter, 4)
        self.assertEqual(mockVisitor.directoryCounter, 1)

    def test_accept(self):
        directory = os.path.join(os.path.dirname(os.path.dirname(__file__)), "test_files", "test_files", "PortfolioTreeTests", "demopersonal")
        mockVisitorDontStart = MockVisitorDontStart()
        pt = PortfolioTree(None, directory)
        pt.create()
        pt.accept(mockVisitorDontStart)
        self.assertEqual(mockVisitorDontStart.runCounter, 0)
        self.assertEqual(mockVisitorDontStart.finishCounter, 0)

    def test_accept(self):
        directory = os.path.join(os.path.dirname(os.path.dirname(__file__)), "test_files", "test_files", "PortfolioTreeTests", "demopersonal")
        mockVisitorBreakRun = MockVisitorBreakRun()
        pt = PortfolioTree(None, directory)
        pt.create()
        pt.accept(mockVisitorBreakRun)
        self.assertEqual(mockVisitorBreakRun.runCounter, 2)
        self.assertEqual(mockVisitorBreakRun.finishCounter, 1)
