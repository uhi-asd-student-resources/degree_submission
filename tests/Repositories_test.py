import unittest
import git
import os
import Repositories
import MockScenario
import WindowsWorkarounds

class RepositoriesTest(unittest.TestCase):

    def test_clonePersonalRepositoryTemplate(self):
        targetPath = os.path.join(os.path.dirname(__file__), "personal_repository_template")
        thisDirPath = Repositories.clonePersonalRepositoryTemplate(targetPath)
        self.assertTrue(os.path.isdir(thisDirPath))
        WindowsWorkarounds.shutil_rmtree(thisDirPath)

    def test_repo_public_clone(self):
        if os.path.isdir("public-repo-test"):
            WindowsWorkarounds.shutil_rmtree("public-repo-test")
        repo = git.Repo.clone_from("https://github.com/tommccallum/Demo-Assessment", "public-repo-test")
        self.assertTrue(os.path.isdir("public-repo-test"))
        if os.path.isdir("public-repo-test"):
            WindowsWorkarounds.shutil_rmtree("public-repo-test")
        
    def test_repo_private_clone(self):
        if os.path.isdir("private-repo-test"):
            WindowsWorkarounds.shutil_rmtree("private-repo-test")
        repositoryUrl = "git@bitbucket.org:uhi-asd-student-resources/demo_private_repository_2020.git"
        repo = git.Repo.clone_from(repositoryUrl, "private-repo-test")
        self.assertTrue(os.path.isdir("private-repo-test"))
        if os.path.isdir("private-repo-test"):
            WindowsWorkarounds.shutil_rmtree("private-repo-test")

    def test_clone_repository_list_success(self):
        MockScenario.mock_no_directory("123456789")
        repositoryList = [
            "https://github.com/tommccallum/Demo-Assessment",
            "git@bitbucket.org:uhi-asd-student-resources/demo_private_repository_2020.git"
        ]
        Repositories.clone_repository_list(repositoryList, "123456789")
        MockScenario.mock_destroy("123456789")

    def test_clone_repository_list_fail(self):
        MockScenario.mock_no_directory("123456789")
        repositoryList = [
            "https://github.com/tommccallum/does_not_exist",
        ]
        with self.assertRaises(RuntimeError):
            Repositories.clone_repository_list(repositoryList, "123456789")
        MockScenario.mock_destroy("123456789")

    def test_clone_repository_list_fail_as_empty(self):
        MockScenario.mock_no_directory("123456789")
        repositoryList = [
            "git@bitbucket.org:uhi-asd-student-resources/test_empty_repository.git",
        ]
        # attempt 1
        Repositories.clone_repository_list( repositoryList, "123456789")
        # attempt 2 - creates an error
        with self.assertRaises(RuntimeError):
            Repositories.clone_repository_list( repositoryList, "123456789")
        MockScenario.mock_destroy("123456789")

    def test_clone_repository_list_exists(self):
        MockScenario.mock_completed_directory("123456789")
        repositoryList = [
            "https://github.com/tommccallum/Demo-Assessment",
            "git@bitbucket.org:uhi-asd-student-resources/demo_private_repository_2020.git"
        ]
        Repositories.clone_repository_list(repositoryList, "123456789")
        MockScenario.mock_destroy("123456789")

    def test_read_commits(self):
        MockScenario.mock_completed_directory("123456789")
        commits = Repositories.read_in_commits("123456789/Demo-Assessment")
        self.assertEqual(5, len(commits))
        MockScenario.mock_destroy("123456789")

    def test_write_commits(self):
        if os.path.isfile("test_commit_log.csv"):
            os.remove("test_commit_log.csv")
        MockScenario.mock_completed_directory("123456789")
        commits = Repositories.read_in_commits("123456789/Demo-Assessment")
        Repositories.write_commits(commits, "test_commit_log.csv")
        self.assertTrue(os.path.isfile("test_commit_log.csv"))
        MockScenario.mock_destroy("123456789")
        os.remove("test_commit_log.csv")