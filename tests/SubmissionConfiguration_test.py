import unittest
import os
from SubmissionConfiguration import SubmissionConfiguration, createSubmissionConfigurationFromFile
import MockScenario

class SubmissionConfigurationTest(unittest.TestCase):

    def test_creation(self):
        config = SubmissionConfiguration("123456789")

        self.assertEqual("123456789", config.studentNumber)
        self.assertTrue(config.assetsDirectory.endswith(os.path.join("123456789","assets")))
        self.assertTrue(config.extractDirectory.endswith(os.path.join("123456789_extracted")))
        self.assertEqual("123456789", config.zipFilePrefix)
        self.assertEqual("123456789.zip", config.zipFileName)
        self.assertTrue(config.mappingFilePath.endswith(os.path.join("123456789","mapping.csv")))
        self.assertTrue(config.repositoryListFilePath.endswith(os.path.join("123456789","repositories.txt")))
        
    def test_read_repositories(self):
        MockScenario.mock_completed_directory("123456789")
        config = SubmissionConfiguration("123456789", "./")
        repositoryList = config.readRepositories()
        self.assertEqual(7, len(repositoryList))
        self.assertEqual("git@bitbucket.org:uhi-asd-student-resources/demo_private_repository_2020.git", repositoryList[4])
        MockScenario.mock_no_directory("123456789")

    def test_read_in_submission_configuration(self):
        config = createSubmissionConfigurationFromFile("test_files/test_files/submission_configuration.json")

        self.assertEqual("123456789", config.studentNumber)
        self.assertTrue(config.assetsDirectory.endswith(os.path.join("123456789","assets")))
        self.assertTrue(config.extractDirectory.endswith(os.path.join("123456789_extracted")))
        self.assertEqual("123456789", config.zipFilePrefix)
        self.assertEqual("123456789.zip", config.zipFileName)
        self.assertTrue(config.mappingFilePath.endswith(os.path.join("123456789","mapping.csv")))
        self.assertTrue(config.repositoryListFilePath.endswith(os.path.join("123456789","repositories.txt")))
        
        

    def test_set_year_invalid(self):
        config = SubmissionConfiguration("123456789", "./")
        self.assertRaises(ValueError, config.setYearList, 7)

    def test_set_year_valid(self):
        config = SubmissionConfiguration("123456789", "./")
        config.setYearList(4)
        self.assertEqual([4], config.yearList)

    def test_set_modules_valid(self):
        config = SubmissionConfiguration("123456789", "./")
        config.setModuleList([1,2,3,4,5,6])
        self.assertEqual([1,2,3,4,5,6], config.moduleList)