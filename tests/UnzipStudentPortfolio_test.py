import unittest
import UnzipStudentPortfolio
import os
import shutil
import WindowsWorkarounds
import logging
import sys

root = logging.getLogger()
root.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

class UnzipStudentPortfolioTest(unittest.TestCase):

    def setUp(self):
        self.degreeSubmissionPath = os.path.dirname(os.path.dirname(__file__))
        self.testPath = os.path.join(self.degreeSubmissionPath, "test_files", "test_files", "UnzipTests")

    def test_unzip_contents(self):
        """
        Test when the zip file has been zipped inside the directory
        We extract into /tmp
        """
        targetDirectory = "/tmp"
        self.assertTrue(os.path.isdir(targetDirectory)) # must be linux currently
        targetPath = os.path.join(targetDirectory,"123456789")
        if os.path.isdir(targetPath):
            WindowsWorkarounds.shutil_rmtree(targetPath)
        zipFile = os.path.join(targetDirectory,"123456789.zip")
        if os.path.isfile(zipFile):
            os.remove(zipFile)
        source = os.path.join(self.testPath, "123456789_contents.zip")
        shutil.copyfile(source, zipFile)
        UnzipStudentPortfolio.unzip_file(zipFile, targetDirectory, studentNumber="123456789")
        self.assertTrue(os.path.isdir(targetPath))
        self.assertTrue(os.path.isfile(os.path.join(targetPath,"submission_configuration.json")))
        self.assertTrue(os.path.isfile(os.path.join(targetPath,"mapping.csv")))
        if os.path.isfile(zipFile):
            os.remove(zipFile)
        WindowsWorkarounds.shutil_rmtree(targetPath)
    
    def test_unzip_contents_fail(self):
        """
        Test when the zip file has been zipped inside the directory
        We extract into /tmp
        """
        targetDirectory = "/tmp"
        self.assertTrue(os.path.isdir(targetDirectory)) # must be linux currently
        targetPath = os.path.join(targetDirectory,"123456789")
        if os.path.isdir(targetPath):
            WindowsWorkarounds.shutil_rmtree(targetPath)
        zipFile = os.path.join(targetDirectory,"123456789.zip")
        if os.path.isfile(zipFile):
            os.remove(zipFile)
        source = os.path.join(self.testPath, "123456789_contents_nomappingcsv.zip")
        shutil.copyfile(source, zipFile)
        with self.assertRaises(RuntimeError):
            UnzipStudentPortfolio.unzip_file(zipFile, targetDirectory, studentNumber="123456789")
            self.assertFalse(os.path.isdir(targetPath))
            if os.path.isfile(zipFile):
                os.remove(zipFile)
            WindowsWorkarounds.shutil_rmtree(targetPath)

    def test_unzip_correct(self):
        """
        Test when the zip file has been zipped outside the directory as expected
        We extract into /tmp
        """
        targetDirectory = "/tmp"
        self.assertTrue(os.path.isdir(targetDirectory)) # must be linux currently
        targetPath = os.path.join(targetDirectory,"123456789")
        if os.path.isdir(targetPath):
            WindowsWorkarounds.shutil_rmtree(targetPath)
        zipFile = os.path.join(targetDirectory,"123456789.zip")
        if os.path.isfile(zipFile):
            os.remove(zipFile)
        source = os.path.join(self.testPath, "123456789_correct.zip")
        shutil.copyfile(source, zipFile)
        UnzipStudentPortfolio.unzip_file(zipFile, targetDirectory, studentNumber="123456789")
        self.assertTrue(os.path.isdir(targetPath))
        self.assertTrue(os.path.isfile(os.path.join(targetPath,"submission_configuration.json")))
        self.assertTrue(os.path.isfile(os.path.join(targetPath,"mapping.csv")))
        if os.path.isfile(zipFile):
            os.remove(zipFile)
        WindowsWorkarounds.shutil_rmtree(targetPath)
        
    def test_unzip_onesubdirectory(self):
        targetDirectory = "/tmp"
        self.assertTrue(os.path.isdir(targetDirectory)) # must be linux currently
        targetPath = os.path.join(targetDirectory,"123456789")
        if os.path.isdir(targetPath):
            WindowsWorkarounds.shutil_rmtree(targetPath)
        zipFile = os.path.join(targetDirectory,"123456789.zip")
        if os.path.isfile(zipFile):
            os.remove(zipFile)
        source = os.path.join(self.testPath, "123456789_onedirectorydown.zip")
        shutil.copyfile(source, zipFile)
        UnzipStudentPortfolio.unzip_file(zipFile, targetDirectory, studentNumber="123456789")
        self.assertTrue(os.path.isdir(targetPath))
        self.assertTrue(os.path.isfile(os.path.join(targetPath,"submission_configuration.json")))
        self.assertTrue(os.path.isfile(os.path.join(targetPath,"mapping.csv")))
        if os.path.isfile(zipFile):
            os.remove(zipFile)
        WindowsWorkarounds.shutil_rmtree(targetPath)

    def test_unzip_twosubdirectory(self):
        targetDirectory = "/tmp"
        self.assertTrue(os.path.isdir(targetDirectory)) # must be linux currently
        targetPath = os.path.join(targetDirectory,"123456789")
        if os.path.isdir(targetPath):
            WindowsWorkarounds.shutil_rmtree(targetPath)
        zipFile = os.path.join(targetDirectory,"123456789.zip")
        if os.path.isfile(zipFile):
            os.remove(zipFile)
        source = os.path.join(self.testPath, "123456789_twodirectorydown.zip")
        shutil.copyfile(source, zipFile)
        UnzipStudentPortfolio.unzip_file(zipFile, targetDirectory, studentNumber="123456789")
        self.assertTrue(os.path.isdir(targetPath))
        self.assertTrue(os.path.isfile(os.path.join(targetPath,"submission_configuration.json")))
        self.assertTrue(os.path.isfile(os.path.join(targetPath,"mapping.csv")))
        if os.path.isfile(zipFile):
            os.remove(zipFile)
        WindowsWorkarounds.shutil_rmtree(targetPath)

    def test_unzip_submitted_degree_tool(self):
        targetDirectory = "/tmp"
        self.assertTrue(os.path.isdir(targetDirectory)) # must be linux currently
        targetPath = os.path.join(targetDirectory,"123456789")
        if os.path.isdir(targetPath):
            WindowsWorkarounds.shutil_rmtree(targetPath)
        zipFile = os.path.join(targetDirectory,"123456789.zip")
        if os.path.isfile(zipFile):
            os.remove(zipFile)
        source = os.path.join(self.testPath, "FakeDegreeSubmissionDirectory.zip")
        shutil.copyfile(source, zipFile)
        UnzipStudentPortfolio.unzip_file(zipFile, targetDirectory, studentNumber="123456789")
        self.assertTrue(os.path.isdir(targetPath))
        self.assertTrue(os.path.isfile(os.path.join(targetPath,"submission_configuration.json")))
        self.assertTrue(os.path.isfile(os.path.join(targetPath,"mapping.csv")))
        if os.path.isfile(zipFile):
            os.remove(zipFile)
        WindowsWorkarounds.shutil_rmtree(targetPath)

