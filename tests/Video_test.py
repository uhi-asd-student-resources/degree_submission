import unittest
import MockScenario
import Video
import os
import shutil

class CompressMediaTest(unittest.TestCase):

    # def test_compress_video(self):
    #     destPath = "123456789"
    #     MockScenario.mock_no_directory(destPath)
    #     targetPath = os.path.join(destPath,"test_video.mp4")
    #     shutil.copyfile("test_files/test_video.mp4", targetPath)
    #     compressedTargetPath = os.path.join(destPath,"test_video_compressed.mp4")
    #     CompressMedia.compress_video(targetPath, compressedTargetPath, 15*1024)
    #     self.assertTrue(os.path.isfile(compressedTargetPath))
    #     self.assertTrue(os.path.getsize(compressedTargetPath) < 15 * 1024 * 1024)
    #     MockScenario.mock_destroy(destPath)

    def test_compress_video_small_no_change(self):
        destPath = "123456789"
        MockScenario.mock_no_directory(destPath)
        targetPath = os.path.join(destPath,"test_short.mp4")
        shutil.copyfile("test_files/test_files/test_short.mp4", targetPath)
        compressedTargetPath = os.path.join(destPath,"test_video_compressed.mp4")
        Video.compress_video(targetPath, compressedTargetPath, 15*1024*1024)
        self.assertFalse(os.path.isfile(compressedTargetPath))
        MockScenario.mock_destroy(destPath)

    def test_compress_video_compressable(self):
        destPath = "123456789"
        MockScenario.mock_no_directory(destPath)
        targetPath = os.path.join(destPath,"test_shrinkable.mp4")
        shutil.copyfile("test_files/test_files/test_shrinkable.mp4", targetPath)
        compressedTargetPath = os.path.join(destPath,"test_video_compressed.mp4")
        Video.compress_video(targetPath, compressedTargetPath, 15*1024*1024)
        self.assertTrue(os.path.isfile(compressedTargetPath))
        self.assertTrue(os.path.getsize(compressedTargetPath) < 15*1024*1024)
        MockScenario.mock_destroy(destPath)
