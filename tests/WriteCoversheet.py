import docx

def writeCoversheet(path):    
    doc = docx.Document(path)

    tags = [
        "Your student number",
        "Your college",
        "The date of submission here",
        "Type your first and last name here or put a signature",
        "Date Submitted"
    ]
    
    for tableIndex, t in enumerate(doc.tables):
        for rowIndex, row in enumerate(t.rows):
            for cellIndex, cell in enumerate(row.cells):
                # print("CELL {}".format(cell.text))
                for tag in tags:
                    if "<<{}>>".format(tag.lower()) in str(cell.text).strip().lower():
                        cell.text = "Something that is not empty"
    doc.save(path)