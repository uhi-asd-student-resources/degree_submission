import unittest
import ZipStudentPortfolio
import os
import MockScenario
import random
import shutil

class ZipStudentPortfolioTest(unittest.TestCase):

    def test_make_zip_with_accidental_file_extension(self):
        MockScenario.mock_completed_directory("123456789")
        ZipStudentPortfolio.compress_student_portfolio_into_zip_file("123456789","test_123456789.zip")
        self.assertTrue(os.path.isfile("test_123456789.zip"))
        os.remove("test_123456789.zip")
        MockScenario.mock_destroy("123456789")

    def test_make_zip_as_expected(self):
        MockScenario.mock_completed_directory("123456789")
        ZipStudentPortfolio.compress_student_portfolio_into_zip_file("123456789","test_123456789")
        self.assertTrue(os.path.isfile("test_123456789.zip"))
        os.remove("test_123456789.zip")
        MockScenario.mock_destroy("123456789")
        

    def test_very_large_student_portfolio(self):
        # This will take a while...
        MockScenario.mock_completed_directory("123456789")
        targetLargeFilePath = os.path.join(os.path.dirname(os.path.dirname(__file__)), "123456789", "very_large_file.txt")
        srcLargeFilePath = os.path.join(os.path.dirname(os.path.dirname(__file__)), "test_files", "test_files", "very_large_file.txt")
        shutil.copyfile(srcLargeFilePath, targetLargeFilePath)

        with self.assertRaises(ValueError):
            ZipStudentPortfolio.compress_student_portfolio_into_zip_file("123456789","test_123456789")
        # it should still make the zip file
        self.assertTrue(os.path.isfile("test_123456789.zip"))
        os.remove("test_123456789.zip")
        MockScenario.mock_destroy("123456789")